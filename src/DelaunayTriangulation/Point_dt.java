/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package DelaunayTriangulation;


import java.io.PrintStream;
import java.util.Comparator;
import javax.vecmath.Point3d;
import pkg3drecreation.DepthPoint;

// Referenced classes of package delaunay_triangulation:
//            Compare

public class Point_dt
{
    private DepthPoint originalPoint=null;
    public Point_dt()
    {
    }

    public Point_dt(double x, double y, double z)
    {
        this.x = x;
        this.y = y;
        this.z = z;
    }

    public Point_dt(double x, double y)
    {
        this(x, y, 0.0D);
    }
    public Point_dt(Point_dt p)
    {
        
    }
    public Point_dt(DepthPoint dPoint)
    {
        Point3d p3d=dPoint.get3DPoint();
        this.x = p3d.x;
        this.y = p3d.y;
        this.z = p3d.z;
        originalPoint=dPoint;
    }

    public double x()
    {
        return x;
    }

    public double y()
    {
        return y;
    }

    public double z()
    {
        return z;
    }

    double distance2(Point_dt p)
    {
        return (p.x - x) * (p.x - x) + (p.y - y) * (p.y - y);
    }

    double distance2(double px, double py)
    {
        return (px - x) * (px - x) + (py - y) * (py - y);
    }

    boolean isLess(Point_dt p)
    {
        return x < p.x || x == p.x && y < p.y;
    }

    boolean isGreater(Point_dt p)
    {
        return x > p.x || x == p.x && y > p.y;
    }

    public boolean equals(Point_dt p)
    {
        return x == p.x && y == p.y;
    }

    public String toString()
    {
        return new String((new StringBuilder(" Pt[")).append(x).append(",").append(y).append(",").append(z).append("]").toString());
    }

    public double distance(Point_dt p)
    {
        double temp = Math.pow(p.x() - x, 2D) + Math.pow(p.y() - y, 2D);
        return Math.sqrt(temp);
    }

    public double distance3D(Point_dt p)
    {
        double temp = Math.pow(p.x() - x, 2D) + Math.pow(p.y() - y, 2D) + Math.pow(p.z() - z, 2D);
        return Math.sqrt(temp);
    }

    public String toFile()
    {
        return (new StringBuilder()).append(x).append(" ").append(y).append(" ").append(z).toString();
    }

    String toFileXY()
    {
        return (new StringBuilder()).append(x).append(" ").append(y).toString();
    }

    public int pointLineTest(Point_dt a, Point_dt b)
    {
        double dx = b.x - a.x;
        double dy = b.y - a.y;
        double res = dy * (x - a.x) - dx * (y - a.y);
        if(res < 0.0D)
            return 1;
        if(res > 0.0D)
            return 2;
        if(dx > 0.0D)
        {
            if(x < a.x)
                return 3;
            return b.x >= x ? 0 : 4;
        }
        if(dx < 0.0D)
        {
            if(x > a.x)
                return 3;
            return b.x <= x ? 0 : 4;
        }
        if(dy > 0.0D)
        {
            if(y < a.y)
                return 3;
            return b.y >= y ? 0 : 4;
        }
        if(dy < 0.0D)
        {
            if(y > a.y)
                return 3;
            return b.y <= y ? 0 : 4;
        } else
        {
            System.out.println("Error, pointLineTest with a=b");
            return 5;
        }
    }

    boolean areCollinear(Point_dt a, Point_dt b)
    {
        double dx = b.x - a.x;
        double dy = b.y - a.y;
        double res = dy * (x - a.x) - dx * (y - a.y);
        return res == 0.0D;
    }

    Point_dt circumcenter(Point_dt a, Point_dt b)
    {
        double u = ((a.x - b.x) * (a.x + b.x) + (a.y - b.y) * (a.y + b.y)) / 2D;
        double v = ((b.x - x) * (b.x + x) + (b.y - y) * (b.y + y)) / 2D;
        double den = (a.x - b.x) * (b.y - y) - (b.x - x) * (a.y - b.y);
        if(den == 0.0D)
            System.out.println("circumcenter, degenerate case");
        return new Point_dt((u * (b.y - y) - v * (a.y - b.y)) / den, (v * (a.x - b.x) - u * (b.x - x)) / den);
    }

    public static Comparator getComparator(int flag)
    {
        return new Compare(flag);
    }

    public static Comparator getComparator()
    {
        return new Compare(0);
    }

    double x;
    double y;
    double z;
    public static final int ONSEGMENT = 0;
    public static final int LEFT = 1;
    public static final int RIGHT = 2;
    public static final int INFRONTOFA = 3;
    public static final int BEHINDB = 4;
    public static final int ERROR = 5;

    /**
     * @return the originalPoint
     */
    public DepthPoint getOriginalPoint() {
        return originalPoint;
    }
}