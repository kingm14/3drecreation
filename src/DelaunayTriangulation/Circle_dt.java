/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package DelaunayTriangulation;

// Referenced classes of package delaunay_triangulation:
//            Point_dt

class Circle_dt
{

    public Circle_dt()
    {
    }

    public Circle_dt(Point_dt c, double r)
    {
        this.c = c;
        this.r = r;
    }

    public Circle_dt(Circle_dt circ)
    {
        c = circ.c;
        r = circ.r;
    }

    public String toString()
    {
        return new String((new StringBuilder(" Circle[")).append(c.toString()).append("|").append(r).append("|").append((int)Math.round(Math.sqrt(r))).append("]").toString());
    }

    Point_dt c;
    double r;
}