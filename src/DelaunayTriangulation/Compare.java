/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package DelaunayTriangulation;

import java.util.Comparator;

// Referenced classes of package delaunay_triangulation:
//            Point_dt

class Compare
    implements Comparator
{

    public Compare(int i)
    {
        _flag = i;
    }

    public int compare(Object o1, Object o2)
    {
        int ans = 0;
        if(o1 != null && o2 != null && (o1 instanceof Point_dt) && (o2 instanceof Point_dt))
        {
            Point_dt d1 = (Point_dt)o1;
            Point_dt d2 = (Point_dt)o2;
            if(_flag == 0)
            {
                if(d1.x > d2.x)
                    return 1;
                if(d1.x < d2.x)
                    return -1;
                if(d1.y > d2.y)
                    return 1;
                if(d1.y < d2.y)
                    return -1;
            } else
            if(_flag == 1)
            {
                if(d1.x > d2.x)
                    return -1;
                if(d1.x < d2.x)
                    return 1;
                if(d1.y > d2.y)
                    return -1;
                if(d1.y < d2.y)
                    return 1;
            } else
            if(_flag == 2)
            {
                if(d1.y > d2.y)
                    return 1;
                if(d1.y < d2.y)
                    return -1;
                if(d1.x > d2.x)
                    return 1;
                if(d1.x < d2.x)
                    return -1;
            } else
            if(_flag == 3)
            {
                if(d1.y > d2.y)
                    return -1;
                if(d1.y < d2.y)
                    return 1;
                if(d1.x > d2.x)
                    return -1;
                if(d1.x < d2.x)
                    return 1;
            }
        } else
        {
            if(o1 == null && o2 == null)
                return 0;
            if(o1 == null && o2 != null)
                return 1;
            if(o1 != null && o2 == null)
                return -1;
        }
        return ans;
    }

    public boolean equals(Object ob)
    {
        return false;
    }

    private int _flag;
}