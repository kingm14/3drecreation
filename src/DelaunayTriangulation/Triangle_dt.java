/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package DelaunayTriangulation;

import java.io.PrintStream;

// Referenced classes of package delaunay_triangulation:
//            Point_dt, Circle_dt

public class Triangle_dt
{

    public Triangle_dt(Point_dt A, Point_dt B, Point_dt C)
    {
        _mc = 0;
        halfplane = false;
        _mark = false;
        a = A;
        int res = C.pointLineTest(A, B);
        if(res <= 1 || res == 3 || res == 4)
        {
            b = B;
            c = C;
        } else
        {
            System.out.println("Warning, ajTriangle(A,B,C) expects points in counterclockwise order.");
            System.out.println((new StringBuilder()).append(A).append(B).append(C).toString());
            b = C;
            c = B;
        }
        circumcircle();
    }

    public Triangle_dt(Point_dt A, Point_dt B)
    {
        _mc = 0;
        halfplane = false;
        _mark = false;
        a = A;
        b = B;
        halfplane = true;
    }

    public boolean isHalfplane()
    {
        return halfplane;
    }

    public Point_dt p1()
    {
        return a;
    }

    public Point_dt p2()
    {
        return b;
    }

    public Point_dt p3()
    {
        return c;
    }

    public Triangle_dt next_12()
    {
        return abnext;
    }

    public Triangle_dt next_23()
    {
        return bcnext;
    }

    public Triangle_dt next_31()
    {
        return canext;
    }

    void switchneighbors(Triangle_dt Old, Triangle_dt New)
    {
        if(abnext == Old)
            abnext = New;
        else
        if(bcnext == Old)
            bcnext = New;
        else
        if(canext == Old)
            canext = New;
        else
            System.out.println("Error, switchneighbors can't find Old.");
    }

    Triangle_dt neighbor(Point_dt p)
    {
        if(a == p)
            return canext;
        if(b == p)
            return abnext;
        if(c == p)
        {
            return bcnext;
        } else
        {
            System.out.println((new StringBuilder("Error, neighbors can't find p: ")).append(p).toString());
            return null;
        }
    }

    Circle_dt circumcircle()
    {
        double u = ((a.x - b.x) * (a.x + b.x) + (a.y - b.y) * (a.y + b.y)) / 2D;
        double v = ((b.x - c.x) * (b.x + c.x) + (b.y - c.y) * (b.y + c.y)) / 2D;
        double den = (a.x - b.x) * (b.y - c.y) - (b.x - c.x) * (a.y - b.y);
        if(den == 0.0D)
        {
            circum = new Circle_dt(a, (1.0D / 0.0D));
        } else
        {
            Point_dt cen = new Point_dt((u * (b.y - c.y) - v * (a.y - b.y)) / den, (v * (a.x - b.x) - u * (b.x - c.x)) / den);
            circum = new Circle_dt(cen, cen.distance2(a));
        }
        return circum;
    }

    boolean circumcircle_contains(Point_dt p)
    {
        return circum.r > circum.c.distance2(p);
    }

    public String toString()
    {
        String res = "";
        res = (new StringBuilder(String.valueOf(res))).append(a.toString()).append(b.toString()).toString();
        if(!halfplane)
            res = (new StringBuilder(String.valueOf(res))).append(c.toString()).toString();
        return res;
    }

    public boolean contains(Point_dt p)
    {
        boolean ans = false;
        if(halfplane | (p == null))
            return false;
        if((p.x == a.x) & (p.y == a.y) | (p.x == b.x) & (p.y == b.y) | (p.x == c.x) & (p.y == c.y))
            return true;
        int a12 = p.pointLineTest(a, b);
        int a23 = p.pointLineTest(b, c);
        int a31 = p.pointLineTest(c, a);
        if(a12 == 1 && a23 == 1 && a31 == 1 || a12 == 2 && a23 == 2 && a31 == 2 || a12 == 0 || a23 == 0 || a31 == 0)
            ans = true;
        return ans;
    }

    public double z_value(Point_dt q)
    {
        if(q == null || halfplane)
            throw new RuntimeException((new StringBuilder("*** ERR wrong parameters, can't approximate the z value ..***: ")).append(q).toString());
        if((q.x == a.x) & (q.y == a.y))
            return a.z;
        if((q.x == b.x) & (q.y == b.y))
            return b.z;
        if((q.x == c.x) & (q.y == c.y))
            return c.z;
        double X = 0.0D;
        double x0 = q.x;
        double x1 = a.x;
        double x2 = b.x;
        double x3 = c.x;
        double Y = 0.0D;
        double y0 = q.y;
        double y1 = a.y;
        double y2 = b.y;
        double y3 = c.y;
        double Z = 0.0D;
        double m01 = 0.0D;
        double k01 = 0.0D;
        double m23 = 0.0D;
        double k23 = 0.0D;
        int flag01 = 0;
        if(x0 != x1)
        {
            m01 = (y0 - y1) / (x0 - x1);
            k01 = y0 - m01 * x0;
            if(m01 == 0.0D)
                flag01 = 1;
        } else
        {
            flag01 = 2;
        }
        int flag23 = 0;
        if(x2 != x3)
        {
            m23 = (y2 - y3) / (x2 - x3);
            k23 = y2 - m23 * x2;
            if(m23 == 0.0D)
                flag23 = 1;
        } else
        {
            flag23 = 2;
        }
        if(flag01 == 2)
        {
            X = x0;
            Y = m23 * X + k23;
        } else
        if(flag23 == 2)
        {
            X = x2;
            Y = m01 * X + k01;
        } else
        {
            X = (k23 - k01) / (m01 - m23);
            Y = m01 * X + k01;
        }
        double r = 0.0D;
        if(flag23 == 2)
            r = (y2 - Y) / (y2 - y3);
        else
            r = (x2 - X) / (x2 - x3);
        Z = b.z + (c.z - b.z) * r;
        if(flag01 == 2)
            r = (y1 - y0) / (y1 - Y);
        else
            r = (x1 - x0) / (x1 - X);
        double qZ = a.z + (Z - a.z) * r;
        return qZ;
    }

    public double z(double x, double y)
    {
        return z_value(new Point_dt(x, y));
    }

    public Point_dt z(Point_dt q)
    {
        double z = z_value(q);
        return new Point_dt(q.x, q.y, z);
    }

    Point_dt a;
    Point_dt b;
    Point_dt c;
    Triangle_dt abnext;
    Triangle_dt bcnext;
    Triangle_dt canext;
    Circle_dt circum;
    int _mc;
    boolean halfplane;
    boolean _mark;
    public static int _counter = 0;
    public static int _c2 = 0;

}