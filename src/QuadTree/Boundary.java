/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package QuadTree;

import java.awt.Point;

/**
 *
 * @author kingm14
 */
public class Boundary {
    private Point.Double center;
    private double halfSize;
    
    public Boundary(Point.Double center, double halfSize)
    {
        this.center=center;
        this.halfSize=halfSize;
    }
    public Boundary(double x, double y, double halfSize)
    {
        this.center=new Point.Double(x,y);
        this.halfSize=halfSize;
    }
    public boolean containsPoint(Point.Double p)
    {
        return (Math.abs(p.getX()-getCenter().getX())<=getHalfSize()&&Math.abs(p.getY()-getCenter().getY())<=getHalfSize());
    }
    public boolean intersects(Boundary b)
    {
        return (Math.abs(b.getCenter().getX()-getCenter().getX())<=(2*getHalfSize())&&Math.abs(b.getCenter().getY()-getCenter().getY())<=(2*getHalfSize()));
    }
    //returns if the current boundary is entirely within surrounding
    public boolean entirelyWithin(Boundary surrounding)
    {
        if(surrounding.getHalfSize()<getHalfSize())
        {
            return false;
        }
        Point.Double surC=surrounding.getCenter();
        double surX=surC.getX();
        double surY=surC.getY();
        double surSize=surrounding.getHalfSize();
        
        double meX=center.getX();
        double meY=center.getY();
        return (((surX-surSize)<=(meX-halfSize))
                &&((surX+surSize)>=(meX+halfSize))
                &&((surY-surSize)<=(meY-halfSize))
                &&((surY+surSize)>=(meY+halfSize)));
    }

    /**
     * @return the center
     */
    public Point.Double getCenter() {
        return center;
    }

    /**
     * @param center the center to set
     */
    public void setCenter(Point.Double center) {
        this.center = center;
    }

    /**
     * @return the halfSize
     */
    public double getHalfSize() {
        return halfSize;
    }

    /**
     * @param halfSize the halfSize to set
     */
    public void setHalfSize(double halfSize) {
        this.halfSize = halfSize;
    }
}
