/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package QuadTree;

import java.awt.Point;
import mpi.cbg.fly.Feature;

/**
 *
 * @author kingm14
 */
public class PointToFeature {
    private Point.Double point;
    private Feature feature;
    public PointToFeature(Feature f)
    {
        feature=f;
        point=convertToPoint(feature);
    }
    public static Point.Double convertToPoint(Feature f)
    {
        return new Point.Double(f.location[0],f.location[1]);
        
    }

    /**
     * @return the point
     */
    public Point.Double getPoint() {
        return point;
    }

    /**
     * @param point the point to set
     */
    public void setPoint(Point.Double point) {
        this.point = point;
    }

    /**
     * @return the feature
     */
    public Feature getFeature() {
        return feature;
    }

    /**
     * @param feature the feature to set
     */
    public void setFeature(Feature feature) {
        this.feature = feature;
    }
}
