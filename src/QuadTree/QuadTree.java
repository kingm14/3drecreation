/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package QuadTree;

import java.awt.Point;
import java.util.ArrayList;
import mpi.cbg.fly.Feature;

/**
 *
 * @author kingm14
 */
public class QuadTree {

    private final int QT_NODE_CAPACITY = 4;
    private Boundary boundary;
    ArrayList<PointToFeature> points;
    private QuadTree northWest;
    private QuadTree northEast;
    private QuadTree southWest;
    private QuadTree southEast;

    public QuadTree(Boundary b) {
        this.boundary = b;
        northWest = null;
        northEast = null;
        southWest = null;
        southEast = null;
        points = new ArrayList<PointToFeature>(QT_NODE_CAPACITY);
    }

    public boolean insert(Feature f) {
        PointToFeature pf=new PointToFeature(f);
        return insert(pf);
    }
    public boolean insert(PointToFeature pf) {
        Point.Double p=pf.getPoint();
        Feature f=pf.getFeature();
        if (!boundary.containsPoint(p)) {
            return false;
        }
        if (points.size() < QT_NODE_CAPACITY) {
            points.add(pf);
            return true;
        }
        if (northWest == null) {
            subdivide();
        }
        PointToFeature cur=pf;
        Point.Double center=boundary.getCenter();
        double dist=pf.getPoint().distance(center);
        for(int i=0; i<this.QT_NODE_CAPACITY; i++)
        {
            Point.Double temp=points.get(i).getPoint();
            double tempDist=temp.distance(center);
            if(tempDist>dist)
            {
                points.set(i, cur);
                cur=points.get(i);
                dist=tempDist;
                
            }
            
            
        }
        if (northWest.insert(cur)) {
            return true;
        }
        if (northEast.insert(cur)) {
            return true;
        }
        if (southWest.insert(cur)) {
            return true;
        }
        if (southEast.insert(cur)) {
            return true;
        }

        //should never be reached
        return false;
    }
    

    public void subdivide() {
        double qSize = boundary.getHalfSize() / 2;
        double x = boundary.getCenter().getX();
        double y = boundary.getCenter().getY();
        Boundary nw = new Boundary(x - qSize, y - qSize, qSize);
        northWest = new QuadTree(nw);
        Boundary ne = new Boundary(x + qSize, y - qSize, qSize);
        northEast = new QuadTree(ne);
        Boundary sw = new Boundary(x - qSize, y + qSize, qSize);
        southWest = new QuadTree(sw);
        Boundary se = new Boundary(x + qSize, y + qSize, qSize);
        southEast = new QuadTree(se);
    }

    public ArrayList<Feature> getPointsInBoundary(Boundary b) {
        ArrayList<Feature> ret = new ArrayList<Feature>();

        if (!b.intersects(boundary)) {
            return ret;
        }
        
            for (PointToFeature f : points) {
                Point.Double p=f.getPoint();
                if (b.containsPoint(p)) {
                    ret.add(f.getFeature());
                }
            }
        
        if (northWest == null) {
            return ret;
        }
        ret.addAll(northWest.getPointsInBoundary(b));
        ret.addAll(northEast.getPointsInBoundary(b));
        ret.addAll(southWest.getPointsInBoundary(b));
        ret.addAll(southEast.getPointsInBoundary(b));
        return ret;
    }
    public void draw()
    {
        QuadTreeVisualization frame = new QuadTreeVisualization();
        addBoundaries(frame);
        frame.setVisible(true);
        frame.repaint();
    }
    public void addBoundaries(QuadTreeVisualization frame)
    {
        frame.boundaries.add(boundary);
        frame.points.addAll(points);
        
        if(northWest!=null)
        {
            northWest.addBoundaries(frame);
            northEast.addBoundaries(frame);
            southWest.addBoundaries(frame);
            southEast.addBoundaries(frame);
        }
    }
}
