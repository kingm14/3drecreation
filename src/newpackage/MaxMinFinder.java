/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package newpackage;

import java.awt.image.*;
import java.util.*;
import pkg3drecreation.RGB;

/**
 *
 * @author kingm14
 */
public class MaxMinFinder {

    private ArrayList<PixelInfo> maxs;
    private ArrayList<PixelInfo> mins;
    public ArrayList<ArrayList<RGB[][]>> RGBPics;

    public MaxMinFinder()
    {
        maxs=new ArrayList<PixelInfo>();
        mins=new ArrayList<PixelInfo>();
                
    }
    /*public MaxMinFinder(ArrayList<ArrayList<BufferedImage>> octaves)
    {
        convertToRGB(octaves);
        maxs=new ArrayList<PixelInfo>();
        mins=new ArrayList<PixelInfo>();
    }*/
    public MaxMinFinder(ArrayList<ArrayList<RGB[][]>> octaves)
    {
        RGBPics=octaves;
        maxs=new ArrayList<PixelInfo>();
        mins=new ArrayList<PixelInfo>();
    }
    public ArrayList<PixelInfo> findMinMaxs() {
        ArrayList<PixelInfo> ret=new ArrayList<PixelInfo>();
        for (int octave = 0; octave < RGBPics.size(); octave++) {
            ArrayList<RGB[][]> scales = RGBPics.get(octave);
            for (int scale = 1; scale < scales.size()-1; scale++) {
                
                System.out.println(scale+"/"+scales.size());
                RGB[][] pic = scales.get(scale);
                for (int x = 1; x < pic.length - 1; x++) {
                    for (int y = 1; y < pic[0].length - 1; y++) {
                        int val=isMinOrMax(octave,scale,x,y);
                        //System.out.println(val);
                        if(val!=0)
                        {
                            PixelInfo info=new PixelInfo(octave,scale,x,y,val);
                            info.setPrintString(""+val);
                            ret.add(info);
                            if(val>0)
                            {
                                getMaxs().add(info);
                            }
                            else
                            {
                                getMins().add(info);
                            }
                        }
                    }
                }

            }
        }
        return ret;
    }

    public void convertToRGB(ArrayList<ArrayList<BufferedImage>> octaves) {
        RGBPics = new ArrayList<ArrayList<RGB[][]>>();
        for (int octave = 0; octave < octaves.size(); octave++) {
            ArrayList<BufferedImage> scales = octaves.get(octave);
            ArrayList<RGB[][]> RGBScales = new ArrayList<RGB[][]>();
            System.out.println(scales.size());
            for (int scale = 0; scale < scales.size(); scale++) {
                RGBScales.add(ImageComputations.computeToArray(scales.get(scale)));
            }
            RGBPics.add(RGBScales);
        }
    }

    public int isMinOrMax(int pixOctave, int pixScale, int pixX, int pixY) {
        ArrayList<RGB[][]> scales = RGBPics.get(pixOctave);
        double intensity = getRGBAt(pixOctave, pixScale, pixX, pixY).getIntensity();
        //System.out.println(intensity+"vs ");
        boolean hasLower = false;
        boolean hasGreater = false;
        
        for (int scale = pixScale - 1; scale <= pixScale + 1; scale++) {
            for (int x = pixX - 1; x <= pixX + 1; x++) {
                for (int y = pixY - 1; y <= pixY + 1; y++) {
                    if (scale != pixScale || x != pixX || y != pixY) {
                        double curIntens = getRGBAt(pixOctave, scale, x, y).getIntensity();
                        //System.out.println(curIntens+" ");
                        if (curIntens < intensity) {
                            hasLower = true;

                        } else if (curIntens > intensity) {
                            hasGreater = true;
                        } else {
                            return 0;
                        }
                        if (hasLower && hasGreater) {
                            return 0;
                        }
                    }
                }
            }
        }
        //System.out.println();
        if(!hasLower)
        {
            return 1*(int)(intensity*1000);
        }
        return -1*(int)(intensity*1000);


    }

    public RGB getRGBAt(int pixOctave, int pixScale, int pixX, int pixY) {
        return RGBPics.get(pixOctave).get(pixScale)[pixX][pixY];
    }

    /**
     * @return the maxs
     */
    public ArrayList<PixelInfo> getMaxs() {
        return maxs;
    }

    /**
     * @param maxs the maxs to set
     */
    public void setMaxs(ArrayList<PixelInfo> maxs) {
        this.maxs = maxs;
    }

    /**
     * @return the mins
     */
    public ArrayList<PixelInfo> getMins() {
        return mins;
    }

    /**
     * @param mins the mins to set
     */
    public void setMins(ArrayList<PixelInfo> mins) {
        this.mins = mins;
    }
}
