/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package newpackage;

import com.jhlabs.image.*;
import java.awt.*;
import java.awt.geom.Point2D;
import java.awt.image.*;
import java.util.*;

/**
 *
 * @author kingm14
 */
public class ScaleSpaces {
    public ArrayList<ArrayList<BufferedImage>> createOctave(BufferedImage origImage, double blurChange)
    {
        return createOctave(origImage, blurChange,4,5);
    }
    public ArrayList<ArrayList<BufferedImage>> createOctave(BufferedImage origImage, double blurChange, int numScales, int numBlurs)
    {
        
   
    
        //double blurChange=Math.sqrt(2);
        
        
        ArrayList<ArrayList<BufferedImage>> octaves = new ArrayList<ArrayList<BufferedImage>>();
        
        BufferedImage curImage=new BufferedImage(origImage.getWidth()*2, origImage.getHeight()*2,origImage.getType());
        BufferedImage nextImage=null;
        ScaleFilter sf=new ScaleFilter((int)(origImage.getWidth()*2),(int)( origImage.getHeight()*2));
        sf.filter(origImage, curImage);
        //BufferedImage temp2=new BufferedImage(origImage.getWidth()/((int)Math.pow(2, depth)), origImage.getHeight()/((int)Math.pow(2, depth)),origImage.getType());
        double blurVal1=0.707107;
        for(int i=0; i<numScales; i++)
        {
            double blurVal=blurVal1;
            
            ArrayList<BufferedImage> scales= new ArrayList<BufferedImage>();
            for(int j=0; j<numBlurs; j++)
            {
                
                
                
                //System.out.println("init blur val");
                
                //float blurVal=(((float)(Math.pow(blurChange,(((3-i)*2)-1))+Math.pow(blurChange, ((4-j)-1)))));
                
                //System.out.println("done blur val  "+(timeEnd-timeStart));
                
                //System.out.println();
                
                
                
                //System.out.println();
                
                //BufferedImage temp2=new BufferedImage((int)(origImage.getWidth()/(Math.pow(2, i-1))),(int)( origImage.getHeight()/(Math.pow(2, i-1))),origImage.getType());
                //BufferedImage temp3=new BufferedImage(origImage.getWidth(),origImage.getHeight(),origImage.getType());
                //BufferedImage temp4=new BufferedImage(temp2.getWidth(),temp2.getHeight(),temp2.getType());
                //gf.filter(origImage, temp3);
                
                //sf.filter(temp3, temp2);
                //System.out.println("start Gauss filtering");
                
                //System.out.println("done Gauss filtering  "+(timeEnd-timeStart));
                
                //System.out.println();
                
                //System.out.println("start scale Filter");
                
                
                //timeStart=System.currentTimeMillis();
                
            
                /*BufferedImage afterGauss=new BufferedImage(origImage.getWidth(),origImage.getHeight(),origImage.getType());
                BufferedImage afterScale=new BufferedImage((int)(origImage.getWidth()/(Math.pow(2, i-1))),(int)( origImage.getHeight()/(Math.pow(2, i-1))),origImage.getType());
                
                gf.filter(origImage, afterGauss);
                sf.filter(afterGauss,afterScale);
                scales.add(afterScale);*/
                //BufferedImage afterScale=new BufferedImage((int)(curImage.getWidth()/2),(int)( curImage.getHeight()/2), curImage.getType());
                BufferedImage afterGauss=new BufferedImage(curImage.getWidth(),curImage.getHeight(),curImage.getType());
                GaussianBlur gf=new GaussianBlur((float)blurVal*3);
                //sf.filter(origImage, afterScale);
                gf.filter(curImage, afterGauss);
                //GaussianBlur2.gaussianBlur(afterScale, blurVal);
                //timeEnd=System.currentTimeMillis();
                //System.out.println("done scale Filter  "+(timeEnd-timeStart));
                scales.add(afterGauss);
                if(j==numBlurs/2)
                {
                    nextImage=afterGauss;
                }
                //scales.add(temp2);
                //System.out.print(gf.getRadius()+/*(float)(Math.pow(blurChange,((i*2)-1+j)))+*/"     ");
                String print=""+blurVal;
                while(print.length()<8)
                {
                    print+="0";
                }
                /*print=print.substring(0, 8);
                System.out.print(print+"     ");*/
                blurVal*=blurChange;
                

            }
            curImage=new BufferedImage((int)(nextImage.getWidth()/2),(int)( nextImage.getHeight()/2),nextImage.getType());
            sf=new ScaleFilter((int)(nextImage.getWidth()/2),(int)( nextImage.getHeight()/2));
            sf.filter(nextImage, curImage);
            //curImage=nextImage;
            System.out.println();
            octaves.add(scales);
            /*blurVal1*=blurChange;
            blurVal1*=blurChange;*/
            //System.out.println();
        }
        /*BufferedImage temp=gf.filter(origImage, null);
        
        sf.filter(origImage, temp);
        ret.add(next);
        ret.addAll(splitImage(orignext,depth+1));*/
        return octaves;
        
        
        
    }
    public ArrayList<ArrayList<BufferedImage>> createOctave(BufferedImage origImage)
    {
        double blurChange=Math.sqrt(2);
        return createOctave(origImage, blurChange);
    }
    public static void main(String[] args)
    {
        ScaleSpaces ss=new ScaleSpaces();
        ss.createOctave(new BufferedImage(100,100,BufferedImage.TYPE_INT_RGB));
    }
}
