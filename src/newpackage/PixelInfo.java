/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package newpackage;

/**
 *
 * @author kingm14
 */
public class PixelInfo {
    private int octave;
    private int scale;
    private double x;
    private double y;
    private int minOrMax;
    private String printString="";
    
    public PixelInfo(int octave, int scale, double x, double y)
    {
        this.octave=octave;
        this.scale=scale;
        this.x=x;
        this.y=y;
        minOrMax=0;
    }
    public PixelInfo(int octave, int scale, double x, double y, int minOrMax)
    {
        this.octave=octave;
        this.scale=scale;
        this.x=x;
        this.y=y;
        this.minOrMax=minOrMax;
    }
    /**
     * @return the octave
     */
    public int getOctave() {
        return octave;
    }

    /**
     * @param octave the octave to set
     */
    public void setOctave(int octave) {
        this.octave = octave;
    }

    /**
     * @return the scale
     */
    public int getScale() {
        return scale;
    }

    /**
     * @param scale the scale to set
     */
    public void setScale(int scale) {
        this.scale = scale;
    }

    /**
     * @return the x
     */
    public double getX() {
        return x;
    }

    /**
     * @param x the x to set
     */
    public void setX(double x) {
        this.x = x;
    }

    /**
     * @return the y
     */
    public double getY() {
        return y;
    }

    /**
     * @param y the y to set
     */
    public void setY(double y) {
        this.y = y;
    }

    /**
     * @return the minOrMax
     */
    public int getMinOrMax() {
        return minOrMax;
    }

    /**
     * @param minOrMax the minOrMax to set
     */
    public void setMinOrMax(int minOrMax) {
        this.minOrMax = minOrMax;
    }
    public String toString()
    {
        return ("("+octave+","+scale+","+x+","+y+"):\""+getPrintString()+"\"");
    }

    /**
     * @return the printString
     */
    public String getPrintString() {
        return printString;
    }

    /**
     * @param printString the printString to set
     */
    public void setPrintString(String printString) {
        this.printString = printString;
    }
}
