/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package newpackage;

import static com.jhlabs.image.ConvolveFilter.CLAMP_EDGES;
import com.jhlabs.image.GaussianFilter;
import java.awt.image.*;
import pkg3drecreation.RGB;

/**
 *
 * @author kingm14
 */
public class LaplacianOfGaussian {
    public static RGB[][] computeDifferences(BufferedImage image1, BufferedImage image2, float sigma1, float sigma2)
    {
        GaussianBlur gf1=null;
        GaussianBlur gf2=null;
        if(sigma1>sigma2)
        {
            gf1=new GaussianBlur(sigma1*3);
            gf2=new GaussianBlur(sigma1*3, sigma2);
        }
        else
        {
            gf1=new GaussianBlur(sigma2*3,sigma1);
            gf2=new GaussianBlur(sigma2*3);
        }
        
        float[] kernel1=gf1.getKernel().getKernelData(null);
        float[] kernel2=gf2.getKernel().getKernelData(null);
        
        float[] kernel3=new float[kernel1.length];
        
        for(int i=0; i<kernel1.length; i++)
        {
            kernel3[i]=kernel1[i]-kernel2[i];
        }
        Kernel k=new Kernel(gf1.getKernel().getWidth(),gf1.getKernel().getHeight(),kernel3);
        
        gf1.setKernel(k);
        return ImageComputations.computeToArray(gf1.filter(image1, null));
        
    }
    public static BufferedImage computeDifferencesAsImage(BufferedImage image1, BufferedImage image2, float sigma1, float sigma2)
    {
        GaussianBlur gf1=null;
        GaussianBlur gf2=null;
        if(sigma1>sigma2)
        {
            gf1=new GaussianBlur(sigma1*3);
            gf2=new GaussianBlur(sigma1*3, sigma2);
        }
        else
        {
            gf1=new GaussianBlur(sigma2*3,sigma1);
            gf2=new GaussianBlur(sigma2*3);
        }
        
        float[] kernel1=gf1.getKernel().getKernelData(null);
        float[] kernel2=gf2.getKernel().getKernelData(null);
        
        float[] kernel3=new float[kernel1.length];
        
        for(int i=0; i<kernel1.length; i++)
        {
            kernel3[i]=kernel1[i]-kernel2[i];
        }
        Kernel k=new Kernel(gf1.getKernel().getWidth(),gf1.getKernel().getHeight(),kernel3);
        
        gf1.setKernel(k);
        return gf1.filter(image1, null);
        
    }
    
}
