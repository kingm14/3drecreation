/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package newpackage;

import java.awt.*;
import java.awt.image.*;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Vector;
import javax.imageio.ImageIO;
import javax.swing.*;
import pkg3drecreation.RGB;
//import mpi.cbg.fly.SIFT;
import mpi.cbg.fly.Feature;
/**
 *
 * @author kingm14
 */
public class ScaleSpacesTest extends javax.swing.JFrame {

    /**
     * Creates new form ScaleSpacesTest
     */
    //NEW!
    ArrayList<ArrayList<BufferedImage>> octaves;
    //ArrayList<ArrayList<BufferedImage>> laplacciansImage;
    JLabel label;
    boolean inited = false;
    ArrayList<PixelInfo> minMaxs;
    ArrayList<Point> drawPoints;
    public ScaleSpacesTest() {
        initComponents();
        octaves = null;

        //imageGraphics=image.getGraphics();
        label = new JLabel();
        minMaxs = null;
        drawPoints=null;
    }

    public void paint(Graphics g) {

        super.paint(g);
        Point viewPoint = scrollPane.getViewport().getViewPosition();
        System.out.println(viewPoint);
        int sumX = 0;
        int sumY = 0;
        System.out.println(scrollPane.getHorizontalScrollBarPolicy());

        /*if(laplacciansImage!=null)
         {
         System.out.println("laplaccian");
         Graphics imageGraphics;  //NEW!
         BufferedImage image;  
         //double scale=Math.pow(2, -1*octaves.size())*(Math.pow(2, octaves.size()+1)-1);
         BufferedImage firstImg=laplacciansImage.get(0).get(0);
         System.out.println((int)(firstImg.getWidth()*2)+"x"+(int)(firstImg.getHeight()*5));
         System.out.println(Runtime.getRuntime().freeMemory());
         image=new BufferedImage((int)(firstImg.getWidth()*2), (int)(firstImg.getHeight()*5), BufferedImage.TYPE_INT_RGB);
         imageGraphics=image.getGraphics();
            
         for(int i=0; i<laplacciansImage.size(); i++)
         {
         ArrayList<BufferedImage> scales=laplacciansImage.get(i);
         sumY=0;
         for(int j=0; j<scales.size(); j++)
         {
         BufferedImage temp=scales.get(j);
         imageGraphics.drawImage(temp, sumX, sumY, this);
         sumY+=temp.getHeight();
         }
                
         sumX+=scales.get(0).getWidth();
         }
         if(minMaxs!=null)
         {
         imageGraphics.setColor(Color.red);
         for(PixelInfo info: minMaxs)
         {
         int octave=info.getOctave();
         int scale=info.getScale();
         double x=info.getX();
         double y=info.getY();
         int pointX=0;
         for(int i=0; i<octave; i++)
         {
         pointX+=laplacciansImage.get(i).get(0).getWidth();
         }
         pointX+=y;
                    
         int pointY=0;
         pointY=laplacciansImage.get(octave).get(0).getHeight()*scale;
         pointY+=x;
         if(info.getMinOrMax()>0)
         imageGraphics.setColor(Color.red);
         else if(info.getMinOrMax()<0)
         imageGraphics.setColor(Color.blue);
         imageGraphics.fillRect(pointX-2,pointY-2,4,4);
         }
         }
         Icon icon=new ImageIcon(image);
         //label = new JLabel(icon);
         label.setIcon(icon);
         if(!inited)
         {
         scrollPane.getViewport().add(label);
         inited=true;
         }
            
         }
         else */        
        if (octaves != null) {
            //
                Graphics imageGraphics;  //NEW!
                BufferedImage image;
                //double scale=Math.pow(2, -1*octaves.size())*(Math.pow(2, octaves.size()+1)-1);
                BufferedImage firstImg = octaves.get(0).get(0);
                //if (!inited) {
                image = new BufferedImage((int) (firstImg.getWidth() * 2), (int) (firstImg.getHeight() * 5), BufferedImage.TYPE_INT_RGB);
                imageGraphics = image.getGraphics();

                for (int i = 0; i < octaves.size(); i++) {
                    ArrayList<BufferedImage> scales = octaves.get(i);
                    sumY = 0;
                    for (int j = 0; j < scales.size(); j++) {
                        BufferedImage temp = scales.get(j);
                        imageGraphics.drawImage(temp, sumX, sumY, this);
                        sumY += temp.getHeight();
                    }

                    sumX += scales.get(0).getWidth();
                }
                //}
                //else
                //{
                    /*image=(BufferedImage)((ImageIcon)label.getIcon()).getImage();
                    imageGraphics = image.getGraphics();*/
                //}
                    
            //}
            if (minMaxs != null) {
                imageGraphics.setColor(Color.red);
                for (PixelInfo info : minMaxs) {
                    int octave = info.getOctave();
                    int scale = info.getScale();
                    double x = info.getX();
                    double y = info.getY();
                    int pointX = 0;
                    for (int i = 0; i < octave; i++) {
                        pointX += octaves.get(i).get(0).getWidth();
                    }
                    pointX += y;

                    int pointY = 0;
                    pointY = octaves.get(octave).get(0).getHeight() * scale;
                    pointY += x;
                    if (info.getMinOrMax() > 0) {
                        imageGraphics.setColor(Color.red);
                    } else if (info.getMinOrMax() < 0) {
                        imageGraphics.setColor(Color.blue);
                    }
                    imageGraphics.fillRect(pointX - 2, pointY - 2, 4, 4);
                }
            }
            if(drawPoints!=null)
            {
                g.setColor(Color.blue);
                for(Point p:drawPoints)
                {
                    imageGraphics.fillRect((int)p.getX() - 2, (int)p.getY() - 2, 4, 4);
                }
            }
            Icon icon = new ImageIcon(image);
            //label = new JLabel(icon);
            label.setIcon(icon);
            if (!inited) {
                scrollPane.getViewport().add(label);
                inited = true;
            }


        }

    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLayeredPane1 = new javax.swing.JLayeredPane();
        fileChooserFrame = new javax.swing.JInternalFrame();
        fileChooser = new javax.swing.JFileChooser();
        jButton1 = new javax.swing.JButton();
        blurSpinner = new javax.swing.JSpinner();
        jButton2 = new javax.swing.JButton();
        jButton3 = new javax.swing.JButton();
        jButton4 = new javax.swing.JButton();
        jButton5 = new javax.swing.JButton();
        scrollPane = new javax.swing.JScrollPane();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        fileChooserFrame.setVisible(true);

        fileChooser.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                fileChooserActionPerformed(evt);
            }
        });

        jButton1.setText("Convert to 2d array");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        blurSpinner.setModel(new javax.swing.SpinnerNumberModel(Double.valueOf(1.414214d), null, null, Double.valueOf(1.0d)));

        jButton2.setText("Laplaccians");
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });

        jButton3.setText("Find Mins/Maxs");
        jButton3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton3ActionPerformed(evt);
            }
        });

        jButton4.setText("Intensity");
        jButton4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton4ActionPerformed(evt);
            }
        });

        jButton5.setText("Cheat");
        jButton5.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton5ActionPerformed(evt);
            }
        });

        org.jdesktop.layout.GroupLayout fileChooserFrameLayout = new org.jdesktop.layout.GroupLayout(fileChooserFrame.getContentPane());
        fileChooserFrame.getContentPane().setLayout(fileChooserFrameLayout);
        fileChooserFrameLayout.setHorizontalGroup(
            fileChooserFrameLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(fileChooserFrameLayout.createSequentialGroup()
                .addContainerGap()
                .add(fileChooserFrameLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(fileChooser, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 560, Short.MAX_VALUE)
                    .add(org.jdesktop.layout.GroupLayout.TRAILING, fileChooserFrameLayout.createSequentialGroup()
                        .add(0, 113, Short.MAX_VALUE)
                        .add(fileChooserFrameLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.TRAILING)
                            .add(fileChooserFrameLayout.createSequentialGroup()
                                .add(jButton4)
                                .addPreferredGap(org.jdesktop.layout.LayoutStyle.UNRELATED)
                                .add(jButton2))
                            .add(blurSpinner, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 147, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                        .add(18, 18, 18)
                        .add(fileChooserFrameLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                            .add(fileChooserFrameLayout.createSequentialGroup()
                                .add(fileChooserFrameLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.TRAILING)
                                    .add(jButton1)
                                    .add(jButton3))
                                .add(36, 36, 36))
                            .add(org.jdesktop.layout.GroupLayout.TRAILING, fileChooserFrameLayout.createSequentialGroup()
                                .add(jButton5)
                                .add(47, 47, 47))))))
        );
        fileChooserFrameLayout.setVerticalGroup(
            fileChooserFrameLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(fileChooserFrameLayout.createSequentialGroup()
                .addContainerGap()
                .add(fileChooser, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 462, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .add(fileChooserFrameLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(fileChooserFrameLayout.createSequentialGroup()
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(jButton1))
                    .add(org.jdesktop.layout.GroupLayout.TRAILING, fileChooserFrameLayout.createSequentialGroup()
                        .add(7, 7, 7)
                        .add(blurSpinner, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 28, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)))
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(fileChooserFrameLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(fileChooserFrameLayout.createSequentialGroup()
                        .add(fileChooserFrameLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                            .add(jButton2)
                            .add(jButton4))
                        .addContainerGap())
                    .add(fileChooserFrameLayout.createSequentialGroup()
                        .add(jButton3)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .add(jButton5))))
        );

        fileChooserFrame.setBounds(0, 0, 590, 610);
        jLayeredPane1.add(fileChooserFrame, javax.swing.JLayeredPane.DEFAULT_LAYER);

        scrollPane.setHorizontalScrollBarPolicy(javax.swing.ScrollPaneConstants.HORIZONTAL_SCROLLBAR_ALWAYS);
        scrollPane.setVerticalScrollBarPolicy(javax.swing.ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
        scrollPane.addInputMethodListener(new java.awt.event.InputMethodListener() {
            public void inputMethodTextChanged(java.awt.event.InputMethodEvent evt) {
            }
            public void caretPositionChanged(java.awt.event.InputMethodEvent evt) {
                scrollPaneCaretPositionChanged(evt);
            }
        });
        scrollPane.setBounds(0, 0, 1710, 770);
        jLayeredPane1.add(scrollPane, javax.swing.JLayeredPane.DEFAULT_LAYER);

        org.jdesktop.layout.GroupLayout layout = new org.jdesktop.layout.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(jLayeredPane1, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 1720, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(org.jdesktop.layout.GroupLayout.TRAILING, jLayeredPane1, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 776, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void fileChooserActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_fileChooserActionPerformed
        // TODO add your handling code here:
        BufferedImage origImage = null;
        try {

            origImage = ImageIO.read(fileChooser.getSelectedFile());

        } catch (IOException ex) {
        }
        ScaleSpaces ss = new ScaleSpaces();

        octaves = ss.createOctave(origImage, (Double) blurSpinner.getValue());
        //fileChooserFrame.setVisible(false);
        repaint();
    }//GEN-LAST:event_fileChooserActionPerformed

    private void scrollPaneCaretPositionChanged(java.awt.event.InputMethodEvent evt) {//GEN-FIRST:event_scrollPaneCaretPositionChanged
        // TODO add your handling code here:
    }//GEN-LAST:event_scrollPaneCaretPositionChanged

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        // TODO add your handling code here:
        BufferedImage origImage = null;
        try {

            origImage = ImageIO.read(fileChooser.getSelectedFile());

        } catch (IOException ex) {
        }
        ImageComputations.computeToArray(origImage);
    }//GEN-LAST:event_jButton1ActionPerformed

    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed
        // TODO add your handling code here:
        double blurVal = (Double) blurSpinner.getValue();


        ArrayList<ArrayList<BufferedImage>> laplacciansImage = new ArrayList<ArrayList<BufferedImage>>();


        for (int i = 0; i < octaves.size(); i++) {
            ArrayList<BufferedImage> scale = octaves.get(i);

            ArrayList<BufferedImage> temp2 = new ArrayList<BufferedImage>();
            for (int j = 0; j < scale.size() - 1; j++) {
                BufferedImage dog = LaplacianOfGaussian.computeDifferencesAsImage(scale.get(j), scale.get(j + 1), (float) (blurVal * j), (float) (blurVal * (j + 1)));
                RGB[][] dog1 = ImageComputations.computeToArray(dog);

                temp2.add(dog);
            }

            laplacciansImage.add(temp2);
        }
        octaves = laplacciansImage;
        repaint();
    }//GEN-LAST:event_jButton2ActionPerformed

    private void jButton3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton3ActionPerformed
        // TODO add your handling code here:
        //ArrayList<ArrayList<RGB[][]>> laplaccians;
        MaxMinFinder finder = new MaxMinFinder();
        finder.convertToRGB(octaves);
        minMaxs = finder.findMinMaxs();
        ArrayList<PixelInfo> out = finder.getMaxs();
        System.out.println("##########################################");
        System.out.println("Maxs:");
        for (int i = 0; i < out.size(); i++) {
            System.out.println(out.get(i));
        }
        out = finder.getMins();
        System.out.println("-------------------------------------------");
        System.out.println("Mins:");
        for (int i = 0; i < out.size(); i++) {
            System.out.println(out.get(i));
        }
        System.out.println("##########################################");
        System.gc();
        Runtime.getRuntime().gc();
        repaint();
    }//GEN-LAST:event_jButton3ActionPerformed

    private void jButton4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton4ActionPerformed
        // TODO add your handling code here:
        for (int octave = 0; octave < octaves.size(); octave++) {
            ArrayList<BufferedImage> scales = octaves.get(octave);
            for (int scale = 0; scale < scales.size(); scale++) {
                RGB[][] orig = ImageComputations.computeToArray(scales.get(scale));
                int[][] temp = new int[orig.length][orig[0].length];
                for (int r = 0; r < orig.length; r++) {
                    for (int c = 0; c < orig[0].length; c++) {
                        RGB cur = orig[r][c];
                        double intensity = cur.getIntensity();
                        temp[r][c] = (int) intensity;
                        //System.out.print((int)(intensity)+" ");

                    }
                    //System.out.println();
                }
                //System.out.println("-------------------------------------------------------------");
                octaves.get(octave).set(scale, (BufferedImage) ImageComputations.getImage(temp));
            }
            //System.out.println("##################################################################");
        }
        repaint();
    }//GEN-LAST:event_jButton4ActionPerformed

    private void jButton5ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton5ActionPerformed
        // TODO add your handling code here:
        BufferedImage origImage = null;
        octaves=new ArrayList<ArrayList<BufferedImage>>();
        ArrayList<BufferedImage> temp=new ArrayList<BufferedImage>();
        
        try {

            origImage = ImageIO.read(fileChooser.getSelectedFile());

        } catch (IOException ex) {
        }
        temp.add(origImage);
        octaves.add(temp);
        drawPoints=new ArrayList<Point>();
        Vector<Feature> features=SIFT.getFeatures(origImage);
        for(Feature f:features)
        {
            
            float[] array=f.location;
            
            drawPoints.add(new Point(Math.round(array[0]),Math.round(array[1])));
            
            
        }
        repaint();
        //System.out.println(SIFT.getFeatures(origImage));
    }//GEN-LAST:event_jButton5ActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(ScaleSpacesTest.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(ScaleSpacesTest.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(ScaleSpacesTest.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(ScaleSpacesTest.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new ScaleSpacesTest().setVisible(true);
            }
        });
    }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JSpinner blurSpinner;
    private javax.swing.JFileChooser fileChooser;
    private javax.swing.JInternalFrame fileChooserFrame;
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton2;
    private javax.swing.JButton jButton3;
    private javax.swing.JButton jButton4;
    private javax.swing.JButton jButton5;
    private javax.swing.JLayeredPane jLayeredPane1;
    private javax.swing.JScrollPane scrollPane;
    // End of variables declaration//GEN-END:variables
}
