/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package newpackage;

import java.awt.image.*;
import java.io.File;
import java.util.*;
import javax.imageio.ImageIO;
import pkg3drecreation.RGB;

/**
 *
 * @author kingm14
 */
public class ImageComputations {

    public static RGB[][] computeToArray(BufferedImage image) {
        final byte[] pixels = ((DataBufferByte) image.getRaster().getDataBuffer()).getData();
        final int width = image.getWidth();
        final int height = image.getHeight();
        //System.out.println(width+","+height);
        final boolean hasAlphaChannel = image.getAlphaRaster() != null;
        System.out.println(width+" by "+ height);
        RGB[][] result = new RGB[height][width];
        if (hasAlphaChannel) {
            System.out.println("has alpha");
            final int pixelLength = 4;
            for (int pixel = 0, row = 0, col = 0; pixel < pixels.length; pixel += pixelLength) {
                int argb = 0;
                argb += (((int) pixels[pixel] & 0xff) << 24); // alpha
                argb += ((int) pixels[pixel + 1] & 0xff); // blue
                argb += (((int) pixels[pixel + 2] & 0xff) << 8); // green
                argb += (((int) pixels[pixel + 3] & 0xff) << 16); // red
                RGB rgb = new RGB((((int) pixels[pixel + 3] & 0xff)), (((int) pixels[pixel + 2] & 0xff)), ((int) pixels[pixel + 1] & 0xff));
                result[row][col] = rgb;
                col++;
                if (col == width) {
                    col = 0;
                    row++;
                }
            }
        } else {
            final int pixelLength = 3;
            for (int pixel = 0, row = 0, col = 0; pixel < pixels.length-2; pixel += pixelLength) {
                /*int argb = 0;
                 argb += -16777216; // 255 alpha
                 argb += ((int) pixels[pixel] & 0xff); // blue
                 argb += (((int) pixels[pixel + 1] & 0xff) << 8); // green
                 argb += (((int) pixels[pixel + 2] & 0xff) << 16); // red*/
                RGB rgb = new RGB((((int) pixels[pixel + 2] & 0xff)), (((int) pixels[pixel +1] & 0xff)), ((int) pixels[pixel] & 0xff));
                result[row][col] = rgb;
                col++;
                if (col == width) {
                    col = 0;
                    row++;
                }
            }
        }

        return result;
    }

    public static java.awt.Image getImage(int pixels[][]) {
        int w = pixels.length;
        int h = pixels[0].length;
        BufferedImage image = new BufferedImage(w, h, BufferedImage.TYPE_BYTE_GRAY);
        WritableRaster raster = (WritableRaster) image.getData();
        for (int i = 0; i < w; i++) {
            for (int j = 0; j < h; j++) {
                raster.setSample(i, j, 0, pixels[i][j]);
            }
        }


        return image;
    }
    public static double getIntensityOfPixel(double r, double g, double b)
    {
        return ((0.2989 * r) + (0.5870 * g) + (0.1140 * b));
    }
    public static double getIntensityOfPixel(RGB rgb)
    {
        return getIntensityOfPixel(rgb.getR(), rgb.getG(), rgb.getB());
    }
    /*public static double[][] convolute(double[][] a1, Kernel kernel)
    {
        double[][] ret=new double[a1.length-kernel.getWidth()+1][a1[0].length-kernel.getHeight()+1];
        float[] kArray=new float[kernel.getWidth()*kernel.getHeight()];
        kernel.getKernelData(kArray);
        
        int curK=0;
        for(int j)
        
    }*/
}
