/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pkg3drecreation;

import DelaunayTriangulation.Triangle_dt;
import java.awt.*;
import java.util.ArrayList;

/**
 *
 * @author kingm14
 */
public class DepthTriangle extends DepthObject {

    private DepthPoint v1, v2, v3;
    private Polygon poly;
    CircumCircle circle;
    private double alpha;

    public DepthTriangle(DepthPoint v1, DepthPoint v2, DepthPoint v3) {
        this.v1 = v1;
        this.v2 = v2;
        this.v3 = v3;
        //System.out.println(v1.getP()+" "+v2.getP()+" "+v3.getP());
        poly = new Polygon();
        poly.addPoint((int) v1.getP().getX(), (int) v1.getP().getY());
        poly.addPoint((int) v2.getP().getX(), (int) v2.getP().getY());
        poly.addPoint((int) v3.getP().getX(), (int) v3.getP().getY());
        circle = new CircumCircle(v1.getP(), v2.getP(), v3.getP());
        alpha = .75;

    }

    public DepthTriangle(Triangle_dt dtTri) {

        this.v1 = dtTri.p1().getOriginalPoint();
        this.v2 = dtTri.p2().getOriginalPoint();
        this.v3 = dtTri.p3().getOriginalPoint();
        //System.out.println(v1.getP()+" "+v2.getP()+" "+v3.getP());
        poly = new Polygon();
        poly.addPoint((int) v1.getP().getX(), (int) v1.getP().getY());
        poly.addPoint((int) v2.getP().getX(), (int) v2.getP().getY());
        poly.addPoint((int) v3.getP().getX(), (int) v3.getP().getY());
        circle = new CircumCircle(v1.getP(), v2.getP(), v3.getP());
        alpha = .75;
    }

    public boolean contains(Point p) {
        return poly.contains(p);
    }

    public boolean contains(DepthPoint p) {
        return contains(p.getP());
    }

    public ArrayList<DepthTriangle> split(DepthPoint p) {
        ArrayList<DepthTriangle> list = new ArrayList<DepthTriangle>();
        list.add(new DepthTriangle(v1, v2, p));
        list.add(new DepthTriangle(v2, v3, p));
        list.add(new DepthTriangle(v3, v1, p));
        return list;
    }

    public boolean hasVertex(DepthPoint d) {
        return v1 == d || v2 == d || v3 == d;
    }

    public void paint(Graphics g, double scale) {
        //System.out.println("paint triangle");

        Graphics2D g2d = (Graphics2D) g.create();

        int v1X = (int) (v1.getP().getX() * scale);
        int v1Y = (int) (v1.getP().getY() * scale);
        Point v1P = new Point(v1X, v1Y);
        int v2X = (int) (v2.getP().getX() * scale);
        int v2Y = (int) (v2.getP().getY() * scale);
        Point v2P = new Point(v2X, v2Y);
        int v3X = (int) (v3.getP().getX() * scale);
        int v3Y = (int) (v3.getP().getY() * scale);
        Point v3P = new Point(v3X, v3Y);

        Point[] points = new Point[]{v1P, v2P, v3P};
        Color[] colors = new Color[]{v1.getColor(), v2.getColor(), v3.getColor()};
        g2d.setComposite(AlphaComposite.getInstance(AlphaComposite.SRC_OVER, (float) alpha));
        this.drawGouraud(points, colors, 1, g2d);
//        GradientPaint grad=new GradientPaint(v1P,v1.getColor(),v2P,v2.getColor());
//        g2d.setPaint(grad);
//        Polygon line=new Polygon();
//        line.addPoint(v1X, v1Y);
//        line.addPoint(v1X+1, v1Y);
//        line.addPoint(v2X+1, v2Y);
//        line.addPoint(v2X, v2Y);
//        g2d.fillPolygon(line);
//        g2d.setComposite(AlphaComposite.getInstance(AlphaComposite.SRC_OVER, 1.0f));
//        g2d.fillPolygon(poly);
//        
//        //g2d.drawLine(v1X, v1Y, v2X, v2Y);
//        grad=new GradientPaint(v2P,v2.getColor(),v3P,v3.getColor());
//        g2d.setPaint(grad);
//        g2d.drawLine(v2X, v2Y, v3X, v3Y);
//        line=new Polygon();
//        line.addPoint(v2X, v2Y);
//        line.addPoint(v2X+1, v2Y);
//        line.addPoint(v3X+1, v3Y);
//        line.addPoint(v3X, v3Y);
//        g2d.fillPolygon(line);
//        g2d.setComposite(AlphaComposite.getInstance(AlphaComposite.SRC_OVER, 0.66f));
//        g2d.fillPolygon(poly);
//        g2d.setComposite(AlphaComposite.getInstance(AlphaComposite.SRC_OVER, 1.0f));
//        
//        grad=new GradientPaint(v3P,v3.getColor(),v1P,v1.getColor());
//        
//        g2d.setPaint(grad);
//        g2d.drawLine(v3X, v3Y, v1X, v1Y);
//        line=new Polygon();
//        line.addPoint(v3X, v3Y);
//        line.addPoint(v3X+1, v3Y);
//        line.addPoint(v1X+1, v1Y);
//        line.addPoint(v1X, v1Y);
//        g2d.fillPolygon(line);
//        g2d.setComposite(AlphaComposite.getInstance(AlphaComposite.SRC_OVER, 0.33f));
//        g2d.fillPolygon(poly);
//        g2d.setComposite(AlphaComposite.getInstance(AlphaComposite.SRC_OVER, 1.0f));

        v1.paint(g2d, scale);
        v2.paint(g2d, scale);
        v3.paint(g2d, scale);

        g2d.dispose();

    }

    public void drawGouraud(Point[] points, Color[] colors, int pixelSize, Graphics g) {
        Polygon poly = new Polygon();
        double sum = 0;
        for (int i = 0; i < points.length; i++) {
            poly.addPoint((int) points[i].getX(), (int) points[i].getY());
            int next = i + 1;
            next = next % points.length;
            sum += points[i].distance(points[next]);
        }
        sum /= points.length;

        Rectangle bounds = poly.getBounds();
        int minX = (int) bounds.getMinX();
        int maxX = (int) bounds.getMaxX();
        int minY = (int) bounds.getMinY();
        int maxY = (int) bounds.getMaxY();

        for (int x = minX; x < maxX; x += pixelSize) {
            for (int y = minY; y < maxY; y += pixelSize) {
                if (poly.contains(x, y)) {

                    double red = 0;//(c1.getRed()*prop1)+(c2.getRed()*prop2)+(c3.getRed()*prop3);
                    double green = 0;//(c1.getGreen()*prop1)+(c2.getGreen()*prop2)+(c3.getGreen()*prop3);
                    double blue = 0;//(c1.getBlue()*prop1)+(c2.getBlue()*prop2)+(c3.getBlue()*prop3);
                    for (int i = 0; i < points.length; i++) {
                        double dist = points[i].distance(x, y);

                        double prop = 1 - (dist / sum);
                        red += prop * colors[i].getRed();
                        green += prop * colors[i].getGreen();
                        blue += prop * colors[i].getBlue();

                    }

                    if (red > 255) {
                        red = 255;
                    } else if (red < 0) {
                        red = 0;
                    }
                    if (green > 255) {
                        green = 255;
                    } else if (green < 0) {
                        green = 0;
                    }
                    if (blue > 255) {
                        blue = 255;
                    } else if (blue < 0) {
                        blue = 0;
                    }
                    //System.out.println(red+","+green+","+blue);
                    Color tempColor = new Color((int) red, (int) green, (int) blue);
                    g.setColor(tempColor);
                    g.fillRect(x, y, pixelSize, pixelSize);
                }

            }
        }
    }

    public boolean withinCircumCircle(Point p) {

        return circle.contains(p);
    }

    public boolean isToLeft(Point p) {
        return circle.getP().getX() + circle.getRad() < p.getX();
    }

    public ArrayList<Edge> getEdges() {
        ArrayList<Edge> edges = new ArrayList<Edge>();
        edges.add(new Edge(v1, v2));
        edges.add(new Edge(v2, v3));
        edges.add(new Edge(v3, v1));
        return edges;
    }

    public ArrayList<DepthPoint> getVertices() {
        ArrayList<DepthPoint> ret = new ArrayList<DepthPoint>();
        ret.add(v1);
        ret.add(v2);
        ret.add(v3);

        return ret;
    }

    /**
     * @return the alpha
     */
    public double getAlpha() {
        return alpha;
    }

    /**
     * @param alpha the alpha to set
     */
    public void setAlpha(double alpha) {
        this.alpha = alpha;
    }

    public String toSaveString() {
        
        return "dt|" + v1.toSaveString() + ":" + v2.toSaveString() + ":" + v3.toSaveString()+"|"+getAlpha();
    }

    

}
