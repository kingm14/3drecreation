/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pkg3drecreation;

import java.awt.*;
import java.util.*;
import mpi.cbg.fly.Feature;

/**
 *
 * @author kingm14
 */
public class Corner implements Comparable,Saveable{
    private double x,y;
    private RGB color;
    private ArrayList<Corner> otherCorners;
    private ArrayList<Corner> matches;
    private double maxDist;
    private RGB[][] sur;
    private double cornerness;
    private int orientation;
    private double direction;
    private SiftName name;
    private String text="";
    private double matchDif;
    private double probRight=1;
    private double scale;
    private boolean isPaintingText;
    private ArrayList<Corner> connectedCorners;
    public Corner(int x, int y, RGB color)
    {
        sur=null;
        this.x=x;
        this.y=y;
        this.color=color;
        maxDist=0;
        connectedCorners=new ArrayList<Corner>();
        matches=new ArrayList<Corner>();
        isPaintingText=true;
    }
    public Corner(double x, double y, RGB color)
    {
        sur=null;
        this.x=x;
        this.y=y;
        this.color=color;
        maxDist=0;
        connectedCorners=new ArrayList<Corner>();
        matches=new ArrayList<Corner>();
        isPaintingText=true;
    }
    public int getX()
    {
        return (int)x;
    }
    public int getScaledX()
    {
        return (int)(x*Math.pow(2, scale - 1));
    }
    public int getScaledY()
    {
        return (int)(y*Math.pow(2, scale - 1));
    }
    public int getY()
    {
        return (int)y;
    }
    public double getXDouble()
    {
        
        return x;
    }
    public double getYDouble()
    {
        return y;
    }
    public Point getLocation()
    {
        return new Point((int)x,(int)y);
    }
    public Point.Double getLocationDouble()
    {
        return new Point.Double(x,y);
    }
    public Point getScaledLocation()
    {
        return new Point(getScaledX(),getScaledY());
    }
    public RGB getColor()
    {
        return color;
    }
    public void setX(double x)
    {
        this.x=x;
    }
    public void setY(double y)
    {
        this.y=y;
    }
    public void setLocation(Point p)
    {
        x=p.getX();
        y=p.getY();
    }
    public void setColor(RGB color)
    {
        this.color=color;
    }
    public double getDistance(Corner c)
    {
        return this.getLocation().distance(c.getLocation());
    }
    
    public ArrayList<Double> createDistanceList()
    {
        maxDist=0;
        ArrayList<Double> ret=new ArrayList<Double>();
        for(Corner c: otherCorners)
        {
            double d=(getDistance(c));
            if(d>maxDist)
            {
                maxDist=d;
            }
            ret.add(d);
        }
        Collections.sort(ret);
        return ret;
    }
    String valueString=null;
    public String toString()
    {
        
        int length=(""+((int)maxDist)).length();
        double d=0;
        StringBuilder str=new StringBuilder();
        str.append((int)color.toGray());
        str.append(".");
        ArrayList<Double> dList=createDistanceList();
        //System.out.println("  "+dList.size());
        for(Double dub: dList)
        {
            String temp=""+(dub.intValue());
            while(temp.length()<length)
            {
                temp+="0";
            }
            str.append(temp);
            
        }
        //System.out.println(str.toString());
        return str.toString();
    }
    /**
     * @return the otherCorners
     */
    public ArrayList<Corner> getOtherCorners() {
        return otherCorners;
    }

    /**
     * @param otherCorners the otherCorners to set
     */
    public void setOtherCorners(ArrayList<Corner> otherCorners) {
        this.otherCorners = otherCorners;
    }

    @Override
    public int compareTo(Object t) {
        if(!(t instanceof Corner))
        {
            throw new ClassCastException();
        }
        
        Corner c=(Corner)t;
        /*if(this.getColor().toIntString().equals(c.getColor().toIntString()))
        {
            ArrayList<Double> myDistances=this.createDistanceList();
            ArrayList<Double> theirDistances=c.createDistanceList();
            
            
        }
        else
        {
            return this.getColor().toIntString().compareTo(c.getColor().toIntString());
        }*/
        Double d1=Double.parseDouble(this.toString());
        Double d2=Double.parseDouble(c.toString());
        return d1.compareTo(d2);
    }

    /**
     * @return the sur
     */
    public RGB[][] getSur() {
        return sur;
    }

    /**
     * @param sur the sur to set
     */
    public void setSur(RGB[][] sur) {
        this.sur = sur;
    }

    /**
     * @return the cornerness
     */
    public double getCornerness() {
        return cornerness;
    }

    /**
     * @param cornerness the cornerness to set
     */
    public void setCornerness(double cornerness) {
        this.cornerness = cornerness;
    }

    /**
     * @return the connectedCorners
     */
    public ArrayList<Corner> getConnectedCorners() {
        return connectedCorners;
    }

    /**
     * @param connectedCorners the connectedCorners to set
     */
    public void setConnectedCorners(ArrayList<Corner> connectedCorners) {
        this.connectedCorners = connectedCorners;
    }
    public void addConnectedCorner(Corner c)
    {
        connectedCorners.add(c);
        connectedCorners.addAll(c.getConnectedCorners());
        for(int i=0; i<connectedCorners.size(); i++)
        {
            Corner cur=connectedCorners.get(i);
            ArrayList<Corner> temp=(ArrayList<Corner>) connectedCorners.clone();
            temp.remove(cur);
            temp.add(this);
            cur.setConnectedCorners(temp);
        }
        
    }
    
    public void addAllConnectedCorner(ArrayList<Corner> c)
    {
        connectedCorners.addAll(c);
    }

    /**
     * @return the orientation
     */
    public int getOrientation() {
        return orientation;
    }

    /**
     * @param orientation the orientation to set
     */
    public void setOrientation(int orientation) {
        this.orientation = orientation;
    }
    
    /**
     * @return the name
     */
    public SiftName getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(SiftName name) {
        this.name = name;
    }

    /**
     * @return the direction
     */
    public double getDirection() {
        return direction;
    }

    /**
     * @param direction the direction to set
     */
    public void setDirection(double direction) {
        this.direction = (direction+360)%360;
    }

    /**
     * @return the text
     */
    public String getText() {
        /*if(probRight!=1&&!text.equals(""))
        {
            return text+" "+(int)probRight;
        }*/
        if(!isPaintingText)
        {
            return "";
        }
        return text;
    }

    /**
     * @param text the text to set
     */
    public void setText(String text) {
        this.text = text;
    }

    /**
     * @return the matches
     */
    public ArrayList<Corner> getMatches() {
        return matches;
    }

    /**
     * @param matches the matches to set
     */
    public void setMatches(ArrayList<Corner> matches) {
        this.matches = matches;
    }
    public void addMatch(Corner c)
    {
        matches.add(c);
    }

    /**
     * @return the matchDif
     */
    public double getMatchDif() {
        return matchDif;
    }

    /**
     * @param matchDif the matchDif to set
     */
    public void setMatchDif(double matchDif) {
        this.matchDif = matchDif;
    }

    /**
     * @return the probRight
     */
    public double getProbRight() {
        return probRight;
    }

    /**
     * @param probRight the probRight to set
     */
    public void setProbRight(double probRight) {
        this.probRight = probRight;
    }

    /**
     * @return the scale
     */
    public double getScale() {
        return scale;
    }

    /**
     * @param scale the scale to set
     */
    public void setScale(double scale) {
        this.scale = scale;
    }

    /**
     * @return the paintText
     */
    public boolean isPaintingText() {
        return isPaintingText;
    }

    /**
     * @param paintText the paintText to set
     */
    public void setIsPaintingText(boolean paintText) {
        this.isPaintingText = paintText;
    }

    @Override
    public String toSaveString() {
        Feature f=getName().getFeature();
        StringBuilder locString=new StringBuilder();
        float[] loc=f.location;
        for(int i=0; i<loc.length; i++)
        {
            locString.append(loc[i]);
            if(i<loc.length-1)
            {
                locString.append(",");
            }
        }
        StringBuilder descString=new StringBuilder();
        float[] desc=f.descriptor;
        for(int i=0; i<desc.length; i++)
        {
            descString.append(desc[i]);
            if(i<desc.length-1)
            {
                descString.append(",");
            }
        }
        String featureString=f.scale+":"+f.orientation+":"+locString.toString()+":"+descString.toString();
        return "corner|"+getName()+"|"+x+","+y+"|"+getOrientation()+"|"+featureString;
    }
    
    
    
}
