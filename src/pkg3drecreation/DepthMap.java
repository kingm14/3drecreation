/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pkg3drecreation;

import java.awt.*;
import java.util.ArrayList;
import java.util.Collections;
import javax.vecmath.Point3d;

/**
 *
 * @author kingm14
 */
public class DepthMap {

    private ArrayList<DepthObject> depthMap;

    public DepthMap(ArrayList<DepthObject> depthMap) {
        this.depthMap = depthMap;
    }

    public static DepthMap createDepthMapXChangeOnly(ArrayList<Point[]> corners, Point3d camera1Pos, Point3d camera2Pos, double focalLength) {
        ArrayList<DepthObject> depthMap = new ArrayList<DepthObject>();
        double maxChange = 0;
        for (int i = 0; i < corners.size(); i++) {
            Point p1A = corners.get(i)[0];
            Point p2A = corners.get(i)[1];

            double b = camera1Pos.distance(camera2Pos);
            double f = focalLength;
            double deltaXL = p1A.getX();
            double deltaXR = p2A.getX();
            double depth = (b * f) / (deltaXL - deltaXR);

            /*
             for(int j=i+1; j<corners.size(); j++)
             {
             Point p1B=corners.get(j)[0];
             Point p2B=corners.get(j)[1];
                
             double length1=p1A.distance(p1B);
             double length2=p2A.distance(p2B);
                
             double change=Math.abs(length2-length1);
             DepthLine line=new DepthLine(p2A,p2B,change);
             depthMap.add(line);
             if(change>maxChange)
             {
             maxChange=change;
             }
             }*/

            if (depth > maxChange) {
                maxChange = depth;
            }
            depthMap.add(new DepthPoint(new Point((int) p1A.getY(), (int) p1A.getX()), depth));

        }
        for (int i = 0; i < depthMap.size(); i++) {
            depthMap.get(i).setMaxDepth(maxChange);
        }
        Collections.sort(depthMap);
        return new DepthMap(depthMap);
    }

    public static DepthMap createDepthMapXChangeOnly(ArrayList<Point[]> corners, double focalLength) {
        ArrayList<DepthObject> depthMap = new ArrayList<DepthObject>();
        double maxDepth = Integer.MIN_VALUE;
        double minDepth = Integer.MAX_VALUE;
        for (int i = 0; i < corners.size(); i++) {
            Point p1A = corners.get(i)[0];
            Point p2A = corners.get(i)[1];
            Point3d camera1Pos = new Point3d(0, 0, 0);
            Point3d camera2Pos = new Point3d(1, 0, 0);
            double b = camera1Pos.distance(camera2Pos);
            double f = focalLength;

            double deltaXL = p1A.getY();
            double deltaXR = p2A.getY();
            double depth = (b * f) / (deltaXL - deltaXR);
            if ((deltaXL - deltaXR) != 0) {

                //System.out.println(p1A+"-->"+p2A+":"+depth);
            /*
                 for(int j=i+1; j<corners.size(); j++)
                 {
                 Point p1B=corners.get(j)[0];
                 Point p2B=corners.get(j)[1];
                
                 double length1=p1A.distance(p1B);
                 double length2=p2A.distance(p2B);
                
                 double change=Math.abs(length2-length1);
                 DepthLine line=new DepthLine(p2A,p2B,change);
                 depthMap.add(line);
                 if(change>maxChange)
                 {
                 maxChange=change;
                 }
                 }*/

                if (depth > maxDepth) {
                    maxDepth = depth;
                }
                if (depth < minDepth) {
                    minDepth = depth;
                }
                depthMap.add(new DepthPoint(new Point((int) p1A.getY(), (int) p1A.getX()), depth));
            }

        }
        for (int i = 0; i < depthMap.size(); i++) {
            depthMap.get(i).setMaxDepth(maxDepth);
            depthMap.get(i).setMinDepth(minDepth);
        }
        Collections.sort(depthMap);
        return new DepthMap(depthMap);
    }

    public static DepthMap createDepthMapXChangeOnly(ArrayList<Point.Double[]> corners, double moveDistance, double focalLength, double pic1CenterX, double pic2CenterX) {
        ArrayList<DepthObject> depthMap = new ArrayList<DepthObject>();
        double maxDepth = Integer.MIN_VALUE;
        double minDepth = Integer.MAX_VALUE;
        for (int i = 0; i < corners.size(); i++) {
            Point.Double p1A = corners.get(i)[0];
            Point.Double p2A = corners.get(i)[1];

            double d = moveDistance;
            double f = focalLength;
            double depth = 0;
            boolean useNew = false;
            if (useNew) {
                double x0 = p1A.getY() - pic1CenterX;
                double x1 = p2A.getY() - pic2CenterX;
                double y0 = p1A.getX();
                double y1 = p2A.getX();

                double newX = (-d / 2) * (((1 / x1) + (1 / x0)) / ((1 / x0) - (1 / x1)));
                double newZ1 = ((f / x0) * (newX - (x0 - (d / 2)))) + f;
                double newZ2 = ((f / x1) * (newX - (x1 + (d / 2)))) + f;

                depth = (newZ1 + newZ2) / 2;
                if (depth > maxDepth) {
                    maxDepth = depth;
                }
                if (depth < minDepth) {
                    minDepth = depth;
                }
                double newY = (y0 * depth) / f;
                //System.out.println("x0="+p1A.getX()+" x="+newX+"y0="+p1A.getY()+" y="+newY+" z1="+newZ1);
                DepthPoint temp = new DepthPoint(new Point((int) p1A.getY(), (int) p1A.getX()), depth);
                temp.setColorPoint(new Point((int) p1A.getY(), (int) p1A.getX()));
                depthMap.add(temp);
            } else {

                double deltaXL = p1A.getY()- pic1CenterX;
                double deltaXR = p2A.getY() - pic2CenterX;
                depth = (d * f) / (deltaXL - deltaXR);
                if ((deltaXL - deltaXR) != 0) {

                    //System.out.println(p1A+"-->"+p2A+":"+depth);
            /*
                     for(int j=i+1; j<corners.size(); j++)
                     {
                     Point p1B=corners.get(j)[0];
                     Point p2B=corners.get(j)[1];
                
                     double length1=p1A.distance(p1B);
                     double length2=p2A.distance(p2B);
                
                     double change=Math.abs(length2-length1);
                     DepthLine line=new DepthLine(p2A,p2B,change);
                     depthMap.add(line);
                     if(change>maxChange)
                     {
                     maxChange=change;
                     }
                     }*/

                    if (depth > maxDepth) {
                        maxDepth = depth;
                    }
                    if (depth < minDepth) {
                        minDepth = depth;
                    }

                    DepthPoint temp = new DepthPoint(new Point((int) p1A.getY(), (int) p1A.getX()), depth);
                    temp.setColorPoint(new Point((int) p1A.getY(), (int) p1A.getX()));
                    depthMap.add(temp);
                }
            }

        }
        for (int i = 0; i < depthMap.size(); i++) {
            depthMap.get(i).setMaxDepth(maxDepth);
            depthMap.get(i).setMinDepth(minDepth);
        }
        Collections.sort(depthMap);
        return new DepthMap(depthMap);
    }

    public static DepthMap createDepthMap(ArrayList<Point.Double[]> corners, double moveDistance, double focalLength, double pic1CenterX, double pic1CenterY, double pic2CenterX, double pic2CenterY) {
        ArrayList<DepthObject> depthMap = new ArrayList<DepthObject>();
        double maxDepth = Integer.MIN_VALUE;
        double minDepth = Integer.MAX_VALUE;
        for (int i = 0; i < corners.size(); i++) {
            Point.Double p1A = corners.get(i)[0];
            Point.Double p2A = corners.get(i)[1];

            double d = moveDistance;
            double f = focalLength;
            double depth = 0;
            boolean useNew = true;
            if (useNew) {
                double x0 = p1A.getY();
                double x1 = p2A.getY();
                double dx0 = x0 - pic1CenterX;
                double dx1 = x1 - pic2CenterX;
                double y0 = p1A.getX();
                double y1 = p2A.getX();
                double dy0 = y0 - pic1CenterY;
                double dy1 = y1 - pic2CenterY;

                double newX = (-d / 2) * (((1 / dx1) + (1 / dx0)) / ((1 / dx0) - (1 / dx1)));
                double newZ1 = ((f / dx0) * (newX - (dx0 - (d / 2)))) + f;
                double newZ2 = ((f / dx1) * (newX - (dx1 + (d / 2)))) + f;

                depth = (d * f) / (dx0 - dx1);//(newZ1 + newZ2) / 2;
                if (depth > maxDepth) {
                    maxDepth = depth;
                }
                if (depth < minDepth) {
                    minDepth = depth;
                }
                //newX=(dx0*depth)/f;
                //double newY = (dy0 * depth) / f;
                //System.out.println("x0="+p1A.getX()+" x="+newX+"y0="+p1A.getY()+" y="+newY+" z1="+newZ1);
                DepthPoint temp = new DepthPoint(new Point((int) x0,(int) y0), depth);
                temp.setColorPoint(new Point((int) x0,(int) y0));
                depthMap.add(temp);
            } else {

                double x0 = p1A.getY();
                double x1 = p2A.getY();
                double dx0 = x0 - pic1CenterX;
                double dx1 = x1 - pic2CenterX;
                double y0 = p1A.getX();
                double y1 = p2A.getX();
                double dy0 = y0 - pic1CenterY;
                double dy1 = y1 - pic2CenterY;
                double deltaXL = p1A.getY()- pic1CenterX;
                double deltaXR = p2A.getY() - pic2CenterX;
                depth = (d * f) / (deltaXL - deltaXR);
                if ((deltaXL - deltaXR) != 0) {

                    //System.out.println(p1A+"-->"+p2A+":"+depth);
            /*
                     for(int j=i+1; j<corners.size(); j++)
                     {
                     Point p1B=corners.get(j)[0];
                     Point p2B=corners.get(j)[1];
                
                     double length1=p1A.distance(p1B);
                     double length2=p2A.distance(p2B);
                
                     double change=Math.abs(length2-length1);
                     DepthLine line=new DepthLine(p2A,p2B,change);
                     depthMap.add(line);
                     if(change>maxChange)
                     {
                     maxChange=change;
                     }
                     }*/

                    if (depth > maxDepth) {
                        maxDepth = depth;
                    }
                    if (depth < minDepth) {
                        minDepth = depth;
                    }

                    double newX=(dx0*depth)/f;
                    double newY = (dy0 * depth) / f;
                    DepthPoint temp = new DepthPoint(new Point((int) newX, (int) newY), depth);
                    temp.setColorPoint(new Point((int) p1A.getY(), (int) p1A.getX()));
                    depthMap.add(temp);
                }
            }

        }
        for (int i = 0; i < depthMap.size(); i++) {
            depthMap.get(i).setMaxDepth(maxDepth);
            depthMap.get(i).setMinDepth(minDepth);
        }
        Collections.sort(depthMap);
        return new DepthMap(depthMap);
    }
//    public static DepthMap createDepthMap(ArrayList<Point[]> corners) {
//        ArrayList<DepthObject> depthMap = new ArrayList<DepthObject>();
//        double maxChange = 0;
//        for (int i = 0; i < corners.size(); i++) {
//            Point p1A = corners.get(i)[0];
//            Point p2A = corners.get(i)[1];
//            Point3d camera1Pos = new Point3d(0, 0, 0);
//            Point3d camera2Pos = new Point3d(1, 0, 0);
//            double b = camera1Pos.distance(camera2Pos);
//            double f = 5;
//            double deltaXL = p1A.getX();
//            double deltaXR = p2A.getX();
//            double depth = (b * f) / (deltaXL - deltaXR);
//
//            /*
//             for(int j=i+1; j<corners.size(); j++)
//             {
//             Point p1B=corners.get(j)[0];
//             Point p2B=corners.get(j)[1];
//                
//             double length1=p1A.distance(p1B);
//             double length2=p2A.distance(p2B);
//                
//             double change=Math.abs(length2-length1);
//             DepthLine line=new DepthLine(p2A,p2B,change);
//             depthMap.add(line);
//             if(change>maxChange)
//             {
//             maxChange=change;
//             }
//             }*/
//            double dist = p1A.distance(p2A);
//            if (dist > maxChange) {
//                maxChange = dist;
//            }
//            depthMap.add(new DepthPoint(new Point((int) p1A.getY(), (int) p1A.getX()), dist));
//
//        }
//        for (int i = 0; i < depthMap.size(); i++) {
//            depthMap.get(i).setMaxDepth(maxChange);
//        }
//        Collections.sort(depthMap);
//        return new DepthMap(depthMap);
//    }

    public void paint(Graphics g, double scale) {
        for (int i = 0; i < getDepthMap().size(); i++) {
            getDepthMap().get(i).paint(g, scale);
        }
        //System.out.println("finished painting");
    }

    /**
     * @return the depthMap
     */
    public ArrayList<DepthObject> getDepthMap() {
        return depthMap;
    }

    /**
     * @param depthMap the depthMap to set
     */
    public void setDepthMap(ArrayList<DepthObject> depthMap) {
        this.depthMap = depthMap;
    }
}
