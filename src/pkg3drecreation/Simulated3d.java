/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pkg3drecreation;
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author kingm14
 */
import com.sun.j3d.utils.geometry.*;
import com.sun.j3d.utils.image.TextureLoader;

import com.sun.j3d.utils.universe.*;
import java.applet.Applet;
import java.awt.*;
import java.awt.event.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;

import javax.media.j3d.*;
import javax.swing.SwingWorker;

import javax.vecmath.*;

public class Simulated3d implements MouseMotionListener, MouseListener, MouseWheelListener, KeyListener, Runnable {

    SimpleUniverse universe;
    BranchGroup group;

    public Point3f convertPoint(int x, int y, int z) {
        float max = 300;
        return new Point3f(convert(x, max), -1 * convert(y, max), -1 * convert(z, max));
    }

    public Point3f convertPointNew(DepthPoint p) {
        float maxX = 600;
        float maxY = 600;
        float maxZ = 100;
        float x = (float) p.getP().getX();
        float y = (float) p.getP().getY();
        float z = (float) p.getDepth();
        Point3f point = new Point3f(((x / maxX) * 2) - 1, -1 * (((y / maxY) * 2) - 1), -1 * ((z / maxZ) * 2) - 1);
        return point;
        //return convertPoint((int) p.getP().getX(), (int) p.getP().getY(), (int) ((p.getDepth() - p.getMinDepth() + 1) * 10));
    }

    public Point3f convertPoint(DepthPoint p) {
        float max = 300;
        return convertPoint((int) p.getP().getX(), (int) p.getP().getY(), (int) ((p.getDepth() - p.getMinDepth() + 1) * 10));
    }

    public void addSphere(float x, float y, float z, Color c, BranchGroup group) {
        Sphere sphere = new Sphere((float) (1.0 / 100.0));

        Appearance ap = new Appearance();
        Color3f col = new Color3f(c);
        ColoringAttributes ca = new ColoringAttributes(col, ColoringAttributes.NICEST);
        ap.setColoringAttributes(ca);
        sphere.setAppearance(ap);
        TransformGroup tg = new TransformGroup();

        Transform3D transform = new Transform3D();

        Vector3f vector = new Vector3f((x * 2) - 1, -1 * ((y * 2) - 1), -1 * (z * 2) - 1);

        transform.setTranslation(vector);

        tg.setTransform(transform);

        tg.addChild(sphere);

        group.addChild(tg);
//        float max = 300;
//        System.out.println("added sphere at ("+x+","+y+","+z+") -->("+convert(x, max)+","+-1*convert(y, max)+","+convert2(z, max)+")");
//        
//        Sphere sphere = new Sphere((float)(1/10));
//        Appearance ap = new Appearance();
//        Color3f col = new Color3f(c);
//        ColoringAttributes ca = new ColoringAttributes(col, ColoringAttributes.NICEST);
//        ap.setColoringAttributes(ca);
//        sphere.setAppearance(ap);
//        
//        TransformGroup tg = new TransformGroup();
//
//        Transform3D transform = new Transform3D();
//
//        //Vector3f vector = new Vector3f(convert(x, max), -1*convert(y, max), -1*convert2(z, max));
//        Vector3f vector = new Vector3f(x, -1*y, -1*z);
//
//        transform.setTranslation(vector);
//
//        tg.setTransform(transform);
//
//        tg.addChild(sphere);
//
//        group.addChild(tg);
    }

    public void addTriangle(DepthTriangle tri, BranchGroup group, BufferedImage img) {

        boolean useTexture = true;
        //System.out.println();
        if (useTexture) {
            //try {
            Appearance polygon1Appearance = new Appearance();

            //BufferedImage img = img;
                /*try {
                    brick = ImageIO.read(new File("brick.jpg"));
                } catch (IOException e) {
                }*/
            ArrayList<DepthPoint> vertices = tri.getVertices();
            TriangleArray polygon1 = new TriangleArray(3, TriangleArray.COORDINATES | GeometryArray.TEXTURE_COORDINATE_2);
            polygon1.setCoordinate(0, convertPointNew(vertices.get(0)));
            polygon1.setCoordinate(1, convertPointNew(vertices.get(1)));
            polygon1.setCoordinate(2, convertPointNew(vertices.get(2)));
            //polygon1.setCoordinate (3, new Point3f (0f, 3f, 0f));

            Point cp1 = vertices.get(0).getColorPoint();
            Point cp2 = vertices.get(1).getColorPoint();
            Point cp3 = vertices.get(2).getColorPoint();
            //Color c3 = new Color(img.getRGB((int) cp3.getX(), (int) cp3.getY()));
            double width=img.getWidth();
            double height=img.getHeight();
            polygon1.setTextureCoordinate(0, new Point2f((float)(cp1.getX()/width), (float)(cp1.getY()/height)));
            polygon1.setTextureCoordinate(1, new Point2f((float)(cp2.getX()/width), (float)(cp2.getY()/height)));
            polygon1.setTextureCoordinate(2, new Point2f((float)(cp3.getX()/width), (float)(cp3.getY()/height)));
            //polygon1.setTextureCoordinate (3, new Point2f(0.0f,1.0f));

             
            Texture texImage = new TextureLoader(img).getTexture();
            
            polygon1Appearance.setTexture(texImage);
            PolygonAttributes pgonAttrs = new PolygonAttributes();
            pgonAttrs.setCullFace(pgonAttrs.CULL_NONE);
            polygon1Appearance.setPolygonAttributes(pgonAttrs);
            group.addChild(new Shape3D(polygon1, polygon1Appearance));
            
//            try {
//                //System.out.print("Started");
//                ArrayList<DepthPoint> vertices = tri.getVertices();
//                Shape3D shape = new Shape3D();
//                TriangleArray triangle = new TriangleArray(3, TriangleArray.COORDINATES | GeometryArray.TEXTURE_COORDINATE_2);
//                //TriangleArray triangle = new TriangleArray(3, TriangleArray.COORDINATES | TriangleArray.COLOR_3);
////        triangle.setCoordinate(0, new Point3f(0.5f, 0.0f, 0.0f));
////        triangle.setCoordinate(1, new Point3f(0.0f, 0.5f, 0.0f));
////        triangle.setCoordinate(2, new Point3f(-0.5f, 0.0f, 0.0f));
//                triangle.setCoordinate(0, convertPointNew(vertices.get(0)));
//                triangle.setCoordinate(1, convertPointNew(vertices.get(1)));
//                triangle.setCoordinate(2, convertPointNew(vertices.get(2)));
//                shape.setGeometry(triangle);
//                Point cp1 = vertices.get(0).getColorPoint();
//                //Color c1 = new Color(img.getRGB((int) cp1.getX(), (int) cp1.getY()));
//
//                Point cp2 = vertices.get(1).getColorPoint();
//                //Color c2 = new Color(img.getRGB((int) cp2.getX(), (int) cp2.getY()));
//
//                Point cp3 = vertices.get(2).getColorPoint();
//                //Color c3 = new Color(img.getRGB((int) cp3.getX(), (int) cp3.getY()));
//                //triangle.setColor(0, new Color3f((float) (c1.getRed() / 255.0), (float) (c1.getGreen() / 255.0), (float) (c1.getBlue() / 255.0)));
//                //triangle.setColor(1, new Color3f((float) (c2.getRed() / 255.0), (float) (c2.getGreen() / 255.0), (float) (c2.getBlue() / 255.0)));
//                //triangle.setColor(2, new Color3f((float) (c3.getRed() / 255.0), (float) (c3.getGreen() / 255.0), (float) (c3.getBlue() / 255.0)));
//                triangle.setTextureCoordinate(0, new Point2f(/*(float) (cp1.getX() / img.getWidth()), (float) (cp1.getY() / img.getHeight())*/0.0f, 0.0f));
//                triangle.setTextureCoordinate(1, new Point2f(/*(float) (cp2.getX() / img.getWidth()), (float) (cp2.getY() / img.getHeight())*/1.0f, 0.0f));
//                triangle.setTextureCoordinate(2, new Point2f(/*(float) (cp3.getX() / img.getWidth()), (float) (cp3.getY() / img.getHeight())*/0.0f, 1.0f));
//                //BufferedImage pow2Img=resizeImagePow2(img);
//                //Texture texImage = new TextureLoader(pow2Img).getTexture();
//                ImageComponent2D image = new ImageComponent2D(ImageComponent2D.FORMAT_RGB, img);
//                Texture2D texture = new Texture2D();
//                texture.setImage(0, image);
//
//            //TextureLoader loader=new TextureLoader(img);
//                //Texture tex=loader.getTexture();
//                Appearance ap = new Appearance();
//                ap.setTexture(texture);
//                PolygonAttributes pgonAttrs = new PolygonAttributes();
//                pgonAttrs.setCullFace(pgonAttrs.CULL_NONE);
//                ap.setPolygonAttributes(pgonAttrs);
//                shape.setAppearance(ap);
//                group.addChild(shape);
//                //System.out.println("   Finished");
//            } catch (Exception e) {
//                System.err.println(e.getMessage());
//                e.printStackTrace();
//            };
        } else {
            ArrayList<DepthPoint> vertices = tri.getVertices();
            Shape3D shape = new Shape3D();
            //TriangleArray triangle = new TriangleArray(3, TriangleArray.COORDINATES | TriangleArray.TEXTURE_COORDINATE_3);
            TriangleArray triangle = new TriangleArray(3, TriangleArray.COORDINATES | TriangleArray.COLOR_3);
//        triangle.setCoordinate(0, new Point3f(0.5f, 0.0f, 0.0f));
//        triangle.setCoordinate(1, new Point3f(0.0f, 0.5f, 0.0f));
//        triangle.setCoordinate(2, new Point3f(-0.5f, 0.0f, 0.0f));
            triangle.setCoordinate(0, convertPointNew(vertices.get(0)));
            triangle.setCoordinate(1, convertPointNew(vertices.get(1)));
            triangle.setCoordinate(2, convertPointNew(vertices.get(2)));
            Point cp1 = vertices.get(0).getColorPoint();
            Color c1 = new Color(img.getRGB((int) cp1.getX(), (int) cp1.getY()));

            Point cp2 = vertices.get(1).getColorPoint();
            Color c2 = new Color(img.getRGB((int) cp2.getX(), (int) cp2.getY()));

            Point cp3 = vertices.get(2).getColorPoint();
            Color c3 = new Color(img.getRGB((int) cp3.getX(), (int) cp3.getY()));
            triangle.setColor(0, new Color3f((float) (c1.getRed() / 255.0), (float) (c1.getGreen() / 255.0), (float) (c1.getBlue() / 255.0)));
            triangle.setColor(1, new Color3f((float) (c2.getRed() / 255.0), (float) (c2.getGreen() / 255.0), (float) (c2.getBlue() / 255.0)));
            triangle.setColor(2, new Color3f((float) (c3.getRed() / 255.0), (float) (c3.getGreen() / 255.0), (float) (c3.getBlue() / 255.0)));
            //triangle.setTextureCoordinate(0, new Point2f((float)cp1.getX(),(float)cp1.getY()));
            //triangle.setTextureCoordinate(1, new Point2f((float)cp2.getX(),(float)cp2.getY()));
            //triangle.setTextureCoordinate(2, new Point2f((float)cp3.getX(),(float)cp3.getY()));
            //TextureLoader loader=new TextureLoader(img);
            //Texture tex=loader.getTexture();
            shape.setGeometry(triangle);
            Appearance ap = new Appearance();
            PolygonAttributes pgonAttrs = new PolygonAttributes();
            pgonAttrs.setCullFace(pgonAttrs.CULL_NONE);
            ap.setPolygonAttributes(pgonAttrs);
            shape.setAppearance(ap);
            group.addChild(shape);

        }
        //BranchGroup objRoot = new BranchGroup();
        // Create a triangle with each point a different color.  Remember to
        // draw the points in counter-clockwise order.  That is the default
        // way of determining which is the front of a polygon.
        //        o (1)
        //       / \
        //      /   \
        // (2) o-----o (0)

        /*//System.out.println("add triangle");
        
        
         //QuadArray polygon1 = new QuadArray (4,QuadArray.COORDINATES | GeometryArray.TEXTURE_COORDINATE_2);
    	
	
        
         ArrayList<DepthPoint> vertices = tri.getVertices();

         TriangleArray triangle = new TriangleArray(3, TriangleArray.COORDINATES);
         triangle.setCoordinate(0, convertPointNew(vertices.get(0)));
         triangle.setCoordinate(1, convertPointNew(vertices.get(1)));
         triangle.setCoordinate(2, convertPointNew(vertices.get(2)));
       
         Point cp1=vertices.get(0).getColorPoint();
         Point cp2=vertices.get(1).getColorPoint();
         Point cp3=vertices.get(2).getColorPoint();
        
         triangle.setTextureCoordinate (0, new Point2f((int) cp1.getX(), (int) cp1.getY()));
         triangle.setTextureCoordinate (1, new Point2f((int) cp2.getX(), (int) cp2.getY()));
         triangle.setTextureCoordinate (2, new Point2f((int) cp3.getX(), (int) cp3.getY()));
        
         Texture texImage = new TextureLoader(img).getTexture();
         //        Point cp1=vertices.get(0).getColorPoint();
         //        Color c1 = new Color(img.getRGB((int) cp1.getX(), (int) cp1.getY()));
         //        
         //        Point cp2=vertices.get(1).getColorPoint();
         //        Color c2 = new Color(img.getRGB((int) cp2.getX(), (int) cp2.getY()));
         //        
         //        Point cp3=vertices.get(2).getColorPoint();
         //        Color c3 = new Color(img.getRGB((int) cp3.getX(), (int) cp3.getY()));
         //        
         //        triangle.setColor(0, new Color3b(c1));
         //        triangle.setColor(1, new Color3b(c2));
         //        triangle.setColor(2, new Color3b(c3));
        
         Appearance ap = new Appearance();
        
         //ap.setLineAttributes(LineAttributes.);
         Color3f col = new Color3f(Color.white);
         ColoringAttributes ca = new ColoringAttributes(col, ColoringAttributes.NICEST );
         ap.setColoringAttributes(ca);
         PolygonAttributes pgonAttrs = new PolygonAttributes();
         pgonAttrs.setCullFace(pgonAttrs.CULL_NONE);
        
         ap.setPolygonAttributes(pgonAttrs);

         ap.setTexture(texImage);
         Shape3D myShape = new Shape3D(triangle, ap);
         group.addChild(myShape);
         //
         //        
         //
         //        TriangleArray triangle2 = new TriangleArray(3, TriangleArray.COORDINATES);
         //        triangle2.setCoordinate(0, convertPointNew(vertices.get(2)));
         //        triangle2.setCoordinate(1, convertPointNew(vertices.get(1)));
         //        triangle2.setCoordinate(2, convertPointNew(vertices.get(0)));
         //        triangle2.setColor(0, new Color3b(c3));
         //        triangle2.setColor(1, new Color3b(c2));
         //        triangle2.setColor(2, new Color3b(c1));

        
         //Appearance ap = new Appearance();
         //Color3f col = new Color3f(Color.gray);
         //ColoringAttributes ca = new ColoringAttributes(col, ColoringAttributes.NICEST);
         //ap.setColoringAttributes(ca);

         //        Shape3D myShape2 = new Shape3D(triangle2, ap);
         //        group.addChild(myShape2);*/
    }

    public static int nextPowOf2(int i) {
        return (int) Math.pow(2, Math.ceil(Math.log(i) / Math.log(2)));
    }

    public static BufferedImage resizeImagePow2(BufferedImage image) {
        int curWidth = image.getWidth();
        int curHeight = image.getHeight();
        int newWidth = nextPowOf2(curWidth);
        int newHeight = nextPowOf2(curHeight);
        final BufferedImage bufferedImage = new BufferedImage(newWidth, newHeight, BufferedImage.TYPE_INT_RGB);
        final Graphics2D graphics2D = bufferedImage.createGraphics();
        graphics2D.setComposite(AlphaComposite.Src);
        graphics2D.drawImage(image, 0, 0, newWidth, newHeight, null);
        graphics2D.dispose();
        return bufferedImage;
    }

    public void addTriangles(ArrayList<DepthTriangle> triangles, BranchGroup group, BufferedImage img) {
        //System.out.println(triangles.size());
        if (triangles.size() == 0) {
            return;
        }
        //TriangleArray triangle = new TriangleArray(3 * triangles.size(), TriangleArray.COORDINATES);
        Appearance polygon1Appearance = new Appearance();
        TriangleArray polygon1 = new TriangleArray(3 * triangles.size(), TriangleArray.COORDINATES | GeometryArray.TEXTURE_COORDINATE_2);
//            BufferedImage brick=null;
//                try {
//                    brick = ImageIO.read(new File("brick.jpg"));
//                } catch (IOException e) {
//                }
//            BufferedImage img=brick;
            
            //Color c3 = new Color(img.getRGB((int) cp3.getX(), (int) cp3.getY()));
            double width=img.getWidth();
            double height=img.getHeight();
            
            //polygon1.setTextureCoordinate (3, new Point2f(0.0f,1.0f));

             
           ProgressBarFrame pBarFrame=new ProgressBarFrame();
           
           pBarFrame.setUpdateBarVal(1);
           pBarFrame.setStatus("creating displayable triangles");
           pBarFrame.setProgressBar1Max( triangles.size());
           
        for (int i = 0; i < triangles.size(); i++) {
            pBarFrame.setProgressBar1Val(i);
            int startNum = 3 * i;
            DepthTriangle tri = triangles.get(i);
            
            ArrayList<DepthPoint> vertices = tri.getVertices();
            polygon1.setCoordinate(startNum+0, convertPointNew(vertices.get(0)));
            polygon1.setCoordinate(startNum+1, convertPointNew(vertices.get(1)));
            polygon1.setCoordinate(startNum+2, convertPointNew(vertices.get(2)));
            //polygon1.setCoordinate (3, new Point3f (0f, 3f, 0f));

            Point cp1 = vertices.get(0).getColorPoint();
            Point cp2 = vertices.get(1).getColorPoint();
            Point cp3 = vertices.get(2).getColorPoint();
            
            
            polygon1.setTextureCoordinate(startNum+0, new Point2f((float)(cp1.getX()/width), (float)(1-(cp1.getY()/height))));
            polygon1.setTextureCoordinate(startNum+1, new Point2f((float)(cp2.getX()/width), (float)(1-(cp2.getY()/height))));
            polygon1.setTextureCoordinate(startNum+2, new Point2f((float)(cp3.getX()/width), (float)(1-(cp3.getY()/height))));
            
        }

         Texture texImage = new TextureLoader(img).getTexture();
            
            polygon1Appearance.setTexture(texImage);
            PolygonAttributes pgonAttrs = new PolygonAttributes();
            pgonAttrs.setCullFace(pgonAttrs.CULL_NONE);
            polygon1Appearance.setPolygonAttributes(pgonAttrs);
            group.addChild(new Shape3D(polygon1, polygon1Appearance));
    }
    // points must be in counterclockwise order

    public void addRectangle(Point3f p4, Point3f p3, Point3f p2, Point3f p1, Color c, BranchGroup group) {
        QuadArray polygon1 = new QuadArray(4, QuadArray.COORDINATES);
        polygon1.setCoordinate(0, p1);
        polygon1.setCoordinate(1, p2);
        polygon1.setCoordinate(2, p3);
        polygon1.setCoordinate(3, p4);
        Shape3D poly = new Shape3D(polygon1);
        //set color
        Appearance ap = new Appearance();
        Color3f col = new Color3f(c);
        ColoringAttributes ca = new ColoringAttributes(col, ColoringAttributes.NICEST);
        ap.setColoringAttributes(ca);
        poly.setAppearance(ap);
        group.addChild(poly);

    }

    public void addRectPrism(int x, int y, int z, Color c, BranchGroup group) {
        float max = 300;
        float minX, minY, maxX, maxY;
        float xf = convert(x, max);
        float yf = -1 * convert(y, max);
        float zf = -1 * convert(z, max);
        float rad = (float) (2 / max);
        minX = xf - rad;
        minY = yf - rad;
        maxX = xf + rad;
        maxY = yf + rad;
        //top left far
        Point3f tlf = new Point3f(minX, minY, -1.0f);
        //top Left Close
        Point3f tlc = new Point3f(minX, minY, zf);
        //top right far
        Point3f trf = new Point3f(maxX, minY, -1.0f);
        //top right Close
        Point3f trc = new Point3f(maxX, minY, zf);
        //bottom left far
        Point3f blf = new Point3f(minX, maxY, -1.0f);
        //bottom Left Close
        Point3f blc = new Point3f(minX, maxY, zf);
        //bottom right far
        Point3f brf = new Point3f(maxX, maxY, -1.0f);
        //bottom right Close
        Point3f brc = new Point3f(maxX, maxY, zf);
        addRectangle(tlc, tlf, blf, blc, c, group);
        addRectangle(trc, trf, tlf, tlc, c, group);
        addRectangle(trf, trc, brc, brf, c, group);
        addRectangle(brc, blc, blf, brf, c, group);
        addRectangle(trc, tlc, blc, brc, c, group);
    }

    public void addSphere(int x, int y, int z, BranchGroup group) {

        float max = 300;
        Sphere sphere = new Sphere((float) (2 / max));

        TransformGroup tg = new TransformGroup();

        Transform3D transform = new Transform3D();

        Vector3f vector = new Vector3f(convert(x, max), -1 * convert(y, max), -1 * convert(z, max));

        transform.setTranslation(vector);

        tg.setTransform(transform);

        tg.addChild(sphere);

        group.addChild(tg);
    }

    public void addLine(int x1, int y1, int z1, int x2, int y2, int z2, BranchGroup group) {
        LineArray lineArr = new LineArray(2, LineArray.COORDINATES);
        lineArr.setCoordinate(0, convertPoint(x1, y1, z1));
        lineArr.setCoordinate(1, convertPoint(x2, y2, z2));
        Shape3D plShape = new Shape3D(lineArr);
        group.addChild(plShape);

    }

    public void addLine(DepthPoint p1, DepthPoint p2, BranchGroup group) {
        LineArray lineArr = new LineArray(2, LineArray.COORDINATES);
        lineArr.setCoordinate(0, convertPoint(p1));
        lineArr.setCoordinate(1, convertPoint(p2));
        Shape3D plShape = new Shape3D(lineArr);
        group.addChild(plShape);

    }

    public void addLines(ArrayList<DepthLine> lines, BranchGroup group) {
        //System.out.println(lines.size());
        if (lines.size() == 0) {
            return;
        }
        LineArray lineArr = new LineArray(2 * lines.size(), LineArray.COORDINATES);
        for (int i = 0; i < lines.size(); i++) {
            int startNum = 2 * i;
            DepthLine line = lines.get(i);
            DepthPoint p1 = line.getStart();
            DepthPoint p2 = line.getEnd();
            lineArr.setCoordinate(startNum + 0, convertPoint(p1));
            lineArr.setCoordinate(startNum + 1, convertPoint(p2));

        }
        Material myMat = new Material();
        myMat.setAmbientColor(0.3f, 0.3f, 0.3f);
        myMat.setDiffuseColor(1.0f, 0.0f, 0.0f);
        myMat.setEmissiveColor(0.0f, 0.0f, 0.0f);
        myMat.setSpecularColor(1.0f, 1.0f, 1.0f);
        myMat.setShininess(64.0f);

        Appearance myAppear = new Appearance();
        myAppear.setMaterial(myMat);

        Shape3D myShape = new Shape3D(lineArr, myAppear);
        group.addChild(myShape);
    }

    public void addSphere(float x, float y, float z, BranchGroup group) {
        float max = 10;
        Sphere sphere = new Sphere((float) (1 / max));

        TransformGroup tg = new TransformGroup();

        Transform3D transform = new Transform3D();

        Vector3f vector = new Vector3f(x, y, z);

        transform.setTranslation(vector);

        tg.setTransform(transform);

        tg.addChild(sphere);

        group.addChild(tg);
    }

    public void addSphere(int x, int y, int z, BranchGroup group, float max) {

        //float max = 300;
        Sphere sphere = new Sphere((float) (2 / max));

        TransformGroup tg = new TransformGroup();

        Transform3D transform = new Transform3D();

        Vector3f vector = new Vector3f(convert(x, max), -1 * convert(y, max), convert(z, max));

        transform.setTranslation(vector);

        tg.setTransform(transform);

        tg.addChild(sphere);

        group.addChild(tg);
    }

    public void addSphere(DepthPoint dp, BranchGroup group, Color c) {

        Point p = dp.getP();
        //addRectPrism((int)p.getX(),(int)p.getY(), (int)((dp.getDepth()-dp.getMinDepth()+1)*10),Color.blue,group);
        //System.out.println("max:"+dp.getMaxDepth()+" min:"+dp.getMinDepth()+ " me:"+dp.getDepth());
        //System.out.println("added "+(int)p.getX()+","+(int)p.getY()+","+ (int)((dp.getDepth())));
        addSphere((int) p.getX(), (int) p.getY(), (int) ((dp.getDepth())), c, group);
    }

    public void addSphere(DepthPoint dp, BranchGroup group, Color c, int maxX, int maxY, int maxZ) {

        Point p = dp.getP();
        //addRectPrism((int)p.getX(),(int)p.getY(), (int)((dp.getDepth()-dp.getMinDepth()+1)*10),Color.blue,group);
        //System.out.println("max:"+dp.getMaxDepth()+" min:"+dp.getMinDepth()+ " me:"+dp.getDepth());
        /*System.out.println("added "+(int)p.getX()+","+(int)p.getY()+","+ (int)((dp.getDepth())));
         Sphere sphere = new Sphere(0.05f);

         TransformGroup tg = new TransformGroup();

         Transform3D transform = new Transform3D();
         
         Vector3f vector = new Vector3f((float)(p.getX()/maxX),(float)(p.getY()/maxY), (float)(dp.getDepth()/maxZ));

         transform.setTranslation(vector);

         tg.setTransform(transform);

         tg.addChild(sphere);

         group.addChild(tg);*/
        addSphere((float) (p.getX() / maxX), (float) (p.getY() / maxY), (float) (dp.getDepth() / maxZ), c, group);
    }

    public void addSphere(DepthPoint dp, BranchGroup group) {

        Point p = dp.getP();
        //addRectPrism((int)p.getX(),(int)p.getY(), (int)((dp.getDepth()-dp.getMinDepth()+1)*10),Color.blue,group);
        //System.out.println("max:"+dp.getMaxDepth()+" min:"+dp.getMinDepth()+ " me:"+dp.getDepth());
        addSphere((int) p.getX(), (int) p.getY(), (int) ((dp.getDepth() - dp.getMinDepth() + 1) * 10), group);
    }

    public void addText(String text, Point3f pos, BranchGroup group) {
        Font my2DFont = new Font(
                "Arial", // font name
                Font.PLAIN, // font style
                1); // font size
        FontExtrusion myExtrude = new FontExtrusion();
        Font3D my3DFont = new Font3D(my2DFont, myExtrude);

        Text3D myText = new Text3D();
        myText.setFont3D(my3DFont);
        myText.setString(text);
        myText.setPosition(pos);
        ColoringAttributes myCA = new ColoringAttributes();
        myCA.setColor(1.0f, 1.0f, 0.0f);
        Appearance myAppear = new Appearance();
        myAppear.setColoringAttributes(myCA);
        Shape3D myShape = new Shape3D(myText, myAppear);

        group.addChild(myShape);
    }

    public float convert(double val, double max) {
        return (float) ((val / max) - (.5));
    }

    public float convert2(double val, double max) {
        float f = (float) ((-1 * (val / max)));

        return f;
    }

    public void createRandomSpheres(int numSpheres, BranchGroup group) {
        Random r = new Random();
        for (int i = 0; i < numSpheres; i++) {
            float x = r.nextFloat() - .5f;
            float y = r.nextFloat() - .5f;
            float z = r.nextFloat() - .5f;
            addSphere(x, y, z, group);
        }
    }

    public void createSpheresOnAxis(int numSpheres, BranchGroup group) {
        //Random r = new Random();
        for (int i = 0; i < numSpheres; i++) {
            int x = (300 / numSpheres) * i;

            addSphere(x, 0, 0, Color.BLUE, group);
            addSphere(0, x, 0, Color.BLUE, group);
            addSphere(0, 0, x, Color.BLUE, group);
        }
    }
    private final double screenWidth = java.awt.Toolkit.getDefaultToolkit().getScreenSize().getWidth();
    private final double screenHeight = java.awt.Toolkit.getDefaultToolkit().getScreenSize().getHeight();

    public Simulated3d() {

        universe = new SimpleUniverse();
        universe.getCanvas().setSize((int) screenWidth, (int) screenHeight);

        universe.getCanvas().addMouseMotionListener(this);
        universe.getCanvas().addMouseListener(this);
        universe.getCanvas().addMouseWheelListener(this);
        universe.getCanvas().addKeyListener(this);

        group = new BranchGroup();

        Appearance polygon1Appearance = new Appearance();

        TriangleArray polygon1 = new TriangleArray(3, TriangleArray.COORDINATES | GeometryArray.TEXTURE_COORDINATE_2);
        polygon1.setCoordinate(0, new Point3f(0f, 0f, 2f));
        polygon1.setCoordinate(1, new Point3f(2f, 0f, 0f));
        polygon1.setCoordinate(2, new Point3f(2f, 3f, 0f));
        //polygon1.setCoordinate (3, new Point3f (0f, 3f, 0f));

        polygon1.setTextureCoordinate(0, new Point2f(0.0f, 0.0f));
        polygon1.setTextureCoordinate(1, new Point2f(1.0f, 0.0f));
        polygon1.setTextureCoordinate(2, new Point2f(1.0f, 1.0f));
            //polygon1.setTextureCoordinate (3, new Point2f(0.0f,1.0f));

        BufferedImage img = null;
        try {
            img = ImageIO.read(new File("brick.jpg"));
        } catch (IOException e) {
        }
        Texture texImage = new TextureLoader(img).getTexture();

        polygon1Appearance.setTexture(texImage);
        PolygonAttributes pgonAttrs = new PolygonAttributes();
        pgonAttrs.setCullFace(pgonAttrs.CULL_NONE);
        polygon1Appearance.setPolygonAttributes(pgonAttrs);

        group.addChild(new Shape3D(polygon1, polygon1Appearance));
        // X axis made of spheres
        double max = 1000;
        for (float x = -1.0f; x <= 1.0f; x = x + 0.1f) {

            Sphere sphere = new Sphere(0.05f);

            TransformGroup tg = new TransformGroup();

            Transform3D transform = new Transform3D();

            Vector3f vector = new Vector3f(x, .0f, .0f);

            transform.setTranslation(vector);

            tg.setTransform(transform);

            tg.addChild(sphere);

            group.addChild(tg);

        }

        // Y axis made of cones
        for (float y = -1.0f; y <= 1.0f; y = y + 0.1f) {

            TransformGroup tg = new TransformGroup();

            Transform3D transform = new Transform3D();

            Cone cone = new Cone(0.05f, 0.1f);

            Vector3f vector = new Vector3f(.0f, y, .0f);

            transform.setTranslation(vector);

            tg.setTransform(transform);

            tg.addChild(cone);

            group.addChild(tg);

        }

        // Z axis made of cylinders
        for (float z = -1.0f; z <= 1.0f; z = z + 0.1f) {

            TransformGroup tg = new TransformGroup();

            Transform3D transform = new Transform3D();

            Cylinder cylinder = new Cylinder(0.05f, 0.1f);

            Vector3f vector = new Vector3f(.0f, .0f, z);

            transform.setTranslation(vector);

            tg.setTransform(transform);

            tg.addChild(cylinder);

            group.addChild(tg);

        }
        //createRandomSpheres(50,group);
        createSpheresOnAxis(50, group);
        //addText("Hello",new Point3f(0.0f,0.0f,0.0f), group);
        Color3f light1Color = new Color3f(Color.green); // green light

        BoundingSphere bounds
                = new BoundingSphere(new Point3d(0.0, 0.0, 0.0), 10);

        Vector3f light1Direction = new Vector3f(4.0f, -7.0f, -12.0f);

        DirectionalLight light1 = new DirectionalLight(light1Color, light1Direction);

        light1.setInfluencingBounds(bounds);

        group.addChild(light1);

        //universe.getViewingPlatform().setNominalViewingTransform();
        // add the group of objects to the Universe
        group.compile();
        universe.addBranchGraph(group);
        keyboardThread = new Thread(this);
        keyboardThread.start();
        //setCameraPosition(cameraX, cameraY, cameraZ, centerX, centerY,cameraZ-4);

    }
    Thread keyboardThread;

    public Simulated3d(DepthMap depthMap) {

        ArrayList<DepthTriangle> triangles = new ArrayList<DepthTriangle>();
        ArrayList<DepthLine> lines = new ArrayList<DepthLine>();
        universe = new SimpleUniverse();

        universe.getCanvas().addMouseMotionListener(this);
        universe.getCanvas().addMouseListener(this);
        universe.getCanvas().addMouseWheelListener(this);
        group = new BranchGroup();

        ArrayList<DepthObject> list = depthMap.getDepthMap();
        LineArray lineArr = new LineArray(2, LineArray.COORDINATES);
        for (DepthObject obj : list) {
            if (obj instanceof DepthLine) {
                DepthLine dl = (DepthLine) obj;
                lines.add(dl);
                /*addLine(dl.getStart(), dl.getEnd(), group);
                 DepthPoint dp1=(DepthPoint)dl.getStart();
                 Point p1=dp1.getP();
                 addSphere(dp1,group);
                 DepthPoint dp2=(DepthPoint)dl.getEnd();
                 Point p2=dp2.getP();
                 addSphere(dp2,group);*/
            } else if (obj instanceof DepthTriangle) {
                DepthTriangle dt = (DepthTriangle) obj;
                triangles.add(dt);
                /*ArrayList<DepthPoint> points=dt.getVertices();
                 for(int i=0; i< points.size(); i++)
                 {
                 int next=(i+1)%3;
                 addLine(points.get(i), points.get(next), group);
                 DepthPoint dp=points.get(i);
                 Point p=dp.getP();
                 addSphere(dp,group);
                 }
                 addTriangle(dt,group);*/

            } else if (obj instanceof DepthPoint) {
                DepthPoint dp = (DepthPoint) obj;
                Point p = dp.getP();
                //addRectPrism((int)p.getX(),(int)p.getY(), (int)((dp.getDepth()-dp.getMinDepth()+1)*10),Color.blue,group);
                addSphere(dp, group);
                //(int)p.getX(),(int)p.getY(), (int)((dp.getDepth()-dp.getMinDepth()+1)*10)

                //System.out.println(p+" "+(int)((dp.getDepth()-dp.getMinDepth()+1)*10));
            }

        }

        //addTriangles(triangles,group);
        addLines(lines, group);

//        TriangleArray pyramidGeometry = new TriangleArray(triangles.size()*3,
//				TriangleArray.COORDINATES);
//		
//        for(int i=0; i<triangles.size(); i++)
//        {
//            DepthTriangle t=triangles.get(i);
//            ArrayList<DepthPoint> vertices=t.getVertices();
//            for(int v=0; v<3; v++)
//            {
//                DepthPoint p=vertices.get(v);
//                pyramidGeometry.setCoordinate((i*3)+v, convertPoint(p));
//            }
//        }
//        GeometryInfo geometryInfo = new GeometryInfo(pyramidGeometry);
//        NormalGenerator ng = new NormalGenerator();
//        ng.generateNormals(geometryInfo);
//        GeometryArray result = geometryInfo.getGeometryArray();
//        Appearance appearance = new Appearance();
//		Color3f color = new Color3f(Color.yellow);
//		Color3f black = new Color3f(0.0f, 0.0f, 0.0f);
//		Color3f white = new Color3f(1.0f, 1.0f, 1.0f);
//		Texture texture = new Texture2D();
//		TextureAttributes texAttr = new TextureAttributes();
//		texAttr.setTextureMode(TextureAttributes.MODULATE);
//		texture.setBoundaryModeS(Texture.WRAP);
//		texture.setBoundaryModeT(Texture.WRAP);
//		texture.setBoundaryColor(new Color4f(0.0f, 1.0f, 0.0f, 0.0f));
//		Material mat = new Material(color, black, color, white, 70f);
//		appearance.setTextureAttributes(texAttr);
//		appearance.setMaterial(mat);
//		appearance.setTexture(texture);
//		Shape3D shape = new Shape3D(result, appearance);
//		group.addChild(shape);
        Color3f light1Color = new Color3f(Color.white); // green light

        BoundingSphere bounds
                = new BoundingSphere(new Point3d(0.0, 0.0, 0.0), 10.0);

        Vector3f light1Direction = new Vector3f(4.0f, -7.0f, -12.0f);

        DirectionalLight light1 = new DirectionalLight(light1Color, light1Direction);

        light1.setInfluencingBounds(bounds);

        group.addChild(light1);
        //AmbientLight ambientLightNode = new AmbientLight (light1Color);
        //ambientLightNode.setInfluencingBounds (bounds);
        //group.addChild (ambientLightNode);

        //universe.getViewingPlatform().setNominalViewingTransform();
        // add the group of objects to the Universe
        universe.addBranchGraph(group);
        universe.getViewer().getView().setBackClipDistance(1000);
        setCameraPosition(cameraX, cameraY, cameraZ, centerX, centerY, cameraZ - 4);
        keyboardThread = new Thread(this);
        keyboardThread.start();

    }

    public Simulated3d(DepthMap depthMap, BufferedImage img) {

        ArrayList<DepthTriangle> triangles = new ArrayList<DepthTriangle>();
        ArrayList<DepthLine> lines = new ArrayList<DepthLine>();
        universe = new SimpleUniverse();

        universe.getCanvas().addMouseMotionListener(this);
        universe.getCanvas().addMouseListener(this);
        universe.getCanvas().addMouseWheelListener(this);
        universe.getCanvas().addKeyListener(this);
        group = new BranchGroup();
        group.setCapability(BranchGroup.ALLOW_DETACH);

        universe.getViewer().getView().setBackClipDistance(100000);
        //setCameraPosition(cameraX, cameraY, cameraZ, centerX, centerY,cameraZ-4);
        keyboardThread = new Thread(this);
        keyboardThread.start();
        recreate(depthMap, img);

    }

    public void recreate(DepthMap depthMap, BufferedImage img) {

        ProgressBarFrame pBarFrame=new ProgressBarFrame();
        pBarFrame.setUpdateBarVal(1);
        pBarFrame.setVisible(true);
        group.detach();
        group.removeAllChildren();

        ArrayList<DepthTriangle> triangles = new ArrayList<DepthTriangle>();
        ArrayList<DepthLine> lines = new ArrayList<DepthLine>();

        ArrayList<DepthObject> list = depthMap.getDepthMap();
        LineArray lineArr = new LineArray(2, LineArray.COORDINATES);
        int count=0;
        pBarFrame.setProgressBar1Max(list.size());
        
        for (DepthObject obj : list) {
            pBarFrame.setProgressBar1Val(count);
            count++;
            if (obj instanceof DepthLine) {
                DepthLine dl = (DepthLine) obj;
                lines.add(dl);
                /*addLine(dl.getStart(), dl.getEnd(), group);
                 DepthPoint dp1=(DepthPoint)dl.getStart();
                 Point p1=dp1.getP();
                 addSphere(dp1,group);
                 DepthPoint dp2=(DepthPoint)dl.getEnd();
                 Point p2=dp2.getP();
                 addSphere(dp2,group);*/
            } else if (obj instanceof DepthTriangle) {
                DepthTriangle dt = (DepthTriangle) obj;
                //addTriangle(dt, group, img);
                triangles.add(dt);
//                DepthPoint dp1 = (DepthPoint) dt.getVertices().get(0);
//                Point p1 = dp1.getP();
//                Point cp1=dp1.getColorPoint();
//                //addRectPrism((int)p.getX(),(int)p.getY(), (int)((dp.getDepth()-dp.getMinDepth()+1)*10),Color.blue,group);]
//                try {
//                    Color c = new Color(img.getRGB((int) cp1.getX(), (int) cp1.getY()));
//
//
//                    //Color c= rgb.toColor();
//                    addSphere(dp1, group, c, 600, 600, 100);
//                } catch (Exception e) {
//                    //System.out.println((int)p.getX()+" "+(int)p.getY()+" "+img.getWidth()+" "+img.getHeight());
//                }
//                DepthPoint dp2 = (DepthPoint) dt.getVertices().get(1);
//                Point p2 = dp2.getP();
//                Point cp2=dp2.getColorPoint();
//                //addRectPrism((int)p.getX(),(int)p.getY(), (int)((dp.getDepth()-dp.getMinDepth()+1)*10),Color.blue,group);]
//                try {
//                    Color c = new Color(img.getRGB((int) cp2.getX(), (int) cp2.getY()));
//
//
//                    //Color c= rgb.toColor();
//                    addSphere(dp2, group, c, 600, 600, 100);
//                } catch (Exception e) {
//                    //System.out.println((int)p.getX()+" "+(int)p.getY()+" "+img.getWidth()+" "+img.getHeight());
//                }
//                DepthPoint dp3 = (DepthPoint) dt.getVertices().get(2);
//                Point p3 = dp3.getP();
//                Point cp3=dp3.getColorPoint();
//                //addRectPrism((int)p.getX(),(int)p.getY(), (int)((dp.getDepth()-dp.getMinDepth()+1)*10),Color.blue,group);]
//                try {
//                    Color c = new Color(img.getRGB((int) cp3.getX(), (int) cp3.getY()));
//
//
//                    //Color c= rgb.toColor();
//                    addSphere(dp3, group, c, 600, 600, 100);
//                } catch (Exception e) {
//                    //System.out.println((int)p.getX()+" "+(int)p.getY()+" "+img.getWidth()+" "+img.getHeight());
//                }
//                addLine(dp1,dp2,group);
//                addLine(dp2,dp3,group);
//                addLine(dp3,dp1,group);

                /*ArrayList<DepthPoint> points=dt.getVertices();
                 for(int i=0; i< points.size(); i++)
                 {
                 int next=(i+1)%3;
                 addLine(points.get(i), points.get(next), group);
                 DepthPoint dp=points.get(i);
                 Point p=dp.getP();
                 addSphere(dp,group);
                 }
                 addTriangle(dt,group);*/
            } else if (obj instanceof DepthPoint) {
                DepthPoint dp = (DepthPoint) obj;
                Point p = dp.getP();
                Point cp = dp.getColorPoint();
                //addRectPrism((int)p.getX(),(int)p.getY(), (int)((dp.getDepth()-dp.getMinDepth()+1)*10),Color.blue,group);]
                try {
                    Color c = new Color(img.getRGB((int) cp.getX(), (int) cp.getY()));

                    //Color c= rgb.toColor();
                    addSphere(dp, group, c, 600, 600, 100);
                } catch (Exception e) {
                    //System.out.println((int)p.getX()+" "+(int)p.getY()+" "+img.getWidth()+" "+img.getHeight());
                }
                //(int)p.getX(),(int)p.getY(), (int)((dp.getDepth()-dp.getMinDepth()+1)*10)

                //System.out.println(p+" "+(int)((dp.getDepth()-dp.getMinDepth()+1)*10));
            } else {
                System.out.println(obj.getClass().getCanonicalName());
            }

        }

        addTriangles(triangles,group,img);
        //addLines(lines, group);
//        TriangleArray pyramidGeometry = new TriangleArray(triangles.size()*3,
//				TriangleArray.COORDINATES);
//		
//        for(int i=0; i<triangles.size(); i++)
//        {
//            DepthTriangle t=triangles.get(i);
//            ArrayList<DepthPoint> vertices=t.getVertices();
//            for(int v=0; v<3; v++)
//            {
//                DepthPoint p=vertices.get(v);
//                pyramidGeometry.setCoordinate((i*3)+v, convertPoint(p));
//            }
//        }
//        GeometryInfo geometryInfo = new GeometryInfo(pyramidGeometry);
//        NormalGenerator ng = new NormalGenerator();
//        ng.generateNormals(geometryInfo);
//        GeometryArray result = geometryInfo.getGeometryArray();
//        Appearance appearance = new Appearance();
//		Color3f color = new Color3f(Color.yellow);
//		Color3f black = new Color3f(0.0f, 0.0f, 0.0f);
//		Color3f white = new Color3f(1.0f, 1.0f, 1.0f);
//		Texture texture = new Texture2D();
//		TextureAttributes texAttr = new TextureAttributes();
//		texAttr.setTextureMode(TextureAttributes.MODULATE);
//		texture.setBoundaryModeS(Texture.WRAP);
//		texture.setBoundaryModeT(Texture.WRAP);
//		texture.setBoundaryColor(new Color4f(0.0f, 1.0f, 0.0f, 0.0f));
//		Material mat = new Material(color, black, color, white, 70f);
//		appearance.setTextureAttributes(texAttr);
//		appearance.setMaterial(mat);
//		appearance.setTexture(texture);
//		Shape3D shape = new Shape3D(result, appearance);
//		group.addChild(shape);
//        Color3f light1Color = new Color3f(Color.white); // green light
//
//        BoundingSphere bounds =
//                new BoundingSphere(new Point3d(0.0, 0.0, 0.0), 2.0);
//
//        Vector3f light1Direction = new Vector3f(4.0f, -7.0f, -12.0f);
//
//        DirectionalLight light1 = new DirectionalLight(light1Color, light1Direction);
//
//        light1.setInfluencingBounds(bounds);
//
//        group.addChild(light1);
        //AmbientLight ambientLightNode = new AmbientLight (light1Color);
        //ambientLightNode.setInfluencingBounds (bounds);
        //group.addChild (ambientLightNode);
        BoundingSphere bounds
                = new BoundingSphere(new Point3d(0.0, 0.0, 0.0), 600.0);

        Color3f light1Color = new Color3f(Color.white);
        AmbientLight ambientLightNode = new AmbientLight(light1Color);
        ambientLightNode.setInfluencingBounds(bounds);
        group.addChild(ambientLightNode);
        //universe.getViewingPlatform().setNominalViewingTransform();

        // add the group of objects to the Universe
        universe.addBranchGraph(group);
        System.out.println("finished recreating");
        universe.getCanvas().repaint();
        pBarFrame.setVisible(false);
    }

    public static void main(String[] args) {

        new Simulated3d();

    }
    public boolean upPressed = false;
    public boolean downPressed = false;
    public boolean leftPressed = false;
    public boolean rightPressed = false;
    public boolean liftPressed = false;
    public boolean lowerPressed = false;
    public boolean lookUpPressed = false;
    public boolean lookDownPressed = false;
    public boolean lookLeftPressed = false;
    public boolean lookRightPressed = false;
    public boolean shiftPressed = false;
    Point prevPoint = null;
    double cameraX = 0;
    double cameraY = 0;
    double cameraZ = 4;
    double centerX = 0;
    double centerY = 0;
    double centerZ = 0;
    double viewXAngle = 0;
    double viewYAngle = 0;
    double viewZAngle = 0;
//    double rotX = 0;
//    double rotY = 1;
//    double rotZ = 0;

    public void moveUp(double dist) {

        cameraX += (Math.sin(viewXAngle) * dist);
        cameraY += (Math.sin(viewYAngle + (Math.PI / 2)) * dist);
        //cameraZ -= (Math.cos(viewXAngle) * dist);
    }

    public void moveDown(double dist) {
        cameraX += (Math.sin(viewXAngle) * dist);
        cameraY += (Math.sin(viewYAngle - (Math.PI / 2)) * dist);
        //cameraZ -= (Math.cos(viewXAngle) * dist);
    }

    public void moveForward(double dist) {
        cameraX += (Math.sin(viewXAngle) * dist);
        cameraY += (Math.sin(viewYAngle) * dist);
        cameraZ -= (Math.cos(viewXAngle) * dist);
    }

    public void moveRight(double dist) {
        cameraX += (Math.sin(viewXAngle + (Math.PI / 2)) * dist);
        cameraY += (Math.sin(viewYAngle) * dist);
        cameraZ -= (Math.cos(viewXAngle + (Math.PI / 2)) * dist);

    }

    public void moveLeft(double dist) {
        cameraX += (Math.sin(viewXAngle - (Math.PI / 2)) * dist);
        cameraY += (Math.sin(viewYAngle) * dist);
        cameraZ -= (Math.cos(viewXAngle - (Math.PI / 2)) * dist);

    }

    public void redraw() {
        setCameraPositionAngle(cameraX, cameraY, cameraZ, viewXAngle, viewYAngle, viewZAngle);
    }

    public void setCameraPositionAngle(double x, double y, double z, double viewXAngle, double viewYAngle, double viewZAngle) {

        double centerX1 = x + (Math.sin(viewXAngle) * 4);
        double centerY1 = y + (Math.sin(viewYAngle) * 4);
        double centerZ1 = z - (Math.cos(viewXAngle) * 4);
        //System.out.println("("+x+","+y+","+z+")("+centerX1+","+centerY1+","+centerZ1+")");
        setCameraPosition(x, y, z, centerX1, centerY1, centerZ1);
    }
    boolean spacePressed = false;

    @Override
    public void mouseMoved(MouseEvent me) {
        if (spacePressed) {
            if (prevPoint == null) {
                prevPoint = me.getPoint();
            } else {
                Point curPoint = me.getPoint();
                viewXAngle += Math.toRadians((curPoint.getX() - prevPoint.getX()) / 10);
                viewYAngle -= Math.toRadians((curPoint.getY() - prevPoint.getY()) / 10);
                //centerX+=(curPoint.getX() - prevPoint.getX()) / 100;
                //centerY-=(curPoint.getY() - prevPoint.getY()) / 100;

                //setCameraPositionAngle(cameraX, cameraY, cameraZ, viewXAngle, viewYAngle,viewZAngle);
//            
//            if (me.getButton() == MouseEvent.BUTTON1) {
//
//
//                
//               
//            }
//            else
//            {
//                centerX+=(curPoint.getX() - prevPoint.getX()) / 100;
//                centerY-=(curPoint.getY() - prevPoint.getY()) / 100;
//                setCameraPosition(cameraX, cameraY, cameraZ, centerX, centerY,centerZ);
//            }
                prevPoint = curPoint;
            }
        } else {
            prevPoint = me.getPoint();
        }

    }

    public void setCameraPosition(double x, double y, double z, double centerX, double centerY, double centerZ) {
        if (universe != null) {
            ViewingPlatform viewPlatform = universe.getViewingPlatform();
            TransformGroup viewTransform = viewPlatform.getViewPlatformTransform();
            Transform3D t3d = new Transform3D();
            viewTransform.getTransform(t3d);
            t3d.lookAt(new Point3d(x, y, z), new Point3d(centerX, centerY, centerZ), new Vector3d(0, 1, 0));
            t3d.invert();
            viewTransform.setTransform(t3d);
        } else {
            System.out.println("universe is null");
        }
    }

//    public void setCameraRot(double x, double y, double z) {
//        ViewingPlatform viewPlatform = universe.getViewingPlatform();
//        TransformGroup viewTransform = viewPlatform.getViewPlatformTransform();
//        Transform3D t3d = new Transform3D();
//        viewTransform.getTransform(t3d);
//        t3d.rotX(x);
//        t3d.rotY(y);
//        t3d.rotZ(z);
//        viewTransform.setTransform(t3d);
//    }
    public void mouseDragged(MouseEvent me) {

        if (prevPoint == null) {
            prevPoint = me.getPoint();
        } else {
            Point curPoint = me.getPoint();
            if (me.getButton() == MouseEvent.BUTTON1) {

                cameraX -= (curPoint.getX() - prevPoint.getX()) / 100;
                cameraY += (curPoint.getY() - prevPoint.getY()) / 100;
                centerX += (curPoint.getX() - prevPoint.getX()) / 100;
                centerY -= (curPoint.getY() - prevPoint.getY()) / 100;

                setCameraPosition(cameraX, cameraY, cameraZ, centerX, centerY, centerZ);
            } else {
                centerX += (curPoint.getX() - prevPoint.getX()) / 100;
                centerY -= (curPoint.getY() - prevPoint.getY()) / 100;
                setCameraPosition(cameraX, cameraY, cameraZ, centerX, centerY, centerZ);
            }
            prevPoint = curPoint;
        }

        //universe.
    }
    /*boolean spacePressed=false;

     @Override
     public void mouseMoved(MouseEvent me) {
     if(spacePressed)
     {
     if (prevPoint == null) {
     prevPoint = me.getPoint();
     } else {
     Point curPoint = me.getPoint();
     centerX+=(curPoint.getX() - prevPoint.getX()) / 100;
     centerY-=(curPoint.getY() - prevPoint.getY()) / 100;
                
     setCameraPosition(cameraX, cameraY, cameraZ, centerX, centerY,centerZ);
     //            
     //            if (me.getButton() == MouseEvent.BUTTON1) {
     //
     //
     //                
     //               
     //            }
     //            else
     //            {
     //                centerX+=(curPoint.getX() - prevPoint.getX()) / 100;
     //                centerY-=(curPoint.getY() - prevPoint.getY()) / 100;
     //                setCameraPosition(cameraX, cameraY, cameraZ, centerX, centerY,centerZ);
     //            }
     prevPoint = curPoint;
     }
     }

     }*/

    @Override
    public void mouseClicked(MouseEvent me) {
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void mousePressed(MouseEvent me) {
        prevPoint = me.getPoint();
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void mouseReleased(MouseEvent me) {
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void mouseEntered(MouseEvent me) {
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void mouseExited(MouseEvent me) {
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void mouseWheelMoved(MouseWheelEvent mwe) {
        cameraZ += mwe.getUnitsToScroll() / 10.0;
        setCameraPositionAngle(cameraX, cameraY, cameraZ, viewXAngle, viewYAngle, viewZAngle);
    }

    @Override
    public void keyTyped(KeyEvent ke) {
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void keyPressed(KeyEvent ke) {
        if (ke.getKeyCode() == KeyEvent.VK_UP) {
            lookUpPressed = true;
        }
        if (ke.getKeyCode() == KeyEvent.VK_W) {
            upPressed = true;
            /*//System.out.println("up arrow");
             moveForward(.1);
             //centerZ-=1.0/10;
             setCameraPositionAngle(cameraX, cameraY, cameraZ, viewXAngle, viewYAngle,viewZAngle);*/
        }
        if (ke.getKeyCode() == KeyEvent.VK_DOWN) {
            lookDownPressed = true;
        }
        if (ke.getKeyCode() == KeyEvent.VK_S) {
            downPressed = true;
            /*// System.out.println("down arrow");
             moveForward(-.1);
            
             setCameraPositionAngle(cameraX, cameraY, cameraZ, viewXAngle, viewYAngle,viewZAngle);*/
        }
        if (ke.getKeyCode() == KeyEvent.VK_RIGHT) {
            lookRightPressed = true;
        }
        if (ke.getKeyCode() == KeyEvent.VK_D) {
            rightPressed = true;
//            //System.out.println("right arrow");
//            System.out.println(viewXAngle+","+viewYAngle+","+viewZAngle);
//            moveRight(.1);
//            setCameraPositionAngle(cameraX, cameraY, cameraZ, viewXAngle, viewYAngle,viewZAngle);
        }
        if (ke.getKeyCode() == KeyEvent.VK_LEFT) {
            lookLeftPressed = true;
        }
        if (ke.getKeyCode() == KeyEvent.VK_A) {
            leftPressed = true;
//            //System.out.println("left arrow");
//            moveLeft(.1);
//            setCameraPositionAngle(cameraX, cameraY, cameraZ, viewXAngle, viewYAngle,viewZAngle);
        }
        if (ke.getKeyCode() == KeyEvent.VK_SHIFT) {
            shiftPressed = true;
        }

        if (ke.getKeyCode() == KeyEvent.VK_SPACE) {
            if (spacePressed) {
                spacePressed = false;
            } else {
                spacePressed = true;
            }

        }
        if (ke.getKeyCode() == KeyEvent.VK_PLUS) {
            if (ke.isShiftDown()) {
                lookSpeed += lookSpeed / 2.0;
            } else {
                speed += speed / 2.0;
            }
        }
        if (ke.getKeyCode() == KeyEvent.VK_MINUS) {
            if (ke.isShiftDown()) {
                lookSpeed -= lookSpeed / 2.0;
            } else {
                speed -= speed / 2.0;
            }
        }
        if (ke.getKeyCode() == KeyEvent.VK_Q) {
            liftPressed = true;
        }
        if (ke.getKeyCode() == KeyEvent.VK_E) {
            lowerPressed = true;
        }
        if (ke.getKeyCode() == KeyEvent.VK_P) {
            System.out.println("---------------------------------------------------------------");
            System.out.println("camera (" + cameraX + "," + cameraY + "," + cameraZ + ")");
            System.out.println("center (" + centerX + "," + centerY + "," + centerZ + ")");
            System.out.println("view angle (" + viewXAngle + "," + viewYAngle + "," + viewZAngle + ")");
            System.out.println("---------------------------------------------------------------");
        }
    }

    @Override
    public void keyReleased(KeyEvent ke) {

        if (ke.getKeyCode() == KeyEvent.VK_UP) {
            lookUpPressed = false;
        }
        if (ke.getKeyCode() == KeyEvent.VK_W) {
            upPressed = false;
            /*//System.out.println("up arrow");
             moveForward(.1);
             //centerZ-=1.0/10;
             setCameraPositionAngle(cameraX, cameraY, cameraZ, viewXAngle, viewYAngle,viewZAngle);*/
        }
        if (ke.getKeyCode() == KeyEvent.VK_DOWN) {
            lookDownPressed = false;
        }
        if (ke.getKeyCode() == KeyEvent.VK_S) {
            downPressed = false;
            /*// System.out.println("down arrow");
             moveForward(-.1);
            
             setCameraPositionAngle(cameraX, cameraY, cameraZ, viewXAngle, viewYAngle,viewZAngle);*/
        }
        if (ke.getKeyCode() == KeyEvent.VK_RIGHT) {
            lookRightPressed = false;
        }
        if (ke.getKeyCode() == KeyEvent.VK_D) {
            rightPressed = false;
//            //System.out.println("right arrow");
//            System.out.println(viewXAngle+","+viewYAngle+","+viewZAngle);
//            moveRight(.1);
//            setCameraPositionAngle(cameraX, cameraY, cameraZ, viewXAngle, viewYAngle,viewZAngle);
        }
        if (ke.getKeyCode() == KeyEvent.VK_LEFT) {
            lookLeftPressed = false;
        }
        if (ke.getKeyCode() == KeyEvent.VK_A) {
            leftPressed = false;
//            //System.out.println("left arrow");
//            moveLeft(.1);
//            setCameraPositionAngle(cameraX, cameraY, cameraZ, viewXAngle, viewYAngle,viewZAngle);
        }
        if (ke.getKeyCode() == KeyEvent.VK_SHIFT) {
            shiftPressed = false;
        }
        if (ke.getKeyCode() == KeyEvent.VK_Q) {
            liftPressed = false;
        }
        if (ke.getKeyCode() == KeyEvent.VK_E) {
            lowerPressed = false;
        }

        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    //@Override
    boolean running = true;
    private double speed = .01;
    double lookSpeed = .5;

    public void run() {
        while (running) {
            //System.out.println("running");

            if (upPressed) {
                moveForward(getSpeed());
            }
            if (downPressed) {
                moveForward(-getSpeed());
            }
            if (leftPressed) {
                moveLeft(getSpeed());
            }
            if (rightPressed) {
                moveRight(getSpeed());
            }
            if (liftPressed) {
                moveUp(getSpeed());
            }
            if (lowerPressed) {
                moveDown(getSpeed());
            }

            if (lookUpPressed) {

                viewYAngle += Math.toRadians(lookSpeed);
            }
            if (lookDownPressed) {

                viewYAngle -= Math.toRadians(lookSpeed);
            }
            if (lookLeftPressed) {
                viewXAngle -= Math.toRadians(lookSpeed);

            }
            if (lookRightPressed) {
                viewXAngle += Math.toRadians(lookSpeed);

            }

            redraw();
            try {
                Thread.sleep(20);
            } catch (InterruptedException ex) {
                Logger.getLogger(Simulated3d.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    public void destroy() {
        // will cause thread to stop looping 
        running = false;
        // destroy it. 
        keyboardThread = null;
    }

    /**
     * @return the speed
     */
    public double getSpeed() {
        return speed;
    }

    /**
     * @param speed the speed to set
     */
    public void setSpeed(double speed) {
        this.speed = speed;
    }
}
