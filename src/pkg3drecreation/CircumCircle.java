/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pkg3drecreation;

import java.awt.*;



/**
 *
 * @author kingm14
 */
public class CircumCircle {
    private Point.Double p;
    private double rad;
    public CircumCircle(Point.Double point, double radius)
    {
        
        this.p=p;
        this.rad=rad;
    }
    public  CircumCircle(Point p1, Point p2, Point p3)
    {
         double x1=p1.getX();
        double y1=p1.getY();
       
        
        double x2=p2.getX();
        double y2=p2.getY();
        
        double x3=p3.getX();
        double y3=p3.getY();
        
        //edge0 = p1-->p2
        //edge1=p2-->p3
        
        //-1:no error
        //1:m0=infinity
        //2:m1=infinity
        //3: both=infinity
        
        double slope0=((y2-y1)/(x2-x1));
        double slope1=((y3-y2)/(x3-x2));
//        System.out.println("slope0="+slope0);
//        System.out.println("slope1="+slope1);
        double m0=-1/slope0;
        double m1=-1/slope1;
        if(slope0==0||slope0==-0)
        {
            //infinityErrorCode=1;
//            System.out.println("*slope0=0");
            m0=Integer.MAX_VALUE;
        }
        if(slope1==0||slope1==-0)
        {
//            System.out.println("*slope1=0");
            m1=Integer.MAX_VALUE;
            
        }
        
        
        //infinity error
        double mX0=(x1+x2)/2;
        double mY0=(y1+y2)/2;
        
        double mX1=(x2+x3)/2;
        double mY1=(y2+y3)/2;
        
        
        
        double x=(mY1-mY0+(m0*mX0)-(m1*mX1))/(m0-m1);
        double y=m0*(x-mX0)+mY0;
        p=(new Point.Double(x,y));
        rad=(p.distance(p3));
//        System.out.println(p1+","+p2+","+p3);
//        System.out.println("mid0="+mX0+","+mY0);
//        System.out.println("mid1="+mX1+","+mY1);
//        System.out.println("m0="+m0);
//        System.out.println("m1="+m1);
//        System.out.println(x+","+y);
    }
    public CircumCircle()
    {
        
    }
    public boolean contains(Point np)
    {
        return getP().distance(np)<=getRad();
    }
    public void create(Point p1, Point p2, Point p3)
    {
        
        double x1=p1.getX();
        double y1=p1.getY();
       
        
        double x2=p2.getX();
        double y2=p2.getY();
        
        double x3=p3.getX();
        double y3=p3.getY();
        
        //edge0 = p1-->p2
        //edge1=p2-->p3
        double mX0=(x1+x2)/2;
        double mY0=(y1+y2)/2;
        
        double mX1=(x2+x3)/2;
        double mY1=(y2+y3)/2;
        
        double m0=-1/((y2-y1)/(x2-x1));
        double m1=-1/((y3-y2)/(x3-x2));
        
        double x=(mY1-mY0+(m0*mX0)-(m1*mX1))/(m0-m1);
        double y=m0*(x-mX0)+mY0;
        setP(new Point.Double(x,y));
        setRad(getP().distance(p3));
        System.out.println(x+","+y);
        
        
    }
    public static void main(String[] args)
    {
        CircumCircle c=new CircumCircle();
        c.create(new Point(0,0), new Point(1,1), new Point(2,0));
    }

    /**
     * @return the p
     */
    public Point.Double getP() {
        return p;
    }

    /**
     * @param p the p to set
     */
    public void setP(Point.Double p) {
        this.p = p;
    }

    /**
     * @return the rad
     */
    public double getRad() {
        return rad;
    }

    /**
     * @param rad the rad to set
     */
    public void setRad(double rad) {
        this.rad = rad;
    }
}
