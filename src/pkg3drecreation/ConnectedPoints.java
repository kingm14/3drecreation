/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pkg3drecreation;

import java.awt.Point;
import java.util.*;

/**
 *
 * @author kingm14
 */
public class ConnectedPoints {
    
    private ArrayList<Point> connecteds;
    final int minDist = 8;
    
    public ConnectedPoints() {
        connecteds = new ArrayList<Point>();
    }
    
    public ConnectedPoints(ArrayList<Point> connecteds) {
        this.connecteds = connecteds;
    }
    
    public void add(Point p) {
        connecteds.add(p);
    }
    
    public void addAll(ArrayList<Point> p) {
        connecteds.addAll(p);
    }
    
    public void addAll(ConnectedPoints p) {
        connecteds.addAll(p.getConnecteds());
    }
    
    private class XComparator implements Comparator {
        
        @Override
        public int compare(Object t, Object t1) {
            Point p1 = (Point) t;
            Point p2 = (Point) t1;
            return (int) (p1.getX()) - (int) (p2.getX());
        }
    }
    
    private class YComparator implements Comparator {
        
        @Override
        public int compare(Object t, Object t1) {
            Point p1 = (Point) t;
            Point p2 = (Point) t1;
            return (int) (p1.getY()) - (int) (p2.getY());
        }
    }
    
    public ArrayList<Corner> groupPoints() {
        ArrayList<Corner> ret = new ArrayList<Corner>();
        Point minX = Collections.min(connecteds, new XComparator());
        Point maxX = Collections.max(connecteds, new XComparator());
        Point minY = Collections.min(connecteds, new YComparator());
        Point maxY = Collections.max(connecteds, new YComparator());
        RGB trash=new RGB(0,0,0);
        if ((maxX.getX() - minX.getX()) > minDist || (maxY.getY() - minY.getY()) > minDist) {
            ret.add(new Corner((int)minX.getX(), (int)minX.getY(),trash));
            ret.add(new Corner((int)maxX.getX(), (int)maxX.getY(),trash));
            ret.add(new Corner((int)minY.getX(), (int)minY.getY(),trash));
            ret.add(new Corner((int)maxY.getX(), (int)maxY.getY(),trash));
        } else {
            double sumX = 0;
            double sumY = 0;
            
            for (int j = 0; j < connecteds.size(); j++) {
                Point cur = connecteds.get(j);
                sumX += cur.getX();
                sumY += cur.getY();
                
                
            }
            ret.add(new Corner((int) Math.round(sumX / connecteds.size()), (int) Math.round(sumY / connecteds.size()),trash));
        }
        return ret;
    }

    /**
     * @return the connecteds
     */
    public ArrayList<Point> getConnecteds() {
        return connecteds;
    }

    /**
     * @param connecteds the connecteds to set
     */
    public void setConnecteds(ArrayList<Point> connecteds) {
        this.connecteds = connecteds;
    }
}
