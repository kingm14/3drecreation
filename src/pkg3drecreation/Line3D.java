/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pkg3drecreation;

import java.awt.geom.Line2D;
import javax.vecmath.Point3d;

/**
 *
 * @author kingm14
 */
public class Line3D {
    public static boolean doLinesIntersect(Point3d l1Start, Point3d l1End, Point3d l2Start, Point3d l2End)
    {
        boolean plane1Intersects=Line2D.linesIntersect(l1Start.x, l1Start.y, l1End.x, l1End.y, l2Start.x, l2Start.y, l2End.x, l2End.y);
        boolean plane2Intersects=Line2D.linesIntersect(l1Start.x, l1Start.z, l1End.x, l1End.z, l2Start.x, l2Start.z, l2End.x, l2End.z);
        boolean plane3Intersects=Line2D.linesIntersect(l1Start.z, l1Start.y, l1End.z, l1End.y, l2Start.z, l2Start.y, l2End.z, l2End.y);
        
        int sum=0;
        if(plane1Intersects)
        {
            sum++;
        }
        if(plane2Intersects)
        {
            sum++;
        }
        if(plane3Intersects)
        {
            sum++;
        }
        
        
        return plane1Intersects;
    }
    //[xplane,yplane, zplane]
    public static boolean[] getIntersectPlanes(Point3d l1Start, Point3d l1End, Point3d l2Start, Point3d l2End)
    {
        
        boolean plane1Intersects=Line2D.linesIntersect(l1Start.x, l1Start.y, l1End.x, l1End.y, l2Start.x, l2Start.y, l2End.x, l2End.y);
        boolean plane2Intersects=Line2D.linesIntersect(l1Start.x, l1Start.z, l1End.x, l1End.z, l2Start.x, l2Start.z, l2End.x, l2End.z);
        boolean plane3Intersects=Line2D.linesIntersect(l1Start.z, l1Start.y, l1End.z, l1End.y, l2Start.z, l2Start.y, l2End.z, l2End.y);
        boolean[] ret=new boolean[]{plane1Intersects,plane2Intersects,plane3Intersects};
        
        
        
        return ret;
    }
}
