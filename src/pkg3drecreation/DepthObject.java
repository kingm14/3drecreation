/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pkg3drecreation;

import java.awt.*;

/**
 *
 * @author kingm14
 */
public abstract class DepthObject implements Comparable, Saveable {
    protected double depth;
    protected double maxDepth;
    protected double minDepth;
    protected boolean maxDepthInited=false;
    protected boolean minDepthInited=false;
    public abstract void paint(Graphics g, double scale);

    /**
     * @return the depth
     */
    public double getDepth() {
        return depth;
    }

    /**
     * @param depth the depth to set
     */
    public void setDepth(double depth) {
        this.depth = depth;
    }

    /**
     * @return the maxDepth
     */
    public double getMaxDepth() {
        return maxDepth;
    }

    /**
     * @param maxDepth the maxDepth to set
     */
    public void setMaxDepth(double maxDepth) {
        this.maxDepth = maxDepth;
        maxDepthInited=true;
    }
    
    @Override
    public int compareTo(Object t) {
        
        return (int)(depth-((DepthObject)t).getDepth());
    }

    /**
     * @return the minDepth
     */
    public double getMinDepth() {
        return minDepth;
    }

    /**
     * @param minDepth the minDepth to set
     */
    public void setMinDepth(double minDepth) {
        this.minDepth = minDepth;
        minDepthInited=true;

    }
    public String getDepthSaveString()
    {
        return getMinDepth()+","+getDepth()+","+getMaxDepth();
    }
}
