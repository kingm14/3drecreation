/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pkg3drecreation;

import java.util.ArrayList;

/**
 *
 * @author kingm14
 */
public class Converter {

    private static char symbols[] = new char[] { '0','1','2','3','4','5','6','7','8','9','A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T' };

    public static void main ( String args[] )
    {
                 Converter converter = new Converter ();
        System.out.println( converter.convert ( 31, 16 ));
    }

    public String convert ( int number, int base )
    {
        return convert(number, base, 0, "-" );
    }
    
    private String convert ( int number, int base, int position, String result )
    {
        if ( number < Math.pow(base, position + 1) )
        {
            return (int)((number / (int)Math.pow(base, position))) + result;
        }
        else
        {
            int remainder = (number % (int)Math.pow(base, position + 1));
            return convert (  number - remainder, base, position + 1, "-"+(remainder / (int)( Math.pow(base, position) ))+"-" + result );
        }
    }
    public static ArrayList<Integer> convertList(int number, int base)
    {
        Converter converter=new Converter();
        String s=converter.convert(number,base);
        System.out.print(number+"-->"+s+"-->");
        String[] split=s.split("-");
        ArrayList<Integer> list= new ArrayList<Integer>();
        for(int i=0; i<split.length; i++)
        {
            if(!split[i].equals(""))
            {
                list.add(Integer.parseInt(split[i]));
            }
        }
        for(Integer in:list)
        {
            System.out.print("["+in+"]");
        }
        System.out.println();
        return list;
    }
    public static ArrayList<Integer> convertList(int number, int base, int numBins)
    {
        ArrayList<Integer> list=convertList(number, base);
        while(list.size()<numBins)
        {
            list.add(0, 0);
        }
        return list;
    }
}