/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package pkg3drecreation;

/**
 *
 * @author micahking
 */
public interface Saveable {
    public String toSaveString();
    
    //return null if invalid save String
    //public Saveable parseSaveString(String saveString);
}
