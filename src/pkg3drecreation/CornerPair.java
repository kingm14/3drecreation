/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pkg3drecreation;

import java.util.ArrayList;

/**
 *
 * @author kingm14
 */
public class CornerPair {
    private ArrayList<Corner> pairs;
    public CornerPair(Corner corner1, Corner corner2)
    {
        pairs=new ArrayList<Corner>();
        pairs.add(corner1);
        pairs.add(corner2);
    }
    public CornerPair( ArrayList<Corner> pairs)
    {
        this.pairs=pairs;
    }

    /**
     * @return the pairs
     */
    public ArrayList<Corner> getPairs() {
        return pairs;
    }

    /**
     * @param pairs the pairs to set
     */
    public void setPairs(ArrayList<Corner> pairs) {
        this.pairs = pairs;
    }
}
