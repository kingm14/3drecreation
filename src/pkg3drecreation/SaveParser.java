/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 *//*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pkg3drecreation;

import java.awt.Point;

/**
 *
 * @author micahking
 */
public class SaveParser {

    public Saveable parseSaveString(String s) {
        String code = s.substring(0, s.indexOf(":"));
        String[] parts = s.split("|");
        if (parts.length == 0) {
            return null;
        }
        if (parts[0].equals("dp")) {
            //is a depthPoint
            String[] pParts = parts[1].split(",");
            String[] cpParts = parts[2].split(",");
            String[] depthParts = parts[3].split(",");

            DepthPoint ret = new DepthPoint(new Point(Integer.parseInt(pParts[0]), Integer.parseInt(pParts[1])), Double.parseDouble(depthParts[1]));
            ret.setColorPoint(new Point(Integer.parseInt(cpParts[0]), Integer.parseInt(cpParts[1])));
            if (!depthParts[0].equals("null")) {
                ret.setMinDepth(Double.parseDouble(depthParts[0]));
            }
            if (!depthParts[2].equals("null")) {
                ret.setMaxDepth(Double.parseDouble(depthParts[2]));
            }
            return ret;
        }
        else if(parts[0].equals("dt"))
        {
            //depthTriangle
            String[] vertices = parts[1].split(":");
            DepthPoint v1=(DepthPoint)parseSaveString(vertices[0]);
            DepthPoint v2=(DepthPoint)parseSaveString(vertices[1]);
            DepthPoint v3=(DepthPoint)parseSaveString(vertices[2]);
            
            double alpha=Double.parseDouble(parts[2]);
            
            DepthTriangle tri=new DepthTriangle(v1,v2,v3);
            tri.setAlpha(alpha);
            return tri;
        }
        else if(parts[0].equals("dl"))
        {
            //depthTriangle
            String[] vertices = parts[1].split(":");
            DepthPoint start=(DepthPoint)parseSaveString(vertices[0]);
            DepthPoint end=(DepthPoint)parseSaveString(vertices[1]);
            
           
            DepthLine line=new DepthLine(start,end);
            return line;
        }
        return null;
    }
}