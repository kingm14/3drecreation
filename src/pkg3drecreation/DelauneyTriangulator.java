/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pkg3drecreation;

//import delaunay_triangulation.*;
import DelaunayTriangulation.*;
import java.awt.*;
import java.util.*;
import java.util.concurrent.ExecutionException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JProgressBar;
import javax.swing.SwingWorker;
import javax.vecmath.Point3d;

/**
 *
 * @author kingm14
 */
public class DelauneyTriangulator {

    private static double maxLength = 1500;

    /**
     * @return the maxLength
     */
    public static double getMaxLength() {
        return maxLength;
    }

    /**
     * @param aMaxLength the maxLength to set
     */
    public static void setMaxLength(double aMaxLength) {
        maxLength = aMaxLength;
    }

    public DelauneyTriangulator() {
        triangles = new ArrayList<DepthObject>();
    }

    public static DepthMap createMapPreMade(ArrayList<DepthObject> points, int numReps) {

        ArrayList<DepthObject> newPoints = points;
        ArrayList<DepthObject> triangles = new ArrayList<DepthObject>();
        ProgressBarFrame pBarFrame = new ProgressBarFrame();
        pBarFrame.setUpdateBarVal(1);
        pBarFrame.setVisible(true);
        for (int rep = 0; rep < numReps; rep++) {

            Point_dt[] dPoints = new Point_dt[newPoints.size()];
            pBarFrame.setStatus("converting to Point_dt[]");
            pBarFrame.setProgressBar1Max(newPoints.size());
            for (int i = 0; i < newPoints.size(); i++) {
                pBarFrame.setProgressBar1Val(i);
                DepthObject dObj = newPoints.get(i);
                //Point3d dObjPoint=((DepthPoint)dObj).get3DPoint();
                Point_dt temp = new Point_dt((DepthPoint) dObj);
                dPoints[i] = temp;
            }
            pBarFrame.setStatus("Triangulating");
            Delaunay_Triangulation dt = new Delaunay_Triangulation(dPoints);

            pBarFrame.setStatus("converting to DepthMap");
            Iterator<Triangle_dt> iter = dt.trianglesIterator();

            pBarFrame.setProgressBar1Max(dt.trianglesSize());
            int count = 0;
            newPoints = new ArrayList<DepthObject>();
            while (iter.hasNext()) {
                pBarFrame.setProgressBar1Val(count);
                count++;

                Triangle_dt cur = iter.next();
                if (cur.p1() != null && cur.p2() != null && cur.p3() != null) {

                    DepthPoint p1 = cur.p1().getOriginalPoint();
                    Point3d p13d = p1.get3DPoint();

                    DepthPoint p2 = cur.p2().getOriginalPoint();
                    Point3d p23d = p2.get3DPoint();

                    DepthPoint p3 = cur.p3().getOriginalPoint();
                    Point3d p33d = p3.get3DPoint();

                    double dist12 = p13d.distance(p23d);
                    double dist23 = p23d.distance(p33d);
                    double dist31 = p33d.distance(p13d);
                    if (rep != numReps - 1) {
                        //System.out.prinp13d.distance(p23d)tln(cur.toString());
                        if (dist12 < maxLength && dist31 < maxLength) {
                            newPoints.add(p1);
                        }
                        if (dist12 < maxLength && dist23 < maxLength) {
                            newPoints.add(p2);
                        }
                        if (dist31 < maxLength && dist23 < maxLength) {
                            newPoints.add(p3);
                        }
                    } else {
                        DepthTriangle newTriangle = new DepthTriangle(p1, p2, p3);
                        triangles.add(newTriangle);
                    }

                }
//            else
//            {
//                System.out.println("errored");
//            }
            }
        }
        DepthMap depthMap = new DepthMap(triangles);
        pBarFrame.setVisible(false);
        return depthMap;
        

    }

    public static DepthMap createMap(ArrayList<DepthObject> points, Point topLeft, Point bottomRight) {
        DelauneyTriangulator t = new DelauneyTriangulator();
        return t.createMap3(points, topLeft, bottomRight);
    }

    public static DepthMap createMapWithProgressBar(ArrayList<DepthObject> points, JProgressBar bar, WebcamTracker wt) {
//        MapCreator creator=new MapCreator();
//        creator.init(points, bar, wt);
//        creator.execute();
//        while(!creator.isDone())
//        {
//            try {
//                Thread.sleep(10);
//            } catch (InterruptedException ex) {
//                Logger.getLogger(DelauneyTriangulator.class.getName()).log(Level.SEVERE, null, ex);
//            }
//            
//        }
//        
//        return creator.getResult();
        DelauneyTriangulator t = new DelauneyTriangulator();
        return t.createMap3(points, new Point(0, 0), new Point(1000, 1000));
    }

    public static class Repainter extends SwingWorker {

        private WebcamTracker wt;

        public void init(WebcamTracker wt) {

            this.wt = wt;
        }

        protected Object doInBackground() {
            wt.repaint();
            return null;
        }

    }

    public static class MapCreator<Object, Void> extends SwingWorker {

        private ArrayList<DepthObject> points;
        private JProgressBar bar;
        private WebcamTracker wt;
        private DepthMap result;

        public void init(ArrayList<DepthObject> points) {
            this.points = points;
        }

        public void init(ArrayList<DepthObject> points, JProgressBar bar, WebcamTracker wt) {
            this.points = points;
            this.bar = bar;
            this.wt = wt;
        }

        @Override
        protected DepthMap doInBackground() {
            ArrayList<DepthObject> lines = new ArrayList<DepthObject>();
            double maxVal = points.size() * points.size();
            this.setBarMax(maxVal);
            for (int i = 0; i < points.size(); i++) {

                DepthObject obj = points.get(i);
                if (obj instanceof DepthPoint) {
                    DepthPoint cur = (DepthPoint) obj;
                    System.out.println(cur);
                    for (int j = i + 1; j < points.size(); j++) {
                        DepthObject obj2 = points.get(j);
                        if (obj2 instanceof DepthPoint) {
                            this.setProgress((i * points.size()) + (j - i));
                            DepthPoint next = (DepthPoint) obj2;
                            DepthLine temp = new DepthLine(cur, next);
                            ArrayList<DepthLine> intersections = getIntersections(temp, lines);
                            if (intersections == null) {
                                j++;
                            } else {
                                lines.removeAll(intersections);
                                lines.add(temp);
                            }

                        }
                    }
                }
            }
            return new DepthMap(lines);
            //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        }

        protected void done() {
            try {
                result = (DepthMap) get();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        public void setBarMax(double d) {
            if (bar != null && wt != null) {
                bar.setMaximum((int) (d + 1));
            }
        }

        public void updateProgress(int i) {
            if (bar != null && wt != null) {
                bar.setValue(i);
                wt.repaint();
            }
        }

        /**
         * @return the result
         */
        public DepthMap getResult() {
            return result;
        }
        /**
         * @return the bar
         */
    }

    public DepthMap createMap3(ArrayList<DepthObject> points, Point topLeft, Point bottomRight) {
        ArrayList<DepthObject> lines = new ArrayList<DepthObject>();
        ProgressBarFrame pBarFrame = new ProgressBarFrame();
        pBarFrame.setUpdateBarVal(1);
        pBarFrame.setVisible(true);

        //bar.setMaximum(points.size());
        //bar.setValue(0);
        pBarFrame.setProgressBar1Max(points.size());
        for (int i = 0; i < points.size(); i++) {
            pBarFrame.setProgressBar1Val(i);
            DepthObject obj = points.get(i);
            if (obj instanceof DepthPoint) {
                DepthPoint cur = (DepthPoint) obj;
                pBarFrame.setProgressBar2Max(points.size() - i);
                for (int j = i + 1; j < points.size(); j++) {
                    pBarFrame.setProgressBar2Val(j - i);
                    DepthObject obj2 = points.get(j);
                    if (obj2 instanceof DepthPoint) {
                        DepthPoint next = (DepthPoint) obj2;
                        DepthLine temp = new DepthLine(cur, next);
                        ArrayList<DepthLine> intersections = getIntersections(temp, lines);
                        if (intersections != null) {

                            lines.removeAll(intersections);
                            lines.add(temp);
                        }

                    }
                }
            }
        }
        return new DepthMap(lines);
    }

    public DepthMap createMap4(ArrayList<DepthObject> points, JProgressBar bar, WebcamTracker wt) {
        ArrayList<DepthObject> triangles = new ArrayList<DepthObject>();
        ArrayList<DepthObject> lines = new ArrayList<DepthObject>();
        double maxVal = points.size() * points.size() + 1;
        ProgressBarFrame pBarFrame = new ProgressBarFrame();
        pBarFrame.setUpdateBarVal(1);
        pBarFrame.setVisible(true);

        //bar.setMaximum(points.size());
        //bar.setValue(0);
        pBarFrame.setProgressBar1Max(points.size());
        for (int i = 0; i < points.size(); i++) {
            pBarFrame.setProgressBar1Val(i);
            DepthObject obj = points.get(i);
            if (obj instanceof DepthPoint) {
                DepthPoint cur = (DepthPoint) obj;
                pBarFrame.setProgressBar2Max(points.size() - i);
                for (int j = i + 1; j < points.size(); j++) {
                    pBarFrame.setProgressBar2Val(j - i);
                    //bar.setValue((i*points.size())+(j-i));
                    //System.out.println((i*points.size())+(j-i)+"/"+maxVal);
                    DepthObject obj2 = points.get(j);
                    if (obj2 instanceof DepthPoint) {
                        DepthPoint next = (DepthPoint) obj2;
                        DepthLine temp = new DepthLine(cur, next);
                        ArrayList<DepthLine> intersections = getIntersections(temp, lines);
                        if (intersections != null) {

                            lines.removeAll(intersections);
                            lines.add(temp);
                        }

                    }
                }
            }
        }
        System.out.println("filling");
        maxVal = lines.size() * lines.size() + 1;
        //bar.setMaximum((int)maxVal);
        //bar.setValue(0);
        pBarFrame.setProgressBar1Max(points.size());
        for (int i = 0; i < lines.size(); i++) {
            pBarFrame.setProgressBar1Val(i);
            DepthObject obj = lines.get(i);
            //System.out.println(i);
            if (obj instanceof DepthLine) {
                DepthLine cur = (DepthLine) obj;
                pBarFrame.setProgressBar2Max(lines.size() - i);
                for (int j = i + 1; j < lines.size(); j++) {
                    pBarFrame.setProgressBar2Val(j - i);
                    //System.out.println("~"+j);
                    //bar.setValue((i*lines.size())+(j-i));

                    //System.out.println((i*points.size())+(j-i)+"/"+maxVal);
                    DepthObject obj2 = lines.get(j);
                    if (obj2 instanceof DepthLine) {
                        DepthLine next = (DepthLine) obj2;
                        //bar.setValue((i*lines.size())+(j-i));
                        DepthPoint shared = cur.sharesVertex(next);
                        if (shared != null) {

                            ArrayList<DepthPoint> vertices = new ArrayList<DepthPoint>();
                            if (!cur.getStart().equals(shared)) {
                                vertices.add(cur.getStart());
                            }
                            if (!cur.getEnd().equals(shared)) {
                                vertices.add(cur.getEnd());
                            }
                            if (!next.getStart().equals(shared)) {
                                vertices.add(next.getEnd());
                            }
                            if (!next.getEnd().equals(shared)) {
                                vertices.add(next.getEnd());
                            }
                            if (vertices.size() > 1) {
                                DepthTriangle t = new DepthTriangle(vertices.get(0), vertices.get(1), shared);
                                //System.out.println(t);
                                triangles.add(t);
                                DepthTriangle t2 = new DepthTriangle(vertices.get(1), vertices.get(0), shared);
                                //System.out.println(t2);
                                triangles.add(t2);
                            }

                        }

                    }
                }
            }
        }
        System.out.println("finished");
        lines.addAll(triangles);
        return new DepthMap(lines);
    }
    //returns a list of depthlines that intersect with line me
    //if any intersecting line is shorter returns null

    public static ArrayList<DepthLine> getIntersections(DepthLine me, ArrayList<DepthObject> others) {

        ArrayList<ArrayList<DepthObject>> ret = new ArrayList<ArrayList<DepthObject>>();

        ArrayList<DepthLine> intersectingLines = new ArrayList<DepthLine>();
        //ArrayList<DepthTriangle> triangles = new ArrayList<DepthTriangle>();
        double meLength = me.getLength();
        for (int i = 0; i < others.size(); i++) {
            DepthLine cur = (DepthLine) others.get(i);
            //System.out.println(me.getStart().get3DPoint()+"-->"+me.getEnd().get3DPoint()+"  vs  "+cur.getStart().get3DPoint()+"-->"+cur.getEnd().get3DPoint());

            boolean[] intersectingPlanes = cur.getIntersectingPlanes(me);

            if (intersectingPlanes[0] == true) {
                if (cur.getLength() < meLength) {
                    return null;
                } else {
                    intersectingLines.add(cur);
                }

            }
            /*else if (intersectingPlanes[1]==true&&intersectingPlanes[2]==true) {
             if (cur.getLength() > meLength) {
             return null;
             } else {
             intersectingLines.add(cur);
             }

             }*/
        }
        return intersectingLines;//ret;
    }
    ArrayList<DepthObject> triangles;

    public DepthMap createMap2(ArrayList<DepthObject> points, Point topLeft, Point bottomRight) {

        Collections.sort(points, new DPComparator());

        DepthTriangle superTriangle = createSuperTriangle(topLeft, bottomRight);
        ArrayList<DepthPoint> superVertices = superTriangle.getVertices();
        triangles.add(superTriangle);
        int startIndex = 0;

        for (int i = 0; i < points.size(); i++) {
            DepthObject o = points.get(i);
            if (o != null && o instanceof DepthPoint) {
                DepthPoint p = (DepthPoint) o;
//                System.out.println(p.getP());
                addPoint(p, startIndex);

//                System.out.println(triangles.size());
//                System.out.println(i+"/"+points.size());
            }
        }
//        System.out.println(triangles.size());
        for (int j = 0; j < triangles.size(); j++) {
            DepthTriangle t = (DepthTriangle) triangles.get(j);

            if (t.hasVertex(superVertices.get(0)) || t.hasVertex(superVertices.get(1)) || t.hasVertex(superVertices.get(2))) {
                triangles.remove(j);
                j--;
            }
        }
//        System.out.println(triangles.size());
//        DepthPoint d1=new DepthPoint(topLeft,0);
//        DepthPoint d2=new DepthPoint(new Point((int)topLeft.getX(),(int)bottomRight.getY()),0);
//        DepthPoint d3=new DepthPoint(bottomRight,0);
//        DepthPoint d4=new DepthPoint(new Point((int)bottomRight.getX(),(int)topLeft.getY()),0);
//        triangles.add(new DepthTriangle(d1,d2,d3));
//        triangles.add(new DepthTriangle(d3,d4,d1));
//        
//        for(int i=0; i<points.size(); i++)
//        {
//            DepthObject o=points.get(i);
//            if(o!=null&&o instanceof DepthPoint){
//                DepthPoint p=(DepthPoint)o;
//                
//                    boolean isContained=false;
//                    for(int j=0; j<triangles.size(); j++)
//                    {
//                        DepthTriangle t=(DepthTriangle)triangles.get(j);
//                        if(t.contains(p))
//                        {
//                            triangles.remove(j);
//                            triangles.addAll(t.split(p));
//                            isContained=true;
//                            j=triangles.size();
//                        }
//                        
//
//                    }
//                    /*if(!isContained)
//                    {
//                        triangles.add(new DepthTriangle((DepthPoint)points.get(i-2),(DepthPoint)points.get(i-1),(DepthPoint)points.get(i)));
//                    }*/
//                
//            }
//        }
//        for(int j=0; j<triangles.size(); j++)
//        {
//            DepthTriangle t=(DepthTriangle)triangles.get(j);
//            if(t.hasVertex(d1)||t.hasVertex(d2)||t.hasVertex(d3)||t.hasVertex(d4))
//            {
//                triangles.remove(j);
//                j--;
//            }
//        }
        DepthMap map = new DepthMap(triangles);
        return map;

    }

    private static DepthTriangle createSuperTriangle(Point p1, Point p2) {
        double centerX = (p1.getX() + p2.getX()) / 2;
        double centerY = (p1.getY() + p2.getY()) / 2;
        Point center = new Point((int) centerX, (int) centerY);
        double rad = center.distance(p2);

        return createSuperTriangle(center, rad);
    }

    private static DepthTriangle createSuperTriangle(Point center, double rad) {
        double centerX = center.getX();
        double centerY = center.getY();

        DepthPoint v1 = new DepthPoint(new Point((int) centerX, (int) (centerY - 3 * rad)), 0);
        DepthPoint v2 = new DepthPoint(new Point((int) (centerX - 3 * rad), (int) (centerY + 3 * rad)), 0);
        DepthPoint v3 = new DepthPoint(new Point((int) (centerY + 3 * rad), (int) (centerY + 3 * rad)), 0);

        return new DepthTriangle(v1, v2, v3);

    }

    private static DepthTriangle createSuperTriangle(ArrayList<DepthObject> points) {
        double centerX = 0;
        double centerY = 0;

        for (int i = 0; i < points.size(); i++) {
            Point p = ((DepthPoint) points.get(i)).getP();
            centerX += p.getX();
            centerY += p.getY();
        }
        centerX /= points.size();
        centerY /= points.size();
        Point.Double center = new Point.Double(centerX, centerY);
        double maxDist = 0;
        for (int i = 0; i < points.size(); i++) {
            Point p = ((DepthPoint) points.get(i)).getP();
            double dist = center.distance(p);
            if (dist > maxDist) {
                maxDist = dist;
            }
        }
        DepthPoint v1 = new DepthPoint(new Point((int) centerX, (int) (centerY - 3 * maxDist)), 0);
        DepthPoint v2 = new DepthPoint(new Point((int) (centerX - 3 * maxDist), (int) (centerY + 3 * maxDist)), 0);
        DepthPoint v3 = new DepthPoint(new Point((int) (centerY + 3 * maxDist), (int) (centerY + 3 * maxDist)), 0);

        return new DepthTriangle(v1, v2, v3);

    }

    private int addPoint(DepthPoint p, int startIndex) {
        //System.out.println("-SI:"+startIndex);
        startIndex = 0;
        ArrayList<Edge> edges = new ArrayList<Edge>();
        int ret = startIndex;

        for (int i = startIndex; i < triangles.size(); i++) {
            //System.out.println(i);
            DepthTriangle t = (DepthTriangle) triangles.get(i);
            if (t.isToLeft(p.getP())) {
                ret = i;
            }

            if (t.withinCircumCircle(p.getP())) {

                ArrayList<Edge> temp = t.getEdges();
                for (int j = 0; j < temp.size(); j++) {
                    Edge e = temp.get(j);
                    if (e != null && e.getP1() != null && e.getP2() != null) {
                        int index = -1;
                        for (int k = 0; k < edges.size(); k++) {
                            if (edges.get(k).equals(e)) {
                                index = k;
                                k = edges.size();
                            }
                        }
                        if (index != -1) {
                            Edge removed = edges.remove(index);
//                                System.out.println("removed edge:"+removed.getP1().getP()+"====>"+removed.getP2().getP()+"   vs   "+e.getP1().getP()+"====>"+e.getP2().getP());

                        } else {
                            edges.add(e);
//                                System.out.println("added edge:"+e.getP1().getP()+"====>"+e.getP2().getP());
                        }

                    }

                }
//                    System.out.println("removed triangle "+i);
                triangles.remove(i);
                //System.out.println(triangles.size());
                i--;

            }

        }

        //System.out.println();
        for (int i = 0; i < edges.size(); i++) {
            Edge edge = edges.get(i);
            triangles.add(new DepthTriangle(edge.getP1(), edge.getP2(), p));
//            System.out.println("added:"+edge.getP1().getP()+"====>"+edge.getP2().getP()+"===>"+p.getP());
        }
//        System.out.println("size:"+triangles.size());
//        System.out.println("****************************************************************************************");
        return ret;

    }

    static class DPComparator implements Comparator {

        public int compare(Object t, Object t1) {
            DepthPoint p1 = (DepthPoint) t;
            DepthPoint p2 = (DepthPoint) t1;

            return (int) (p1.getP().getX() - p2.getP().getX());
        }
    }
}
