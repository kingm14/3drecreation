/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pkg3drecreation;

import java.awt.*;

/**
 *
 * @author kingm14
 */
public class BoundingPolygon extends Polygon{
    

    @Override
    public void addPoint(int i, int i1) {
        if(!this.contains(i, i1))
        {
            
            super.addPoint(i, i1); 
        }
        
    }
    public Point getVertex(int i)
    {
        return new Point(super.xpoints[i],super.ypoints[i]);
    }
    public int getNumVertices()
    {
        return super.xpoints.length;
    }
    
}
