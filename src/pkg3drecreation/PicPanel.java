/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pkg3drecreation;

import com.jhlabs.image.ScaleFilter;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.Polygon;
import java.awt.event.*;
import java.awt.geom.Rectangle2D;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JLabel;


/**
 *
 * @author kingm14
 */
public class PicPanel extends javax.swing.JPanel {

    /**
     * Creates new form PicPanel
     */
    private Point boxStart;
    private Point boxEnd;
    private BoundingPolygon poly;
    private DepthMap depthMap; 
    private boolean drawLabels=true;
    private boolean drawingPicture=true;
    private boolean drawingCorners=true;
    public PicPanel() {
        initComponents();
        boxStart=null;
        boxEnd=null;
        poly=null;
        
        //addMouseListener(this);
        
        
    }
    public JLabel getLabel()
    {
        return jLabel1;
    }
    private void drawCorners(Graphics imageGraphics, ArrayList<Corner> corners, double scale)
    {
        drawCorners(imageGraphics, corners,scale, null);
    }
    private void drawCorners(Graphics imageGraphics, ArrayList<Corner> corners, double scale, String search)
    {
        imageGraphics.setColor(Color.green);
        
        //System.out.println("painting corners"+corners.size());
        for (int cornerIndex = 0; cornerIndex < corners.size(); cornerIndex++) {
            //g.setColor(Color.green);
            //System.out.println("C.I.="+cornerIndex);
            Corner c = corners.get(cornerIndex);
            Point p = c.getLocation();
            if(drawingCorners)
            {
                if (c.getText().equals("")) {
                imageGraphics.fillRect((int) ((p.getY() * scale) - 2), ((int) (p.getX() * scale) - 2), 4, 4);
                }
            }
            //531268
            //585224
            

            int dir = c.getOrientation();
            if (dir > 18) {
                dir -= 36;
            }
            boolean isXPos = (Math.abs(dir) <= 9);
            boolean isYPos = dir >= 0;
            if (dir > 9) {
                dir = 18 - dir;
            } else if (dir < -9) {
                dir = (-18 - dir);
            }
            dir = Math.abs(dir);
            double inRads = Math.toRadians(dir * 10);
            double dirY = (Math.sin(inRads) * 5);
            double dirX = (Math.cos(inRads) * 5);
            if (dir == 9) {
                dirX = 0;
                dirY = 5;
            }
            if (!isXPos) {
                dirX *= -1;
            }
            if (!isYPos) {
                dirY *= -1;
            }
            //System.out.println("drawing line");
            //System.out.println(c.getOrientation()+"-->"+dir+" "+dirX+" "+dirY+" "+inRads);
            //System.out.println("X="+p.getX()+" Y="+p.getY()+" scale="+scale+" dirX="+dirX+" dirY="+dirY);
            if (cornerIndex == 30) {
                imageGraphics.drawLine((int) (p.getY() * scale), (int) (p.getX() * scale), (int) (p.getY() * scale) + (int) dirY, (int) (p.getX() * scale) + (int) dirX);
            }
            if(search!=null)
            {
                imageGraphics.setColor(Color.red);
                //System.out.println("drawing label");
                if (!c.getText().equals("")&&c.isPaintingText()) {
                    //System.out.println("has Label");
                    if(c.getText().equals(search))
                    {
                        if(drawLabels)
                            drawLabel(new Point((int) ((p.getY() * scale)), ((int) (p.getX() * scale))), c.getText(), imageGraphics, Color.red);
                    }
                    else
                    {
                        if(drawLabels)
                            drawLabel(new Point((int) ((p.getY() * scale)), ((int) (p.getX() * scale))), c.getText(), imageGraphics);
                    }
                }
            }
            else
            {
               if (!c.getText().equals("")) {
                    if(drawLabels)
                        drawLabel(new Point((int) ((p.getY() * scale)), ((int) (p.getX() * scale))), c.getText(), imageGraphics);
                } 
            }
            //imageGraphics.drawString(c.getText(), (int) ((p.getY() * scale)),  ((int) (p.getX() * scale)));
            imageGraphics.setColor(Color.green);
            //imageGraphics.fillRect(pic1Frame.getX() + (int) ((p.getY() * scale) - 2)+shiftX, pic1Frame.getY() + shiftY + ((int) (p.getX() * scale) - 2), 4, 4);

        }
    }
    private void drawBoundings(Graphics imageGraphics)
    {
        if(getBoxStart()!=null&&getBoxEnd()!=null)
        {
            imageGraphics.setColor(Color.orange);
            imageGraphics.drawRect((int)getBoxStart().getX(), (int)getBoxStart().getY(), (int)(getBoxEnd().getX()-getBoxStart().getX()), (int)(getBoxEnd().getY()-getBoxStart().getY()));
        }
        if(getPoly()!=null)
        {
            imageGraphics.setColor(Color.orange);
            imageGraphics.drawPolygon(getPoly());
            //Rectangle2D rect=getPoly().getBounds2D();
            //imageGraphics.drawRect((int)rect.getX(), (int)rect.getY(), (int)rect.getWidth(), (int)rect.getHeight());
        }
    }
    
    public void paintImage(BufferedImage image, ArrayList<Corner> corners, double scale) {
        
        
        BufferedImage tempImage=new BufferedImage((int)(image.getWidth()*scale),(int)(image.getHeight()*scale), image.getType());
        ScaleFilter sf=new ScaleFilter(tempImage.getWidth(),tempImage.getHeight());
        sf.filter(image, tempImage);
        Graphics2D imageGraphics;  //NEW!
        
        imageGraphics = (Graphics2D)tempImage.getGraphics();
        if(!drawingPicture)
        {
            imageGraphics.setColor(Color.green);
            imageGraphics.fillRect(0, 0, (int)(image.getWidth()*scale), (int)(image.getHeight()*scale));
        }
        drawCorners(imageGraphics,corners,scale);
        if(getDepthMap()!=null)
        {
            getDepthMap().paint(imageGraphics,scale);
        }
        drawBoundings(imageGraphics);
        //imageGraphics.drawImage(image, 0, 0, getLabel());
        
        /*if (img.getWidth() < pic1Frame.getWidth()) {
         pic1Frame.setSize(img.getWidth(), pic1Frame.getHeight());
         }
         if (img.getHeight() < pic1Frame.getHeight()) {
         pic1Frame.setSize(pic1Frame.getWidth(), img.getHeight());
         }*/
        //g.drawImage(img, pic1Frame.getX()+shiftX, pic1Frame.getY() + shiftY, /*pic1Frame.getX() + pic1Frame.getWidth(), pic1Frame.getY() + pic1Frame.getHeight(),*/ this);
        
        
        
        
        //System.out.println("done painting corners");
        
        Icon icon = new ImageIcon(tempImage);
        //pic1Label = new JLabel(icon);
        jLabel1.setIcon(icon);

    }
    public void paintImage(BufferedImage image, ArrayList<Corner> corners, double scale, String search) {
        
        
        BufferedImage tempImage=new BufferedImage((int)(image.getWidth()*scale),(int)(image.getHeight()*scale), image.getType());
        ScaleFilter sf=new ScaleFilter(tempImage.getWidth(),tempImage.getHeight());
        sf.filter(image, tempImage);
        Graphics2D imageGraphics;  //NEW!
        
        imageGraphics = (Graphics2D)tempImage.getGraphics();
        if(!drawingPicture)
        {
            imageGraphics.setColor(Color.DARK_GRAY);
            imageGraphics.fillRect(0, 0, image.getWidth(), image.getHeight());
        }
        //imageGraphics.drawImage(image, 0, 0, getLabel());
        
        /*if (img.getWidth() < pic1Frame.getWidth()) {
         pic1Frame.setSize(img.getWidth(), pic1Frame.getHeight());
         }
         if (img.getHeight() < pic1Frame.getHeight()) {
         pic1Frame.setSize(pic1Frame.getWidth(), img.getHeight());
         }*/
        //g.drawImage(img, pic1Frame.getX()+shiftX, pic1Frame.getY() + shiftY, /*pic1Frame.getX() + pic1Frame.getWidth(), pic1Frame.getY() + pic1Frame.getHeight(),*/ this);
        drawCorners(imageGraphics, corners,scale,search);
        if(getDepthMap()!=null)
        {
            getDepthMap().paint(imageGraphics,scale);
        }
        drawBoundings(imageGraphics);
        
        //else 
        
        
        //System.out.println("done painting corners");
        
        Icon icon = new ImageIcon(tempImage);
        //pic1Label = new JLabel(icon);
        jLabel1.setIcon(icon);

    }
    public static void drawLabel(Point p, String text, Graphics g) {
        //System.out.println(g.getFont().getSize());
        
        g.setFont(new Font(g.getFont().getName(), Font.PLAIN, 10));
        g.setColor(Color.gray);
        g.fill3DRect((int) (p.getX()), ((int) (p.getY())) - g.getFontMetrics().getHeight(), g.getFontMetrics().stringWidth(text) + 2, g.getFontMetrics().getHeight() + 2, true);
        g.setColor(Color.blue);
        g.drawString("" + text, (int) (p.getX() + 1), ((int) (p.getY()) - 1));
    }
    public static void drawLabel(Point p, String text, Graphics g, Color c) {
        //System.out.println(g.getFont().getSize());
        g.setFont(new Font(g.getFont().getName(), Font.PLAIN, 10));
        g.setColor(c);
        g.fill3DRect((int) (p.getX()), ((int) (p.getY())) - g.getFontMetrics().getHeight(), g.getFontMetrics().stringWidth(text) + 2, g.getFontMetrics().getHeight() + 2, true);
        g.setColor(Color.blue);
        g.drawString("" + text, (int) (p.getX() + 1), ((int) (p.getY()) - 1));
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane1 = new javax.swing.JScrollPane();
        jLabel1 = new javax.swing.JLabel();

        jLabel1.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel1.setVerticalAlignment(javax.swing.SwingConstants.TOP);
        jScrollPane1.setViewportView(jLabel1);

        org.jdesktop.layout.GroupLayout layout = new org.jdesktop.layout.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(jScrollPane1, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 400, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(jScrollPane1, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 300, Short.MAX_VALUE)
        );
    }// </editor-fold>//GEN-END:initComponents
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel jLabel1;
    private javax.swing.JScrollPane jScrollPane1;
    // End of variables declaration//GEN-END:variables

    /**
     * @return the boxStart
     */
    public Point getBoxStart() {
        return boxStart;
    }

    /**
     * @param boxStart the boxStart to set
     */
    public void setBoxStart(Point boxStart) {
        this.boxStart = boxStart;
    }

    /**
     * @return the boxEnd
     */
    public Point getBoxEnd() {
        return boxEnd;
    }

    /**
     * @param boxEnd the boxEnd to set
     */
    public void setBoxEnd(Point boxEnd) {
        this.boxEnd = boxEnd;
    }

    /**
     * @return the poly
     */
    public BoundingPolygon getPoly() {
        return poly;
    }

    /**
     * @param poly the poly to set
     */
    public void setPoly(BoundingPolygon poly) {
        this.poly = poly;
    }

    /**
     * @return the depthMap
     */
    public DepthMap getDepthMap() {
        return depthMap;
    }

    /**
     * @param depthMap the depthMap to set
     */
    public void setDepthMap(DepthMap depthMap) {
        this.depthMap = depthMap;
    }

    /**
     * @return the shouldDrawLabels
     */
    public boolean isShouldDrawLabels() {
        return drawLabels;
    }

    /**
     * @param shouldDrawLabels the shouldDrawLabels to set
     */
    public void setShouldDrawLabels(boolean shouldDrawLabels) {
        this.drawLabels = shouldDrawLabels;
    }

    /**
     * @return the drawingPicture
     */
    public boolean isDrawingPicture() {
        return drawingPicture;
    }

    /**
     * @param drawingPicture the drawingPicture to set
     */
    public void setDrawingPicture(boolean drawingPicture) {
        this.drawingPicture = drawingPicture;
    }

    
}
