/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pkg3drecreation;

/**
 *
 * @author kingm14
 */
public class Gradient {
    private double gx;
    private double gy;
    
    public Gradient(double gx, double gy)
    {
        this.gx=gx;
        this.gy=gy;
    }

    /**
     * @return the gx
     */
    public double getGx() {
        return gx;
    }

    /**
     * @param gx the gx to set
     */
    public void setGx(double gx) {
        this.gx = gx;
    }

    /**
     * @return the gy
     */
    public double getGy() {
        return gy;
    }

    /**
     * @param gy the gy to set
     */
    public void setGy(double gy) {
        this.gy = gy;
    }

    /**
     * @return the theta
     */
    public double getTheta() {
        
        return ((2*Math.PI)+Math.atan2(gx,gy))%(2*Math.PI);
    }
    public double getMagnitude()
    {
        return Math.sqrt(gx*gx+gy*gy );
    }
    
}
