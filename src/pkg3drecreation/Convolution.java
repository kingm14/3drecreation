/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pkg3drecreation;

/**
 *
 * @author kingm14
 */
import java.applet.*;
import java.awt.*;
import java.awt.image.*;
import java.net.*;
import java.util.*;
import java.io.*;
import java.lang.Math.*;
import java.awt.Color.*;

/**
 * Convolution is the code for applying the convolution operator.
 *
 * @author: Simon Horne
 */
public class Convolution extends Thread {

    /**
     * Default no-arg constructor.
     */
    public Convolution() {
    }

    /**
     * Takes an image (grey-levels) and a kernel and a position, applies the
     * convolution at that position and returns the new pixel value.
     *
     * @param input The 2D double array representing the image.
     * @param x The x coordinate for the position of the convolution.
     * @param y The y coordinate for the position of the convolution.
     * @param k The 2D array representing the kernel.
     * @param kernelWidth The width of the kernel.
     * @param kernelHeight The height of the kernel.
     * @return The new pixel value after the convolution.
     */
    public static double singlePixelConvolution(double[][] input,
            int x, int y,
            double[][] k,
            int kernelWidth,
            int kernelHeight) {
        double output = 0;
        for (int i = 0; i < kernelWidth; ++i) {
            for (int j = 0; j < kernelHeight; ++j) {
                output = output + (input[x + i][y + j] * k[i][j]);
            }
        }
        return output;
    }

    public static int applyConvolution(int[][] input,
            int x, int y,
            double[][] k,
            int kernelWidth,
            int kernelHeight) {
        int output = 0;
        for (int i = 0; i < kernelWidth; ++i) {
            for (int j = 0; j < kernelHeight; ++j) {
                output = output + (int) Math.round(input[x + i][y + j] * k[i][j]);
            }
        }
        return output;
    }

    /**
     * Takes a 2D array of grey-levels and a kernel and applies the convolution
     * over the area of the image specified by width and height.
     *
     * @param input the 2D double array representing the image
     * @param width the width of the image
     * @param height the height of the image
     * @param kernel the 2D array representing the kernel
     * @param kernelWidth the width of the kernel
     * @param kernelHeight the height of the kernel
     * @return the 2D array representing the new image
     */
    public static double[][] convolution2D(double[][] input,
            int width, int height,
            double[][] kernel,
            int kernelWidth,
            int kernelHeight) {
        int smallWidth = width - kernelWidth + 1;
        int smallHeight = height - kernelHeight + 1;
        double[][] output = new double[smallWidth][smallHeight];
        for (int i = 0; i < smallWidth; ++i) {
            for (int j = 0; j < smallHeight; ++j) {
                output[i][j] = 0;
            }
        }
        for (int i = 0; i < smallWidth; ++i) {
            for (int j = 0; j < smallHeight; ++j) {
                output[i][j] = singlePixelConvolution(input, i, j, kernel,
                        kernelWidth, kernelHeight);
//if (i==32- kernelWidth + 1 && j==100- kernelHeight + 1) System.out.println("Convolve2D: "+output[i][j]);
            }
        }
        return output;
    }
    public static double[][] convolution2D(RGB[][] input,
            int width, int height,
            double[][] kernel,
            int kernelWidth,
            int kernelHeight) {
        int smallWidth = width - kernelWidth + 1;
        int smallHeight = height - kernelHeight + 1;
        double[][] output = new double[smallWidth][smallHeight];
        for (int i = 0; i < smallWidth; ++i) {
            for (int j = 0; j < smallHeight; ++j) {
                output[i][j] = 0;
            }
        }
        for (int i = 0; i < smallWidth; ++i) {
            for (int j = 0; j < smallHeight; ++j) {
                output[i][j] = singlePixelConvolution(input, i, j, kernel,
                        kernelWidth, kernelHeight);
//if (i==32- kernelWidth + 1 && j==100- kernelHeight + 1) System.out.println("Convolve2D: "+output[i][j]);
            }
        }
        return output;
    }
    public static double singlePixelConvolution(RGB[][] input,
            int x, int y,
            double[][] k,
            int kernelWidth,
            int kernelHeight) {
        double output = 0;
        for (int i = 0; i < kernelWidth; ++i) {
            for (int j = 0; j < kernelHeight; ++j) {
                output = output + (input[x + i][y + j].toGray() * k[i][j]);
            }
        }
        return output;
    }
    public static double[][] convolution2DX(GradientMap input,
            int width, int height,
            double[][] kernel,
            int kernelWidth,
            int kernelHeight) {
        double[][] temp=new double[width][height];
        for(int i=0; i<width; i++)
        {
            for(int j=0; j<height; j++)
            {
                temp[i][j]=input.getGradientAt(i, j).getGx();
            }
        }
        return convolution2D(temp,width, height, kernel,kernelWidth,kernelHeight);
    }
    public static double[][] convolution2DY(GradientMap input,
            int width, int height,
            double[][] kernel,
            int kernelWidth,
            int kernelHeight) {
        double[][] temp=new double[width][height];
        for(int i=0; i<width; i++)
        {
            for(int j=0; j<height; j++)
            {
                temp[i][j]=input.getGradientAt(i, j).getGy();
            }
        }
        return convolution2D(temp,width, height, kernel,kernelWidth,kernelHeight);
    }
    public static double singlePixelConvolutionX(GradientMap input,
            int x, int y,
            double[][] k,
            int kernelWidth,
            int kernelHeight) {
        double output = 0;
        for (int i = 0; i < kernelWidth; ++i) {
            for (int j = 0; j < kernelHeight; ++j) {
                output = output + (input.getGradientAt(x+i,y+j).getGx() * k[i][j]);
            }
        }
        return output;
    }
    public static double singlePixelConvolutionY(GradientMap input,
            int x, int y,
            double[][] k,
            int kernelWidth,
            int kernelHeight) {
        double output = 0;
        for (int i = 0; i < kernelWidth; ++i) {
            for (int j = 0; j < kernelHeight; ++j) {
                output = output + (input.getGradientAt(x+i,y+j).getGy() * k[i][j]);
            }
        }
        return output;
    }
    public static Gradient singlePixelConvolution(GradientMap input,
            int x, int y,
            double[][] k,
            int kernelWidth,
            int kernelHeight) {
        double outputX = 0;
        double outputY = 0;
        for (int i = 0; i < kernelWidth; ++i) {
            for (int j = 0; j < kernelHeight; ++j) {
                outputX = outputX + (input.getGradientAt(x+i,y+j).getGx() * k[i][j]);
                outputY = outputY + (input.getGradientAt(x+i,y+j).getGx() * k[i][j]);
            }
        }
        return new Gradient(outputX,outputY);
    }
    public static double[] decompose(double[][] matrix)
    {
        double[] ret=new double[2];
        double a=matrix[0][0];
        double b=matrix[1][0];
        double c=matrix[0][1];
        double d=matrix[1][1];
        //double T=a+d;
        //double D=a*d-b*c;
        
        double p2=Math.pow((a*a)-(2*a*d)+(4*b*c)+(d*d),.5);
        ret[0]=(p2+a+d)/2;
        ret[1]=((-1*p2)+a+d)/2;
        if(ret[0]!=0||ret[1]!=0)
        {
            System.err.println(ret[0]+"    "+ret[1]);
        }
        return ret;
        
    }
}
