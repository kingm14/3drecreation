/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pkg3drecreation;

import java.util.*;
import java.awt.*;
import java.awt.image.*;
import com.jhlabs.image.*;
import java.math.BigDecimal;
import java.math.MathContext;
import java.math.RoundingMode;

/**
 *
 * @author kingm14
 */
public class CornerDetector {

    private ArrayList<Corner> corners;
    private double threshold = 1;
    private int windowSize;
    private int maxShift;

    public CornerDetector() {

        corners = new ArrayList<Corner>();
        windowSize = 1;
        maxShift = 2;
    }

    public BufferedImage getGrayscale(BufferedImage image) {
        GrayscaleFilter filter = new GrayscaleFilter();
        return filter.filter(image, null);
    }

    private static RGB[][] convertTo2DWithoutUsingGetRGB(BufferedImage image) {

        final byte[] pixels = ((DataBufferByte) image.getRaster().getDataBuffer()).getData();
        final int width = image.getWidth();
        final int height = image.getHeight();
        //System.out.println(width+","+height);
        final boolean hasAlphaChannel = image.getAlphaRaster() != null;

        RGB[][] result = new RGB[height][width];
        if (hasAlphaChannel) {
            System.out.println("has alpha");
            final int pixelLength = 4;
            for (int pixel = 0, row = 0, col = 0; pixel < pixels.length; pixel += pixelLength) {
                int argb = 0;
                argb += (((int) pixels[pixel] & 0xff) << 24); // alpha
                argb += ((int) pixels[pixel + 1] & 0xff); // blue
                argb += (((int) pixels[pixel + 2] & 0xff) << 8); // green
                argb += (((int) pixels[pixel + 3] & 0xff) << 16); // red
                RGB rgb = new RGB((((int) pixels[pixel + 3] & 0xff)), (((int) pixels[pixel + 2] & 0xff)), ((int) pixels[pixel + 1] & 0xff));
                result[row][col] = rgb;

                col++;
                if (col == width) {
                    col = 0;
                    row++;
                }
            }
        } else {
            final int pixelLength = 3;
            for (int pixel = 0, row = 0, col = 0; pixel < pixels.length - 2; pixel += pixelLength) {
                /*int argb = 0;
                 argb += -16777216; // 255 alpha
                 argb += ((int) pixels[pixel] & 0xff); // blue
                 argb += (((int) pixels[pixel + 1] & 0xff) << 8); // green
                 argb += (((int) pixels[pixel + 2] & 0xff) << 16); // red*/
                RGB rgb = new RGB((((int) pixels[pixel + 2] & 0xff)), (((int) pixels[pixel + 1] & 0xff)), ((int) pixels[pixel] & 0xff));
                result[row][col] = rgb;

                col++;
                if (col == width) {
                    col = 0;
                    row++;
                }
            }
        }




        return result;
    }

    public void applyCornerOperator() {
    }

    public void getCornernessMap() {
    }

    public void nonMaximalSuppression() {
    }

    public static void printList(RGB[][] map) {
        for (int i = 0; i < map.length; i++) {
            for (int j = 0; j < map[i].length; j++) {
                System.out.print(map[i][j] + " ");
            }
            System.out.println();
        }
    }

    public RGB[][] getBlockMap(RGB[][] map, int size) {
        RGB[][] ret = new RGB[(map.length / size) + 1][(map[0].length / size) + 1];
        int x = 0;
        int y = 0;
        for (int i = 0; i < ret.length; i++) {
            for (int j = 0; j < ret[0].length; j++) {
                ArrayList<RGB> temp = new ArrayList<RGB>();
                for (int r = 0; r < size; r++) {
                    for (int c = 0; c < size; c++) {
                        if ((i + r < map.length && j + c < map[0].length)) {
                            temp.add(map[i * size + r][j * size + c]);
                        }
                    }
                }
                //System.out.println(i/size+","+j/size);
                ret[i][j] = RGB.average(temp);

            }
        }
        return ret;
    }

    public void setThreshold(double i) {
        threshold = i;
    }

    public ArrayList<Corner> getCorners() {
        return corners;
    }

    public void printList(Pixel[][] map, int threshold) {
        //System.out.println("max:"+ (1+(int)Math.pow(max,1/10)));
        double maxVal = 0;
        for (int i = 0; i < map.length; i++) {
            for (int j = 0; j < map[0].length; j++) {
                if (map[i][j] != null && map[i][j].getSSD() > maxVal) {
                    maxVal = map[i][j].getSSD();
                }
            }
        }
        //String s=(int)maxVal+" ";
        //System.out.println("max:"+maxVal+" " + (1 + (int) Math.pow(maxVal, 1.0 / 10)));
        for (int i = 0; i < map.length; i++) {

            for (int j = 0; j < map[i].length; j++) {
                if (map[i][j] != null) {
                    //System.out.print(map[i][j].getLine());
                    if (map[i][j].isCorner(threshold)) {
                        //corners.add(new Point(j,i));
                        System.out.print("#");//Pixel.insertSpaces(map[i][j].getSSD(), s.length()));
                    } else {
                        //for (int num = 0; num < s.length(); num++) {
                        System.out.print(" ");
                        //}
                    }

                }
            }
            System.out.println();
            /*for (int j = 0; j < map[i].length; j++) {
             if (map[i][j] != null) {
             System.out.print(map[i][j].getFirstLine());
             }
             }
             System.out.println();
             for (int j = 0; j < map[i].length; j++) {
             if (map[i][j] != null) {
             System.out.print(map[i][j].getSecondLine());
             }
             }
             System.out.println();
             for (int j = 0; j < map[i].length; j++) {
             if (map[i][j] != null) {
             System.out.print(map[i][j].getThirdLine());
             }
             }
             System.out.println();
             for (int j = 0; j < map[i].length; j++) {
             if (map[i][j] != null) {
             System.out.print(map[i][j].getLine());
             }
             }
             System.out.println();*/

        }
    }

    public double[][] createPartialIntensityMap(RGB[][] map, int x, int y, int rad) {
        double[][] ret = new double[rad * 2 + 1][rad * 2 + 1];
        for (int r = 0; r < ret.length; r++) {
            for (int c = 0; c < ret[0].length; c++) {
                if (map[x + (r - rad)][y + c - rad] != null) {
                    ret[r][c] = map[x + (r - rad)][y + c - rad].getIntensity();
                }
            }
        }
        return ret;
    }

    public ArrayList<Corner> getLine(double[][] cMap, int x, int y, int maxShift,HashMap<Point, Corner> posToCornerMap,double thresh) {
        ArrayList<Corner>ret=new ArrayList<Corner>();
        getLineRecursive(cMap,x,y,maxShift,ret,posToCornerMap,thresh);
        //System.out.println(ret.size());
        return ret;
    }
    private void getLineRecursive(double[][] cMap, int x, int y, int maxShift, ArrayList<Corner> other,HashMap<Point, Corner> posToCornerMap, double thresh) {
        
        double cness=cMap[x][y];
        if(other.contains(posToCornerMap.get(new Point(x,y))))
        {
            return;
        }
        other.add(posToCornerMap.get(new Point(x,y)));
        for (int wX = -1 * maxShift; wX <= maxShift; wX++) {
            for (int wY = -1 * maxShift; wY <= maxShift; wY++) {

                if ((x + wX >= 0 && x + wX< cMap.length && y + wY>= 0 && y + wY < cMap[0].length)) {

                    //if (isCorner[x + wX][y + wY]) {
                    if (!(wX == 0 && wY == 0)) {
                        if(posToCornerMap.containsKey(new Point(x+wX, y+wY))&&cMap[x+wX][y+wY]-cness<=thresh)
                        {
                            
                            
                                
                            getLineRecursive(cMap,x+wX,y+wY,maxShift,other,posToCornerMap,thresh);
                            
                            
                        }
                    }
                }
            }
        }
        
    }
    

    public void createCornerList(RGB[][] map, double threshold, int maxShift, int windowRad) {
        int windowSize = 2 * windowRad + 1;
        
        
        double[][] kX = new double[][]{{5, 0, -5},
                                       {8, 0, -8},
                                       {5, 0, -5}};
        double[][] kY = new double[][]{{5, 8, 5},
                                       {0, 0, 0},
                                       {-5, -8, -5}};
        double[][] gradX=Convolution.convolution2D(map, map.length, map[0].length, kX, kX.length, kX[0].length);
        double[][] gradY=Convolution.convolution2D(map, map.length, map[0].length, kY, kY.length, kY[0].length);
        GradientMap grads = new GradientMap(gradX,gradY);
        //gradX=null;
        //gradY=null;
        //System.out.println("gradX:" + gradX.length + "x" + gradX[0].length + "    gradY:" + gradY.length + "x" + gradY[0].length);
        double sigma = 1.414;
        //double gauss1 = 1 / (2 * Math.PI * sigma * sigma);
        //double sigma2 = sigma * sigma;
        
        int minNum = 7;//(int)(Math.pow(maxShift*2+1,2)-(2));
        
        //ArrayList<Double> difsList = new ArrayList<Double>();
        int methodType = 0;
        if (methodType == 0) {
            
            //ArrayList<Double> cornernessList = new ArrayList<Double>();
            for (int x = 1 + windowRad; x < map.length - 1 - windowRad; x++) {
                for (int y = 1 + windowRad; y < map[0].length - 1 - windowRad; y++) {

                    double[][] A = grads.getHarrisMatrix( x - windowRad - 1, y - windowRad - 1, windowSize, windowSize);
                    //0.04<=k<=0.06
                    double k = 0.06;
                    double cornerness =Matrix.det(A)/1000000 - k*Math.pow(Matrix.trace(A),2)/1000000;

                    //System.out.println("2*("+Matrix.det(A)+"/"+"("+Matrix.trace(A)+"+"+e+"))"+"="+cornerness);
                    //if(cornerness!=0)
                /*if(d!=0)
                     {
                     System.out.print("a:"+a+" bc:"+bc+" d:"+d);
                     if((a!=0&&d!=0)||bc!=0)
                     {
                     System.err.print("true");
                     }
                     System.out.println();
                     System.out.println("2*("+Matrix.det(A)+"/"+"("+Matrix.trace(A)+"+"+e+"))"+"="+cornerness);
                     System.out.println();
                    
                     }*/
                    map[x][y].setCornerness(cornerness);
                    
                    if (cornerness > threshold) {
                        

                        


                        Corner c = new Corner(x, y, this.getRGBAt(x, y));
                        c.setCornerness(cornerness);

                        corners.add(c);
                        



                        //System.out.println(cornerness);
                    }
                    

                }
            }
            ArrayList<Corner> corners2 = new ArrayList<Corner>();
            for (Corner c : corners) {
                int x=c.getX();
                int y=c.getY();
                double cness=c.getCornerness();
                boolean isMax=true;
                for(int i=-maxShift; i<=maxShift; i++)
                {
                    for(int j=-maxShift; j<=maxShift; j++)
                    {
                        if ((x + i - 1 >= 0 && x + i - 1 < map.length && y + j - 1 >= 0 && y + j - 1 < map[0].length)) {
                            if(map[x+i-1][y+j-1].getCornerness()>cness)
                            {
                                isMax=false;
                                j=10;
                                i=10;
                            }
                        }
                    }
                }
                if(isMax)
                {
                    corners2.add(c);
                }
                
            }
                corners=corners2;
                //corners=refineSubPixel(corners,grads,1,0,2);
            //Collections.sort(corners, new cornernessComparator());
            
            
            /*Collections.sort(corners, new cornernessComparator());
            //while (corners.size() > 0) {
           
            for (Corner c : corners) {
                
                //corners2.add(c);
                int x = c.getX();
                int y = c.getY();
                double cornerness = c.getCornerness();
                ArrayList<Corner> overlapping = new ArrayList<Corner>();

                for (int wX = -1 * maxShift; wX <= maxShift; wX++) {
                    for (int wY = -1 * maxShift; wY <= maxShift; wY++) {

                        if ((x + wX - 1 >= 0 && x + wX - 1 < cornernessMap.length && y + wY - 1 >= 0 && y + wY - 1 < cornernessMap[0].length)) {

                            //if (isCorner[x + wX][y + wY]) {
                            if (!(wX == 0 && wY == 0)) {
                                if (cornernessMap[x + wX - 1][y + wY - 1] < cornerness) {
                                    //ArrayList<Corner> remove=getLine(cornernessMap,x+ wX - 1,y+ wY - 1,maxShift,posToCornerMap, threshold/2);
                                    
                                    //corners2.removeAll(remove);
                                    corners2.remove(posToCornerMap.get(new Point(x+ wX - 1,y+ wY - 1)));
                                    
                                    //posToCornerMap.remove(new Point(x, y));
                                }
                                else if (cornernessMap[x + wX - 1][y + wY - 1] > cornerness) {
                                    //ArrayList<Corner> remove=getLine(cornernessMap,x,y,maxShift,posToCornerMap, threshold/2);
                                    
                                    //corners2.removeAll(remove);
                                    corners2.remove(posToCornerMap.get(new Point(x,y)));
                                    
                                   
                                }
                                
                                
                               

                            }

                            

                        }

                    }
                
                }

                //corners2.removeAll(overlapping);
            }
            

            corners = corners2;*/
            //Sift Naming
            corners2 = new ArrayList<Corner>();
            for (Corner corner : corners) {
                int x = corner.getX();
                int y = corner.getY();
                double[] oriHist = grads.createHistogramOfGradOri(x - windowRad - 1, y - windowRad - 1, windowSize, windowSize,36);
                double max = oriHist[0];
                int index = 0;
                for (int i = 1; i < oriHist.length; i++) {
                    if (oriHist[i] > max) {
                        max = oriHist[i];
                        index = i;
                    }
                }
                try{
                corner.setDirection(Math.toDegrees(grads.getGradientAt(x, y).getTheta()));
                }catch(java.lang.NullPointerException e)
                {
                    System.out.println(x+","+y);
                }
                corner.setOrientation(index);
                SiftName name1=new SiftName();
                name1.createSiftName(grads, x, y, corner.getDirection());
                corner.setName(name1);
                for (int i = 0; i < oriHist.length; i++) {
                    if (oriHist[i] / max >= .8) {
                        //System.out.print(i);
                        if (i != index) {
                            //System.out.print("  new");
                            Corner c = new Corner(x, y, this.getRGBAt(x, y));
                            c.setCornerness(corner.getCornerness());
                            c.setOrientation(i);
                            c.setDirection(corner.getDirection());
                            SiftName name=new SiftName();
                            name.createSiftName(grads, x, y, c.getDirection());
                            c.setName(name);

                            corners2.add(c);
                        }
                        //System.out.println();
                    }

                }
            }
            corners.addAll(corners2);
            
            //System.out.println(corners.size()+" vs "+corners2.size());
            /*Collections.sort(cornernessList);
             Collections.reverse(cornernessList);
             System.out.println("----------------------------------");
             int stop=50;
             if(cornernessList.size()<stop)
             {
             stop=cornernessList.size();
             }
             for(int i=0; i<50;i++)
             {
             System.out.println(cornernessList.get(i));
             }
             System.out.println("----------------------------------");*/
        } else {
            for (int x = windowRad + maxShift; x < map.length - windowRad - maxShift; x++) {
                for (int y = windowRad + maxShift; y < map[0].length - windowRad - maxShift; y++) {
                    int sum = 0;
                    /*double cur = new Window(createPartialIntensityMap(map, x, y, windowRad)).getWindowIntensity();
                     for (int u = -1 * maxShift; u <= maxShift; u += maxShift) {
                     int u2 = u * u;
                     for (int v = -1 * maxShift; v <= maxShift; v += maxShift) {
                     int v2 = v * v;
                     double curDif = calculateIntensityChange(map, x, y, u, v, windowRad, cur);
                     double gauss2 = Math.exp((-1 * (u2 + v2)) / (2 * sigma2));
                     //sum+=curDif*gauss1*gauss2;
                     if (curDif >= threshold) {
                     sum++;
                     }
                     }
                     }*/
                    if (sum >= minNum) {
                        
                        Corner c = new Corner(x, y, this.getRGBAt(x, y));
                        corners.add(c);
                        
                    }
                    

                    /*
                     //for(int u=-1*maxShift; u<=maxShift; u++)
                     double prevX=calculateIntensityChange(map, x, y, (-1*maxShift), 0, windowRad);
                     double prevY=calculateIntensityChange(map, x, y,0, (-1*maxShift),  windowRad);
                     double nextX=calculateIntensityChange(map, x, y, (1*maxShift), 0, windowRad);
                     double nextY=calculateIntensityChange(map, x, y,  0,(1*maxShift), windowRad);
                     sumX=prevX+nextX;
                     sumY=prevY+nextY;
                     if(sumX>=threshold&&sumY>=threshold)
                     {
                     Corner c=new Corner(x,y,this.getRGBAt(x, y));
                     corners.add(c);
                     }*/

                    //double cur=new Window(createPartialIntensityMap(map,x,y,windowRad)).getWindowIntensity();

                    /*if(x>=windowRad+maxShift)
                     {
                     prevX=new Window(createPartialIntensityMap(map,x-maxShift,y,windowRad)).getWindowIntensity();
                     }
                     if(y>=windowRad+maxShift)
                     {
                     prevY=new Window(createPartialIntensityMap(map,x,y-maxShift,windowRad)).getWindowIntensity();
                     }
                     if(x<map.length-windowRad-maxShift)
                     {
                     nextX=new Window(createPartialIntensityMap(map,x+maxShift,y,windowRad)).getWindowIntensity();
                     }
                     if(y<map[0].length-windowRad-maxShift)
                     {
                     nextY=new Window(createPartialIntensityMap(map,x,y+maxShift,windowRad)).getWindowIntensity();
                     }
                     sumX+=Math.pow(cur, cur)*/
                }
            }
        }
        /*ArrayList<ConnectedPoints> conecteds = groupAdjacent(isCorner, true);
         ArrayList<Corner> corners2 = new ArrayList<Corner>();
         RGB trash = new RGB(0, 0, 0);
         for (int i = 0; i < conecteds.size(); i++) {
         ConnectedPoints curList = conecteds.get(i);
            
         corners2.addAll(curList.groupPoints());
         }
         corners = corners2;*/

        //Collections.sort(sumsList);
        //boolean[][] isCorner2=new boolean[isCorner.length][isCorner[0].length];
        /*ArrayList<Corner> corners2=new ArrayList<Corner>();
         for(int i=0; i<corners.size(); i++)
         {
         Corner corn=corners.get(i);
         int cX=corn.getX();
         int cY=corn.getY();
         for(int r=-1; r<=1; r++)
         {
         for(int c=-1; c<=1; c++)
         {
                    
         RGB temp=map[cX+r][cY+c];
         if(Math.abs(temp.getIntensity()-corn.getColor().getIntensity())<threshold)
         {
         corners2.add(corn);
         corners.remove(i);
         r=100;
         c=100;
         }
                   
         }
         }
         }
         corners=corners2;*/
        /*for(int i=0; i<corners.size(); i++)
         {
         Corner corn=corners.get(i);
         int cX=corn.getX();
         int cY=corn.getY();
         int numSur=0;
         for(int r=-1;r<=1; r++)
         {
         for(int c=-1; c<=1; c++)
         {
         if(!(r==0&&c==0))
         {
         if(isCorner[cX+r][cY+c])
         {
         numSur++;
                            
         }
         }
         //isSurrounded=isSurrounded&&isCorner[cX+r][cY+c];
         }
         }
         //System.out.println(numSur);
         if(numSur>3)
         {
         //System.out.println("surrounded");
         //isCorner2[cX][cY]=false;
         corners.remove(i);
         i--;
         }

            
         }*/

        /*for(int i=0; i<corners.size(); i++)
         {
         Corner corn=corners.get(i);
         int cX=corn.getX();
         int cY=corn.getY();
         int numSur=0;
         for(int r=-1;r<=1; r++)
         {
         for(int c=-1; c<=1; c++)
         {
         if(!(r==0&&c==0))
         {
         if(isCorner2[cX+r][cY+c])
         {
         numSur++;
                            
         }
         }
         //isSurrounded=isSurrounded&&isCorner[cX+r][cY+c];
         }
         }
         System.out.println(numSur);
         if(numSur>1)
         {
         //System.out.println("surrounded");
         corners.remove(i);
         i--;
         }
         }*/
        /*int max = Collections.max(sumsList);
         int min = Collections.min(sumsList);
         System.out.println((int) (Math.pow(maxShift * 2 + 1, 2)) + "vs" + minNum);
         System.out.print("Sums| ");
         for (int i = min; i < max; i++) {
         System.out.print("(" + i + ":" + Collections.frequency(sumsList, i) + ")");
         }
         System.out.println();*/
        //Collections.

        /*for (int i = 0; i < map.length; i++) {

         for (int j = 0; j < map[i].length; j++) {
         if (map[i][j] != null) {
         //System.out.print(map[i][j].getLine());
         if (map[i][j].isCorner(threshold)) {
         Corner c=new Corner(i,j,this.getRGBAt(i, j));
         corners.add(c);
         //corners.add(new Point(j,i));
                        
         } 

         }
         }
            
         }*/
    }
    public void createCornerListRefine(RGB[][] map, double threshold, int maxShift, int windowRad, int refineIters) {
        int windowSize = 2 * windowRad + 1;
        
        
        double[][] kX = new double[][]{{5, 0, -5},
                                       {8, 0, -8},
                                       {5, 0, -5}};
        double[][] kY = new double[][]{{5, 8, 5},
                                       {0, 0, 0},
                                       {-5, -8, -5}};
        double[][] gradX=Convolution.convolution2D(map, map.length, map[0].length, kX, kX.length, kX[0].length);
        double[][] gradY=Convolution.convolution2D(map, map.length, map[0].length, kY, kY.length, kY[0].length);
        GradientMap grads = new GradientMap(gradX,gradY);
        //gradX=null;
        //gradY=null;
        //System.out.println("gradX:" + gradX.length + "x" + gradX[0].length + "    gradY:" + gradY.length + "x" + gradY[0].length);
        double sigma = 1.414;
        //double gauss1 = 1 / (2 * Math.PI * sigma * sigma);
        //double sigma2 = sigma * sigma;
        
        int minNum = 7;//(int)(Math.pow(maxShift*2+1,2)-(2));
        
        //ArrayList<Double> difsList = new ArrayList<Double>();
        int methodType = 0;
        if (methodType == 0) {
            
            //ArrayList<Double> cornernessList = new ArrayList<Double>();
            for (int x = 1 + windowRad; x < map.length - 1 - windowRad; x++) {
                for (int y = 1 + windowRad; y < map[0].length - 1 - windowRad; y++) {

                    double[][] A = grads.getHarrisMatrix( x - windowRad - 1, y - windowRad - 1, windowSize, windowSize);
                    //0.04<=k<=0.06
                    double k = 0.06;
                    double cornerness =Matrix.det(A)/1000000 - k*Math.pow(Matrix.trace(A),2)/1000000;

                    //System.out.println("2*("+Matrix.det(A)+"/"+"("+Matrix.trace(A)+"+"+e+"))"+"="+cornerness);
                    //if(cornerness!=0)
                /*if(d!=0)
                     {
                     System.out.print("a:"+a+" bc:"+bc+" d:"+d);
                     if((a!=0&&d!=0)||bc!=0)
                     {
                     System.err.print("true");
                     }
                     System.out.println();
                     System.out.println("2*("+Matrix.det(A)+"/"+"("+Matrix.trace(A)+"+"+e+"))"+"="+cornerness);
                     System.out.println();
                    
                     }*/
                    map[x][y].setCornerness(cornerness);
                    
                    if (cornerness > threshold) {
                        

                        


                        Corner c = new Corner(x, y, this.getRGBAt(x, y));
                        c.setCornerness(cornerness);

                        corners.add(c);
                        



                        //System.out.println(cornerness);
                    }
                    

                }
            }
            ArrayList<Corner> corners2 = new ArrayList<Corner>();
            for (Corner c : corners) {
                int x=c.getX();
                int y=c.getY();
                double cness=c.getCornerness();
                boolean isMax=true;
                for(int i=-maxShift; i<=maxShift; i++)
                {
                    for(int j=-maxShift; j<=maxShift; j++)
                    {
                        if ((x + i - 1 >= 0 && x + i - 1 < map.length && y + j - 1 >= 0 && y + j - 1 < map[0].length)) {
                            if(map[x+i-1][y+j-1].getCornerness()>cness)
                            {
                                isMax=false;
                                j=10;
                                i=10;
                            }
                        }
                    }
                }
                if(isMax)
                {
                    corners2.add(c);
                }
                
            }
                corners=corners2;
                corners=refineSubPixel(corners,grads,2,0,refineIters);
            //Collections.sort(corners, new cornernessComparator());
            
            
            /*Collections.sort(corners, new cornernessComparator());
            //while (corners.size() > 0) {
           
            for (Corner c : corners) {
                
                //corners2.add(c);
                int x = c.getX();
                int y = c.getY();
                double cornerness = c.getCornerness();
                ArrayList<Corner> overlapping = new ArrayList<Corner>();

                for (int wX = -1 * maxShift; wX <= maxShift; wX++) {
                    for (int wY = -1 * maxShift; wY <= maxShift; wY++) {

                        if ((x + wX - 1 >= 0 && x + wX - 1 < cornernessMap.length && y + wY - 1 >= 0 && y + wY - 1 < cornernessMap[0].length)) {

                            //if (isCorner[x + wX][y + wY]) {
                            if (!(wX == 0 && wY == 0)) {
                                if (cornernessMap[x + wX - 1][y + wY - 1] < cornerness) {
                                    //ArrayList<Corner> remove=getLine(cornernessMap,x+ wX - 1,y+ wY - 1,maxShift,posToCornerMap, threshold/2);
                                    
                                    //corners2.removeAll(remove);
                                    corners2.remove(posToCornerMap.get(new Point(x+ wX - 1,y+ wY - 1)));
                                    
                                    //posToCornerMap.remove(new Point(x, y));
                                }
                                else if (cornernessMap[x + wX - 1][y + wY - 1] > cornerness) {
                                    //ArrayList<Corner> remove=getLine(cornernessMap,x,y,maxShift,posToCornerMap, threshold/2);
                                    
                                    //corners2.removeAll(remove);
                                    corners2.remove(posToCornerMap.get(new Point(x,y)));
                                    
                                   
                                }
                                
                                
                               

                            }

                            

                        }

                    }
                
                }

                //corners2.removeAll(overlapping);
            }
            

            corners = corners2;*/
            //Sift Naming
            corners2 = new ArrayList<Corner>();
            for (Corner corner : corners) {
                int x = corner.getX();
                int y = corner.getY();
                double[] oriHist = grads.createHistogramOfGradOri(x - windowRad - 1, y - windowRad - 1, windowSize, windowSize,36);
                double max = oriHist[0];
                int index = 0;
                for (int i = 1; i < oriHist.length; i++) {
                    if (oriHist[i] > max) {
                        max = oriHist[i];
                        index = i;
                    }
                }
                try{
                corner.setDirection(Math.toDegrees(grads.getGradientAt(x, y).getTheta()));
                }catch(java.lang.NullPointerException e)
                {
                    System.out.println(x+","+y);
                }
                corner.setOrientation(index);
                SiftName name1=new SiftName();
                name1.createSiftName(grads, x, y, corner.getDirection());
                corner.setName(name1);
                for (int i = 0; i < oriHist.length; i++) {
                    if (oriHist[i] / max >= .8) {
                        //System.out.print(i);
                        if (i != index) {
                            //System.out.print("  new");
                            Corner c = new Corner(x, y, this.getRGBAt(x, y));
                            c.setCornerness(corner.getCornerness());
                            c.setOrientation(i);
                            c.setDirection(corner.getDirection());
                            SiftName name=new SiftName();
                            name.createSiftName(grads, x, y, c.getDirection());
                            c.setName(name);

                            corners2.add(c);
                        }
                        //System.out.println();
                    }

                }
            }
            corners.addAll(corners2);
            
            //System.out.println(corners.size()+" vs "+corners2.size());
            /*Collections.sort(cornernessList);
             Collections.reverse(cornernessList);
             System.out.println("----------------------------------");
             int stop=50;
             if(cornernessList.size()<stop)
             {
             stop=cornernessList.size();
             }
             for(int i=0; i<50;i++)
             {
             System.out.println(cornernessList.get(i));
             }
             System.out.println("----------------------------------");*/
        } else {
            for (int x = windowRad + maxShift; x < map.length - windowRad - maxShift; x++) {
                for (int y = windowRad + maxShift; y < map[0].length - windowRad - maxShift; y++) {
                    int sum = 0;
                    /*double cur = new Window(createPartialIntensityMap(map, x, y, windowRad)).getWindowIntensity();
                     for (int u = -1 * maxShift; u <= maxShift; u += maxShift) {
                     int u2 = u * u;
                     for (int v = -1 * maxShift; v <= maxShift; v += maxShift) {
                     int v2 = v * v;
                     double curDif = calculateIntensityChange(map, x, y, u, v, windowRad, cur);
                     double gauss2 = Math.exp((-1 * (u2 + v2)) / (2 * sigma2));
                     //sum+=curDif*gauss1*gauss2;
                     if (curDif >= threshold) {
                     sum++;
                     }
                     }
                     }*/
                    if (sum >= minNum) {
                        
                        Corner c = new Corner(x, y, this.getRGBAt(x, y));
                        corners.add(c);
                        
                    }
                    

                    
                }
            }
        }
        
    }
public static double round(double value, int places) {
    if (places < 0) throw new IllegalArgumentException();
    try{
    BigDecimal bd = new BigDecimal(value);
    bd = bd.setScale(places, BigDecimal.ROUND_HALF_UP);
    return bd.doubleValue();
    }catch(java.lang.NumberFormatException e){
        return Double.NaN;
    }
    
}
public static double round(BigDecimal value, int places) {
    
    if (places < 0) throw new IllegalArgumentException();
    try{
    BigDecimal bd=value.add(new BigDecimal(0));
    bd = bd.setScale(places, BigDecimal.ROUND_HALF_UP);
    return bd.doubleValue();
    }catch(java.lang.NumberFormatException e){
        return Double.NaN;
    }
    
}
public static double averageList(ArrayList<Double> list)
{
    BigDecimal sum=new BigDecimal(0, MathContext.UNLIMITED);
    for(int i=0; i<list.size(); i++)
    {
        sum.add(new BigDecimal(list.get(i)));
    }
    BigDecimal avg=sum.divide(new BigDecimal(list.size()));
    return avg.doubleValue();
}
    public ArrayList<Corner> refineSubPixel(ArrayList<Corner> cornersOrig, GradientMap grads, int winSize, int deadZone, int numIters) {
        ArrayList<Corner> corners=cornersOrig;
        ArrayList<String> printList=new ArrayList<String>(corners.size());
        
        boolean debugging=false;
        
        //repeat a certain amount of times
        for(int iteration=0; iteration<numIters; iteration++)
        {
            ArrayList<Corner> newCorners=new ArrayList<Corner>();
            Random rand=new Random();
            for(int i=0; i<corners.size(); i++)
            {
                
                Corner corner=corners.get(i);
                //System.out.println(i+"/"+corners.size());
                Point cornerPoint=corner.getLocation();
                
                ArrayList<ArrayList<Vector>> system= new ArrayList<ArrayList<Vector>>();
    //            for(int i=0; i<2; i++)
    //            {
    //                int x=rand.nextInt(2*winSize);
    //                if(x>=0)
    //                {
    //                    x++;
    //                }
    //                int y=rand.nextInt(2*winSize);
    //                if(y>=0)
    //                {
    //                    y++;
    //                }
    ////            }
                //get gradient direction for each point
                for(int x=-1*winSize; x<=winSize; x++)
                {
                    for(int y=-1*winSize; y<=winSize; y++)
                    {
                        if(Math.abs(x)>deadZone||Math.abs(y)>deadZone)
                        {

                            ArrayList<Vector> vectors= new ArrayList<Vector>();
                            int curLocX=(int)(cornerPoint.getX()+x);
                            int curLocY=(int)(cornerPoint.getY()+y);
                            //System.out.println("getting grad");
                            Gradient curGrad=grads.getGradientAt(curLocX, curLocY);

                            //System.out.println("fin getting grad");
                            if(curGrad!=null&&!(x==0&&y==0))
                            {

                                Vector v=Vector.createVectorPolar(curGrad.getMagnitude(),(curGrad.getTheta()));
                                //Vector v=Vector.createVector(-1*vTemp.getX(),vTemp.getY());
                                vectors.add(v);

                                Vector v2=Vector.createVector(curLocX, curLocY);
                                vectors.add(v2);

                                system.add(vectors);
                            }

                        }
                    }
                }

                if(system.size()>1)
                {
                    //StringBuffer errorString=new StringBuffer();
                    MathContext mc=MathContext.DECIMAL32;
                    BigDecimal xSum=new BigDecimal(0, mc);
                    BigDecimal ySum=new BigDecimal(0, mc);
                    int count=0;
                    int count2=0;
                    for(int num1=0; num1<system.size(); num1++)
                    {
                        for(int num2=0; num2<system.size(); num2++)
                        {
                            if(num1!=num2)
                            {
                                
                                Point.Double newQ1=solveForQ1(system.get(num1),system.get(num2));
                                if(newQ1!=null)
                                {
                                    //Point.Double newQ2=solveForQ2(system.get(num1),system.get(num2));
                                    if(newQ1.distance(cornerPoint)<100)
                                    {
                                    xSum=xSum.add((new BigDecimal(newQ1.getX())));
                                    ySum=ySum.add((new BigDecimal(newQ1.getY())));
                                    count++;
                                    
                                        //errorString.append(("newQ1.getX()="+round(newQ1.getX(),3)+" newQ1.getY()="+round(newQ1.getY(),3)+"\n"));
                                    }
                                    
                                    
                                }
                                else
                                {
                                    count2++;
                                }
                                
                            }
                        }
                    }
                    //errorString.append("worked "+count+" times; returned null "+count2+" times"+"\n");
                    if(count!=0)
                    {
                       
                    
                    BigDecimal xBigAvg=xSum.divide(new BigDecimal((count)), 5, RoundingMode.HALF_UP);
                    BigDecimal yBigAvg=ySum.divide(new BigDecimal((count)), 5, RoundingMode.HALF_UP);
                    
                    double xAvg=xBigAvg.doubleValue();
                    double yAvg=yBigAvg.doubleValue();
                    //errorString.append("xAvg="+xAvg+" yAvg="+yAvg);
//                    xSum+=cornerPoint.getX()/50;
//                    ySum+=cornerPoint.getY()/50;
                    /*int num1=rand.nextInt(system.size());
                    int num2=rand.nextInt(system.size());
                    while(num2==num1)
                    {
                        num2=rand.nextInt(system.size());
                    }
                    //System.out.println("solving for Q");
                    Point.Double newQ1=solveForQ1(system.get(num1),system.get(num2));
                    Point.Double newQ2=solveForQ2(system.get(num1),system.get(num2));
                    Point.Double newQ=new Point.Double((newQ1.getX()+newQ2.getX())/2,(newQ1.getY()+newQ2.getY())/2);*/
                    //System.out.println(newQ);
                    //System.out.println("finished solving for Q");
                    //
                    Point.Double newQ=new Point.Double(xAvg,yAvg);
                    if(debugging)
                    {
                        double dx=corner.getLocationDouble().getX()-xAvg;
                        double dy=corner.getLocationDouble().getY()-yAvg;
                        printList.set(i,printList.get(i)+"("+dx+","+dy+")--> ");
                    }
                    if(newQ.distance(cornerPoint)<15)
                    {
                        corner=new Corner(newQ.getX(),newQ.getY(),corner.getColor());
                        
                    }
                    else
                    {
                        System.out.println("err ################################");
                       //System.out.println(errorString);
                        System.out.println("/err ################################\n\n\n");
                        
                    }
                    }
                    


                }

                newCorners.add(corner);
                //System.out.println("added corner");
            }
            corners=newCorners;
        }
        if(isDebugging)
        {
            for(int i=0; i<corners.size(); i++)
            {
            
                
                printList.set(i,printList.get(i)+" "+" ["+corners.get(i).getLocationDouble().getX()+","+corners.get(i).getLocationDouble().getY()+"]");
            }
            for(int i=0; i<printList.size(); i++)
            {
                System.out.println(printList.get(i));
            }
        }
//        System.out.println(cornersOrig.size()+" vs "+corners.size());
//        for(int i=0; i<corners.size(); i++)
//        {
//            System.out.println(cornersOrig.get(i).getLocationDouble()+"-->"+corners.get(i).getLocationDouble());
//        }
        return corners;
    }
    private  final boolean isDebugging=false;
    private  Point.Double solveForQ1(ArrayList<Vector> vectors1, ArrayList<Vector> vectors2)
    {
        
        Vector v1=vectors1.get(0);
        double o=v1.getX();//.getMagnitude();//p
        double p=v1.getY();//.getDirection();//0
        //System.out.println(grad1Dir);
        Vector v2=vectors1.get(1);
        double c=v2.getX();//a
        double d=v2.getY();//b
        
        
        Vector v3=vectors2.get(0);
        double k=v3.getX();//.getMagnitude();//l
        double l=v3.getY();//.getDirection();//k
        //System.out.println("grad1Dir:"+Math.toDegrees(grad1Dir)+" grad2Dir:"+Math.toDegrees(grad2Dir));
        //System.out.println(grad2Dir);
        Vector v4=vectors2.get(1);
        double a=v4.getX();//c
        double b=v4.getY();//d
        boolean changed=false;
        //System.out.println(l*o-k*p);
        //System.out.println(k*p-l*o);
        double qx=0;
        double qy=0;
        if(Math.abs(l*o-k*p)>.000000001)
        {
            qx=((a*l*o)-(b*k*o)-(c*k*p)+(d*k*o))/((l*o)-(k*p));
//            if(Math.abs(qx)>100000)//isDebugging)
//            {
//                System.out.println("qx=(("+a+"*"+l+"*"+o+")-("+b+"*"+k+"*"+o+")-("+c+"*"+k+"*"+p+")+("+d+"*"+k+"*"+o+"))/(("+l+"*"+o+")-("+k+"*"+p+"))");
//                System.out.println("  =(("+(a*l*o)+")-("+(b*k*o)+")-("+(c*k*p)+")+("+(d*k*o)+"))/(("+(l*o)+")-("+(k*p)+"))");
//                System.out.println("  =(("+(a*l*o)+")-("+(b*k*o)+")-("+(c*k*p)+")+("+(d*k*o)+"))/(("+((l*o)-(k*p))+"))");
//                System.out.println("  ="+qx);
//            }
            
            
            
        }
        else
        {
//            if(l*o-k*p!=0)
//            {
//            System.out.println("qx=(("+a+"*"+l+"*"+o+")-("+b+"*"+k+"*"+o+")-("+c+"*"+k+"*"+p+")+("+d+"*"+k+"*"+o+"))/(("+l+"*"+o+")-("+k+"*"+p+"))");
//                System.out.println("  =(("+(a*l*o)+")-("+(b*k*o)+")-("+(c*k*p)+")+("+(d*k*o)+"))/(("+(l*o)+")-("+(k*p)+"))");
//                System.out.println("  =(("+(a*l*o)+")-("+(b*k*o)+")-("+(c*k*p)+")+("+(d*k*o)+"))/(("+((l*o)-(k*p))+"))");
//                System.out.println("  ="+qx);
//            }
            return null;
        }//System.out.print("qx:"+qx);
        if(Math.abs(k*p-l*o)>.000000001)
        {
            qy=((-1*(a*l*p))+(b*k*p)+(c*l*p)-(d*l*o))/((k*p)-(l*o));
//            if(Math.abs(qy)>100000)
//            {
//                
//                System.out.println("qy=((-1*("+a+"*"+l+"*"+p+"))+("+b+"*"+k+"*"+p+")+("+c+"*"+l+"*"+p+")-("+d+"*"+l+"*"+o+"))/(("+k+"*"+p+")-("+l+"*"+o+"))");
//                System.out.println("  =((-1*("+(a*l*p)+"))+("+(b*k*p)+")+("+(c*l*p)+")-("+(d*l*o)+"))/(("+(k*p)+")-("+(l*o)+"))");
//                System.out.println("  =((-1*("+(a*l*p)+"))+("+(b*k*p)+")+("+(c*l*p)+")-("+(d*l*o)+"))/(("+((k*p)-(l*o))+"))");
//                System.out.println("  ="+qy);
//            }
            
        }
        else
        {
//            if(k*p-l*o!=0)
//            {
//            System.out.println("qy=((-1*("+a+"*"+l+"*"+p+"))+("+b+"*"+k+"*"+p+")+("+c+"*"+l+"*"+p+")-("+d+"*"+l+"*"+o+"))/(("+k+"*"+p+")-("+l+"*"+o+"))");
//                System.out.println("  =((-1*("+(a*l*p)+"))+("+(b*k*p)+")+("+(c*l*p)+")-("+(d*l*o)+"))/(("+(k*p)+")-("+(l*o)+"))");
//                System.out.println("  =((-1*("+(a*l*p)+"))+("+(b*k*p)+")+("+(c*l*p)+")-("+(d*l*o)+"))/(("+((k*p)-(l*o))+"))");
//                System.out.println("  ="+qy);
//            }
            return null;
        }
        //System.out.println("   qy:"+qy);
        /*if(((grad2Mag*grad1Dir)-(grad2Dir*grad1Mag))!=0 && grad2Mag!=0)
        {
            changed=true;
            //x = (a l o - c k p + b l p - d l p)/(l o - k p)
            qx=((px1*grad2Mag*grad1Dir)-(px2*grad2Dir*grad1Mag)+(py1*grad2Mag*grad1Mag)-(py2*grad2Mag*grad1Mag))/((grad2Mag*grad1Dir)-(grad2Dir*grad1Mag));
            // y == (-(a k o) + c k o + d l o - b k p)/(l o - k p)) 
            qy=(-(px1*grad2Dir*grad1Dir)+(px2*grad2Dir*grad1Dir)+(py2*grad2Mag*grad1Dir)-(py1*grad2Dir*grad1Mag))/((grad2Mag*grad1Dir)-(grad2Dir*grad1Mag));
            //System.out.println("equation 1");
        }
        if(grad2Mag==0&&grad1Mag!=0&&grad2Dir!=0)
        {
            changed=true;
            //System.out.println("equation 2");
            qx=px2;
            qy = ((px1*grad1Dir) - (px2*grad1Dir) + (py1*grad1Mag))/grad1Mag;
            
        }*/
        
        /*double tanF1=Math.tan(grad1Dir-(Math.PI/2));
        double tanF2=Math.tan(grad2Dir-(Math.PI/2));
        
        double qx=((tanF2*px2)+(tanF1*px1)+py1-py2)/(tanF2-tanF1);
        double qy=tanF1*(qx-px1)+py1;*/
        
        //Vector solved1=Vector.createVector(qx-c,qy-d);
        //System.out.println("Solved dot="+v1.dot(solved1));
        
        return new Point.Double(qx,qy);
    }
    private Point.Double solveForQ2(ArrayList<Vector> vectors1, ArrayList<Vector> vectors2)
    {
        Vector v1=vectors1.get(0);
        double grad1Mag=v1.getMagnitude();//p
        double grad1Dir=v1.getDirection();//0
        //System.out.println(grad1Dir);
        Vector v2=vectors1.get(1);
        double px1=v2.getX();//a
        double py1=v2.getY();//b
        
        
        Vector v3=vectors2.get(0);
        double grad2Dir=v3.getMagnitude();//l
        double grad2Mag=v3.getDirection();//k
        //System.out.println("grad1Dir:"+Math.toDegrees(grad1Dir)+" grad2Dir:"+Math.toDegrees(grad2Dir));
        //System.out.println(grad2Dir);
        Vector v4=vectors2.get(1);
        double px2=v4.getX();//c
        double py2=v4.getY();//d
        boolean changed=false;
        double qx=0;
        double qy=0;
        
        if(((grad2Mag*grad1Dir)-(grad2Dir*grad1Mag))!=0 && grad2Mag!=0)
        {
            changed=true;
            //x = (a l o - c k p + b l p - d l p)/(l o - k p)
            qx=((px1*grad2Mag*grad1Dir)-(px2*grad2Dir*grad1Mag)+(py1*grad2Mag*grad1Mag)-(py2*grad2Mag*grad1Mag))/((grad2Mag*grad1Dir)-(grad2Dir*grad1Mag));
            // y == (-(a k o) + c k o + d l o - b k p)/(l o - k p)) 
            qy=(-(px1*grad2Dir*grad1Dir)+(px2*grad2Dir*grad1Dir)+(py2*grad2Mag*grad1Dir)-(py1*grad2Dir*grad1Mag))/((grad2Mag*grad1Dir)-(grad2Dir*grad1Mag));
            //System.out.println("equation 1");
        }
        if(grad2Mag==0&&grad1Mag!=0&&grad2Dir!=0)
        {
            changed=true;
            //System.out.println("equation 2");
            qx=px2;
            qy = ((px1*grad1Dir) - (px2*grad1Dir) + (py1*grad1Mag))/grad1Mag;
            
        }
        
        /*double tanF1=Math.tan(grad1Dir-(Math.PI/2));
        double tanF2=Math.tan(grad2Dir-(Math.PI/2));
        
        double qx=((tanF2*px2)+(tanF1*px1)+py1-py2)/(tanF2-tanF1);
        double qy=tanF1*(qx-px1)+py1;*/
        
        Vector solved1=Vector.createVector(qx-px1,qy-py1);
        //System.out.println("Solved dot="+v1.dot(solved1));
        
        return new Point.Double(qx,qy);
    }

    private static class CornernessComparator implements Comparator {

        public int compare(Object t, Object t1) {
            Corner c = (Corner) t;
            Corner c1 = (Corner) t1;
            return (int)(c1.getCornerness() - c.getCornerness());
            
        }
    }

    public ArrayList<ConnectedPoints> groupAdjacent(boolean[][] cornerMap, boolean shouldDoDiagonals) {
        ArrayList<ConnectedPoints> ret = new ArrayList<ConnectedPoints>();
        HashMap<Point, Boolean> beenDone = new HashMap<Point, Boolean>();
        for (int x = 0; x < cornerMap.length; x++) {
            for (int y = 0; y < cornerMap[0].length; y++) {
                Point p = new Point(x, y);
                if (cornerMap[x][y]) {
                    if (!(beenDone.containsKey(p) && beenDone.get(p))) {
                        ret.add(getAdjacent(cornerMap, x, y, shouldDoDiagonals, beenDone));
                    }
                }
            }
        }

        return ret;

    }

    public ConnectedPoints getAdjacent(boolean[][] cornerMap, int x, int y, boolean shouldDoDiagonals, HashMap<Point, Boolean> beenDone) {

        ConnectedPoints ret = new ConnectedPoints();
        if (beenDone.containsKey(new Point(x, y)) && beenDone.get(new Point(x, y))) {
            return ret;
        }

        beenDone.put(new Point(x, y), true);

        for (int i = -1; i <= 1; i++) {
            for (int j = -1; j <= 1; j++) {
                if (cornerMap[x + i][y + j]) {
                    if (i == 0 && j == 0) {
                    } else if (i == 0 || j == 0) {
                        ret.addAll(getAdjacent(cornerMap, x + i, y + j, shouldDoDiagonals, beenDone));
                    } else if (shouldDoDiagonals) {
                        ret.addAll(getAdjacent(cornerMap, x + i, y + j, shouldDoDiagonals, beenDone));
                    }
                }

            }
        }
        ret.add(new Point(x, y));
        return ret;
    }

    public double calculateIntensityChange(RGB[][] map, int x, int y, int u, int v, int rad) {


        double cur = new Window(createPartialIntensityMap(map, x, y, rad)).getWindowIntensity();

        return calculateIntensityChange(map, x, y, u, v, rad, cur);
    }

    public double calculateIntensityChange(RGB[][] map, int x, int y, int u, int v, int rad, double cur) {

        if (x + u + rad >= map.length || y + v + rad >= map[0].length || x + u - rad < 0 || y + v - rad < 0) {
            if (x + u + rad >= map.length) {
                System.out.print("0");
            }
            if (y + v + rad >= map[0].length) {
                System.out.print("1");
            }
            if (x + u - rad < 0) {
                System.out.print("2");
            }
            if (y + v - rad < 0) {
                System.out.print("3");
            }
            System.out.println("fail:x=" + x + " y=" + y + " u=" + u + " v=" + v + " rad=" + rad + " map.length=" + map.length + " map[0].length=" + map[0].length);

            return 0;
        }

        double next = new Window(createPartialIntensityMap(map, x + u, y + v, rad)).getWindowIntensity();

        return Math.pow(next - cur, 2);
    }
    //private Pixel[][] pixMap;
    private RGB[][] map;

    /*public Pixel getPixelAt(int x, int y) {
        return pixMap[x][y];
    }*/

    public RGB getRGBAt(int x, int y) {
        if (x >= map.length || y >= map[0].length || x < 0 || y < 0) {
            return null;
        }
        return map[x][y];
    }

    public RGB[][] getSurrounding(int x, int y, int radius) {
        RGB[][] sur = new RGB[(2 * radius) + 1][(2 * radius) + 1];
        for (int i = -1 * radius; i <= radius; i++) {
            for (int j = -1 * radius; j <= radius; j++) {

                sur[i + radius][j + radius] = getRGBAt(x + i, y + j);


            }

        }
        return sur;
    }
    public ArrayList<Point> detectCornersRefine(BufferedImage image,int refineIters) {

        ArrayList<Point> corners = new ArrayList<Point>();

        BufferedImage grayImage = getGrayscale(image);
      

        RGB[][] map0 = convertTo2DWithoutUsingGetRGB(grayImage);
       

        //map = getBlockMap(map0, 1);
        map = map0;
        
        
        createCornerListRefine(map, threshold, maxShift, windowSize,refineIters);

        
        return corners;
    }
    public ArrayList<Point> detectCorners(BufferedImage image) {

        ArrayList<Point> corners = new ArrayList<Point>();

        BufferedImage grayImage = getGrayscale(image);
        //System.out.println(grayImage.getWidth() + " " + grayImage.getHeight());


        //int width = grayImage.getWidth();
        //int height = grayImage.getHeight();

        RGB[][] map0 = convertTo2DWithoutUsingGetRGB(grayImage);
       

        //map = getBlockMap(map0, 1);
        map = map0;
        /*pixMap = new Pixel[map.length][map[0].length];
         //System.out.println(map.length+" "+map[0].length);
         for (int i = 1; i < map.length - 1; i++) {
         for (int j = 1; j < map[i].length - 1; j++) {
                


         pixMap[i][j] = createPixel(getSurrounding(i, j, windowSize), map[i][j]);
         //pixMap[i][j]=new Pixel(map[i][j],Math.sqrt(getSSD(sur, map[i][j])));

         }

         }*/
        //printList(pixMap, threshold);
        createCornerList(map, threshold, maxShift, windowSize);

        //System.out.println("max:" + max);
        /*double ave = 0;
        for (Double d : diffs) {
            ave += d;
        }
        ave /= diffs.size();*/
        //System.out.println("Average:" + ave);


        return corners;
    }
    //double min = 0;
    //double max = 0;

    /*public int getSSD(ArrayList<RGB> surrounding, RGB cur) {
     int ret = 0;

     for (int i = 0; i < surrounding.size(); i++) {
     if (surrounding.get(i) == null) {
     surrounding.set(i, cur);
     }
     if (Math.pow(surrounding.get(i).toGray() - cur.toGray(), 2) > max) {
     max = Math.pow(surrounding.get(i).toGray() - cur.toGray(), 2);
     }
     //System.err.println(Math.pow(surrounding.get(i).toGray() - cur.toGray(), 2));

     }


        
     double[][] temp=new double[3][3];
     Pixel pix = new Pixel(cur,
     Math.pow(surrounding.get(1).toGray() - cur.toGray(), 2),
     Math.pow(surrounding.get(2).toGray() - cur.toGray(), 2),
     Math.pow(surrounding.get(3).toGray() - cur.toGray(), 2),
     Math.pow(surrounding.get(4).toGray() - cur.toGray(), 2),
     Math.pow(surrounding.get(5).toGray() - cur.toGray(), 2),
     Math.pow(surrounding.get(6).toGray() - cur.toGray(), 2),
     Math.pow(surrounding.get(7).toGray() - cur.toGray(), 2),
     Math.pow(surrounding.get(0).toGray() - cur.toGray(), 2));
     for (RGB p : surrounding) {
     if (p != null) {
     ret += Math.pow(p.toGray() - cur.toGray(), 2);
     }
     //System.out.println(ret);
     }
     return ret;
     }*/
    //ArrayList<Double> diffs = new ArrayList<Double>();

    public Pixel createPixel(RGB[][] surrounding, RGB cur) {
        double[][] difs = new double[surrounding.length][surrounding[0].length];
        for (int i = 0; i < surrounding.length; i++) {
            for (int j = 0; j < surrounding[0].length; j++) {
                RGB temp = surrounding[i][j];
                if (temp != null && cur != null) {
                    difs[i][j] = temp.getIntensity();//Math.pow(temp.getIntensity()-cur.getIntensity(),2);
                }
            }
        }
        Pixel pix = new Pixel(cur, difs);
        /*for (int i = 0; i < surrounding.size(); i++) {
         if(cur==null)
         {
         return null;
         }
         if (surrounding.get(i) == null) {
         surrounding.set(i, cur);
         }
         diffs.add(Math.pow(surrounding.get(i).toGray() - cur.toGray(), 2));
         if (Math.pow(surrounding.get(i).toGray() - cur.toGray(), 2) > max) {
         max = Math.pow(surrounding.get(i).toGray() - cur.toGray(), 2);
         }
         //System.err.println(Math.pow(surrounding.get(i).toGray() - cur.toGray(), 2));
         }
         //System.out.println();
         Pixel pix = new Pixel(cur,
         Math.pow(surrounding.get(1).toGray() - cur.toGray(), 2),
         Math.pow(surrounding.get(2).toGray() - cur.toGray(), 2),
         Math.pow(surrounding.get(3).toGray() - cur.toGray(), 2),
         Math.pow(surrounding.get(4).toGray() - cur.toGray(), 2),
         Math.pow(surrounding.get(5).toGray() - cur.toGray(), 2),
         Math.pow(surrounding.get(6).toGray() - cur.toGray(), 2),
         Math.pow(surrounding.get(7).toGray() - cur.toGray(), 2),
         Math.pow(surrounding.get(0).toGray() - cur.toGray(), 2));*/
        return pix;
    }

    /**
     * @return the windowSize
     */
    public int getWindowSize() {
        return windowSize;
    }

    /**
     * @param windowSize the windowSize to set
     */
    public void setWindowSize(int windowSize) {
        this.windowSize = windowSize;
    }

    /**
     * @return the maxShift
     */
    public int getMaxShift() {
        return maxShift;
    }

    /**
     * @param maxShift the maxShift to set
     */
    public void setMaxShift(int maxShift) {
        this.maxShift = maxShift;
    }
    /*public void getSubPixel(GradientMap grads)
    {
        int rad=2;
        int numSample=5;
        int numSurrounding=(rad*2+1)*(rad*2+1)-1;
        double 
        for(int i=0; i<corners.size(); i++)
        {
            for(int i=0; i<)
            Corner c=corners.get(i);
            Point pos=c.getLocation();
            
        }
    }
    public Point getRandomNeighbor(Point cur, int rad, Random rand)
    {
        int numCols=(rad*2+1);
        int numSurrounding=(numCols*numCols)-1;
        
        int r=rand.nextInt(numSurrounding);
        if(r>numSurrounding/2)
        {
            r++;
        }
        int dx=(r/numCols)-rad;
        int dy=(r%numCols)-rad;
        return new Point((int)cur.getX()+dx,(int)cur.getY()+dy);
    }
    //g(x,y) = \frac{1}{2\pi \sigma^2} \cdot e^{-\frac{x^2 + y^2}{2 \sigma^2}} [4][5][6]
    private double getGaussianVal(Point orig, Point cur)
    {
        double sigma=1.4;
        double dx=cur.getX()-orig.getX();
        double dy=cur.getY()-orig.getY();
        double dx2=dx*dx;
        double dy2=dy*dy;
        double sigma22=sigma*sigma*2;
        double ret=(1.0/(Math.PI*sigma22))*Math.exp(-1*((dx2+dy2)/sigma22));
        return ret;
    }
    private double getGaussianVal(double dx, double dy)
    {
        double sigma=1.4;
        
        double dx2=dx*dx;
        double dy2=dy*dy;
        double sigma22=sigma*sigma*2;
        double ret=(1.0/(Math.PI*sigma22))*Math.exp(-1*((dx2+dy2)/sigma22));
        return ret;
    }*/
    
}
