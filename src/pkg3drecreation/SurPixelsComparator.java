/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pkg3drecreation;

import java.awt.*;
import java.util.Comparator;

/**
 *
 * @author kingm14
 */
public class SurPixelsComparator implements Comparator{

    private RGB[][] colorMap1;
    private RGB[][] colorMap2;
    private int maxSur=5;
    public SurPixelsComparator(RGB[][] colorMap1,RGB[][] colorMap2)
    {
        this.colorMap1=colorMap1;
        this.colorMap2=colorMap2;
    }
    public int compare(Object t, Object t1) {
        if(! (t instanceof Point) || ! (t1 instanceof Point))
        {
            throw new ClassCastException();
        }
        int x1=(int)((Point)t).getX();
        int y1=(int)((Point)t).getY();
        int x2=(int)((Point)t1).getX();
        int y2=(int)((Point)t1).getY();
        
        return 0;
        
    }
    public  RGB[][] getSurrounding(int x, int y, int radius, RGB[][] colorMap)
    {
        RGB[][] sur=new RGB[(2*radius)+1][(2*radius)+1];
        for (int i = -1*radius; i <= radius; i++) {
            for (int j = -1*radius; j <= radius; j++) {
                sur[i+radius][j+radius]=colorMap[x+i][y+j];
                

            }

        }
        return sur;
    }
    
}
