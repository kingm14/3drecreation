/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pkg3drecreation;

import java.awt.Color;
import java.util.ArrayList;

/**
 *
 * @author kingm14
 */
public class RGB implements Comparable{

    private float r, g, b;
    private double cornerness;

    public RGB(double r, double g, double b) {
        this.r = (float)r;
        this.g = (float)g;
        this.b = (float)b;
    }

    public static RGB average(ArrayList<RGB> list) {
        double aveR = 0;
        double aveG = 0;
        double aveB = 0;
        for (RGB cur : list) {
            if (cur != null) {
                aveR += cur.getR();
                aveG += cur.getG();
                aveB += cur.getB();
            }
        }
        return new RGB(aveR / list.size(), aveG / list.size(), aveB / list.size());
    }

    public double toGray() {
        return (getR() + getG() + getB()) / 3;
    }

    public String toString() {
        return "(" + (int) getR() + "," + (int) getG() + "," + (int) getB() + ")";
    }
    public String toIntString()
    {
        StringBuilder sb=new StringBuilder();
        String red=""+(int)getR();
        while(red.length()<3)
        {
            red+="0";
        }
        String green=""+(int)getG();
        while(green.length()<3)
        {
            green+="0";
        }
        String blue=""+(int)getB();
        while(blue.length()<3)
        {
            blue+="0";
        }
        sb.append(red);
        sb.append(green);
        sb.append(blue);
        return sb.toString();
    }
    public Color toColor()
    {
        return new Color((int)getR(),(int)getG(),(int)getB());
    }
    public boolean equals(RGB o)
    {
        if(o==null)
        {
            return true;
        }
        return getR()==o.getR()&&getG()==o.getG()&&getB()==o.getB();
    }
    public boolean equals(RGB o, double thresh)
    {
        if(o==null)
        {
            return true;
        }
        return Math.abs(getR()-o.getR())<=thresh && Math.abs(getG()-o.getG())<=thresh && Math.abs(getB()-o.getB())<=thresh;
    }
    /**
     * @return the r
     */
    public double getR() {
        return r;
    }

    /**
     * @param r the r to set
     */
    public void setR(double r) {
        this.r = (float)r;
    }

    /**
     * @return the g
     */
    public double getG() {
        return g;
    }

    /**
     * @param g the g to set
     */
    public void setG(double g) {
        this.g = (float)g;
    }

    /**
     * @return the b
     */
    public double getB() {
        return b;
    }

    /**
     * @param b the b to set
     */
    public void setB(double b) {
        this.b = (float)b;
    }

    @Override
    public int compareTo(Object t) {
        return (int)(toGray()-((RGB)t).toGray());
    }
    public double getIntensity()
    {
        return ((0.2989 * r) + (0.5870 * g) + (0.1140 * b));
    }

    /**
     * @return the cornerness
     */
    public double getCornerness() {
        return cornerness;
    }

    /**
     * @param cornerness the cornerness to set
     */
    public void setCornerness(double cornerness) {
        this.cornerness = cornerness;
    }
}
