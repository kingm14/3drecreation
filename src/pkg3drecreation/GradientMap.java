/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pkg3drecreation;

import java.util.*;

/**
 *
 * @author kingm14
 */
public class GradientMap {
    private Gradient[][] map;
    public GradientMap(ArrayList<ArrayList<Double>> gx,ArrayList<ArrayList<Double>> gy)
    {
        int width=gx.size();
        int height=gx.get(0).size();
        map=new Gradient[width][height];
        if(width==gy.size()&&height==gy.get(0).size())
        {
           for(int i=0; i<width; i++)
           {
               ArrayList<Double> gx1=gx.get(i);
               ArrayList<Double> gy1=gy.get(i);
               for(int j=0; j<height; j++)
               {
                   map[i][j]=new Gradient(gx1.get(j),gy1.get(j));
               }
           }
        }
        
    }
    public GradientMap(double[][] gx,double[][] gy)
    {
        int width=gx.length;
        int height=gx[0].length;
        map=new Gradient[width][height];
        if(width==gy.length&&height==gy[0].length)
        {
           for(int i=0; i<width; i++)
           {
               
               for(int j=0; j<height; j++)
               {
                   map[i][j]=new Gradient(gx[i][j],gy[i][j]);
               }
           }
        }
        
    }
    public Gradient getGradientAt(int x, int y)
    {
        if(x<0||x>=map.length||y<0||y>=map[0].length)
            return null;
        return map[x][y];
    }
    public double[][] getHarrisMatrix(int x, int y, int w, int c)
    {
        double a = 0;
        double bc = 0;
        double d = 0;
        for(int i=x; i<x+w; i++)
        {
            for(int j=y; j<y+c; j++)
            {
                Gradient cur=getGradientAt(i,j);
                double iX=cur.getGx();
                double iY=cur.getGy();
                a += iX * iX;
                bc += iX * iY;
                d += iY * iY;
            }
        }
        //System.out.println("a:"+a+" bc:"+bc+" d:"+d);
        return new double[][]{{a, bc},
                        {bc, d}};
    }
    public double[][] getHarrisAffineMatrix(int x, int y, double sigmaI, double sigmaD)
    {
        
        double[][] matrix;
        Gradient l=getL(sigmaD,x,y);
        double a = 0;
        double bc = 0;
        double d = 0;
        double lx=l.getGx();
        double ly=l.getGy();
        a=lx*lx;
        bc=lx*ly;
        d=ly*ly;
        //System.out.println("a:"+a+" bc:"+bc+" d:"+d);
        matrix= new double[][]{{a, bc},
                        {bc, d}};
        double sigmaD2=sigmaD*sigmaD;
        double[][] kernel=getGaussianKernel(sigmaI,x,y);
        for(int i=0; i<kernel.length; i++)
        {
            for(int j=0; j<kernel[0].length; j++)
            {
                kernel[i][j]*=sigmaD2;
            }
        }
        return Convolution.convolution2D(kernel, kernel.length, kernel[0].length, matrix, matrix.length, matrix[0].length);
    }
    public Gradient getL(double scale, int x, int y)
    {
        
        
        double[][] kernel=getGaussianKernel(scale,3,3);
        
        return Convolution.singlePixelConvolution(this, x, y, kernel, kernel.length, kernel[0].length);
    }
    public double[][] getGaussianKernel(double scale, int width, int height)
    {
        double[][] kernel=new double[width][height];
        
        for(int i=0; i<width; i++)
        {
            for(int j=0; j<height; j++)
            {
                int s=width/2;
                if(!(i-s==0&&j-s==0))
                {
                    kernel[i][j]=getGaussian(scale,i-s,j-s);
                }
                else
                {
                    kernel[i][j]=0;
                }
            }
        }
        return kernel;
    }
    public double getGaussian(double scale, int u, int v)
    {
        return (1/(2*Math.PI*scale))*Math.exp(-1*(u*u+v*v)/2*scale);
    }
    public double[] createHistogramOfGradOri(int x, int y, int w, int c, int bins)
    {
        double[] histogram=new double[bins];
        for(int i=x; i<x+w; i++)
        {
            for(int j=y; j<y+c; j++)
            {
                Gradient cur=getGradientAt(i,j);
                if(cur!=null)
                {
                    double iX=cur.getGx();
                    double iY=cur.getGy();
                    histogram[(int)((Math.toDegrees(cur.getTheta()))/(360.0/bins))]+=Math.sqrt((iX*iX)+(iY*iY));
                }
                
            }
        }
        return histogram;
    }
    public double[] createHistogramOfGradOriGauss(int x, int y, int w, int c, int bins, double sigma, int cx, int cy, double direction)
    {
        double sigma22=sigma*sigma;
        double gP1=1/(Math.PI*sigma22);
        double gP2=(-1/sigma22);
        double[] histogram=new double[bins];
        for(int i=x; i<x+w; i++)
        {
            for(int j=y; j<y+c; j++)
            {
                double power=((x-cx)*(x-cx)+(y-cy)*(y-cy))*gP2;
                double G=gP1*Math.exp(power);
                try{
                Gradient cur=getGradientAt(i,j);
                if(cur!=null)
                {
                    double iX=cur.getGx();
                    double iY=cur.getGy();
                    double angle=(((Math.toDegrees(cur.getTheta())%360)-(direction%360)));
                    double posAngle=((angle+360)%360);
                    //if(angle<0)
                    //{
                        //System.err.println(Math.toDegrees(cur.getTheta())+"-" +direction+"="+angle+"-->"+posAngle);
                    //}
                    histogram[(int)Math.round(posAngle/((360.0)/bins))]+=G*PlatformMathUtils.fastPow((float)((iX*iX)+(iY*iY)),(float)0.5);
                }
                }catch(ArrayIndexOutOfBoundsException e){}
                
            }
        }
        return histogram;
    }
    
}
