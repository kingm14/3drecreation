/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pkg3drecreation;

/**
 *
 * @author kingm14
 */
public class Matrix {
    public static double det(double[][] matrix)
    {
        double det=(matrix[0][0]*matrix[1][1])-(matrix[0][1]*matrix[1][0]);
//        if(matrix[1][1]!=0)
//        {
//            System.out.println("("+matrix[0][0]+"*"+matrix[1][1]+")-("+matrix[0][1]+"*"+matrix[1][0]+")="+det);
//            System.out.println((matrix[0][0]*matrix[1][1])+"-"+(matrix[0][1]*matrix[1][0])+"="+det);
//        }
        return det;
    }
    public static double trace(double[][] matrix)
    {
        double ret=0;
        if(matrix.length!=matrix[0].length)
        {
            throw new IndexOutOfBoundsException();
        }
        for(int i=0; i<matrix.length; i++)
        {
            ret+=matrix[i][i];
        }
        return ret;
        
    }
}
