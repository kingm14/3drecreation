/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pkg3drecreation;

import mpi.cbg.fly.Feature;

/**
 *
 * @author kingm14
 */
public class SiftName {
    private Histogram[][] featureVector;
    private Feature feature;
    
    public SiftName()
    {
        featureVector=new Histogram[4][4];
    }
    public SiftName(Feature f)
    {
        feature=f;
    }
    public double calculateDifference(SiftName other)
    {
        if(feature != null)
        {
            return feature.descriptorDistance(other.getFeature());
        }
        else
        {
        double ret=0;
        Histogram[][] fv2=other.getFeatureVector();
        for(int i=0; i<featureVector.length; i++)
        {
            for(int j=0; j<featureVector[0].length; j++)
            {
                ret+=featureVector[i][j].computeDifferences(fv2[i][j]);
            }
        }
        return ret;
        }
        
    }
    public Feature getFeature()
    {
        return feature;
    }
    public String toString()
    {
        StringBuilder s=new StringBuilder();
        for(int i=0; i<getFeatureVector().length; i++)
        {
            for(int j=0; j<getFeatureVector()[0].length; j++)
            {
                s.append(getFeatureVector()[i][j].toString()+"\n##########\n");
                
            }
        }
        return s.toString();
    }
    public void createSiftName(GradientMap gMap, int cx, int cy, double direction)
    {
        double sigma=5;
        double sum=0;
        for(int featX=-2; featX<=2; featX++)
        {
            for(int featY=-2; featY<=2; featY++)
            {
                if(featX!=0||featY!=0)
                {
                    int placeX=featX;
                    if(placeX>0)
                    {
                        placeX--;
                    }
                    placeX+=2;
                    int placeY=featY;
                    if(placeY>0)
                    {
                        placeY--;
                    }
                    placeY+=2;
                    getFeatureVector()[placeX][placeY]=new Histogram(gMap.createHistogramOfGradOriGauss(cx+2*featX, cy+2*featY, 4, 4, 8, sigma, cx, cy,direction));
                    sum+=Math.pow(getFeatureVector()[placeX][placeY].getSum(),2);
                }
            }
        }
        sum=PlatformMathUtils.fastPow((float)sum, (float)0.5);
        normalize(sum);
        double newSum = cap(0.2);
        normalize(PlatformMathUtils.fastPow((float)newSum, (float)0.5));
        
    }
    public void normalize(double sum)
    {
        for(int i=0; i<getFeatureVector().length; i++)
        {
            for(int j=0; j<getFeatureVector()[0].length; j++)
            {
                getFeatureVector()[i][j].divideAllBy(sum);
            }
        }
    }
    public double cap(double cap)
    {
        double ret=0;
        for(int i=0; i<getFeatureVector().length; i++)
        {
            for(int j=0; j<getFeatureVector()[0].length; j++)
            {
                getFeatureVector()[i][j].cap(cap);
                ret+=Math.pow(getFeatureVector()[i][j].getSum(),2);
            }
        }
        return ret;
    }

    /**
     * @return the featureVector
     */
    public Histogram[][] getFeatureVector() {
        return featureVector;
    }

    /**
     * @param featureVector the featureVector to set
     */
    public void setFeatureVector(Histogram[][] featureVector) {
        this.featureVector = featureVector;
    }
    public double computeDistance(SiftName other)
    {
        
        return calculateDifference(other);
        /*double sum=0;
        Histogram[][] otherH=other.getFeatureVector();
        for(int i=0; i<otherH.length; i++)
        {
            for(int j=0; j<otherH[0].length; j++)
            {
                for(int h=0; h<8; h++)
                {
                    sum+=Math.pow(featureVector[i][j].getValAt(h)-otherH[i][j].getValAt(h),2);
                }
            }
        }
        return Math.sqrt(sum);*/
    }
    class Histogram
    {
        private double[] hist;
        public Histogram(int bins)
        {
            hist=new double[bins];
        }
        public Histogram(double[] hist)
        {
            this.hist=hist;
        }
        public double computeDifferences(Histogram other)
        {
            double ret=0;
            for(int i=0; i<hist.length; i++)
            {
                ret+=Math.abs(hist[i]-other.getHist()[i]);
            }
            return ret;
        }
        public double getSum()
        {
            double ret=0;
            for(int i=0; i<hist.length; i++)
            {
                ret+=hist[i];
            }
            return ret;
        }
        public void divideAllBy(double d)
        {
            for(int i=0; i<hist.length; i++)
            {
                hist[i]/=d;
            }
        }
        public void cap(double d)
        {
            for(int i=0; i<hist.length; i++)
            {
                if(hist[i]>d)
                    hist[i]=d;
            }
        }
        /**
         * @return the hist
         */
        public double[] getHist() {
            return hist;
        }

        /**
         * @param hist the hist to set
         */
        public void setHist(double[] hist) {
            this.hist = hist;
        }
        public double getValAt(int i)
        {
            return hist[i];
        }
        public void setValAt(int i, double d)
        {
            hist[i]=d;
        }
        public String toString()
        {
            StringBuilder s=new StringBuilder();
            for(int i=0; i<hist.length; i++)
            {
                s.append(hist[i]+"\n");
            }
            return s.toString();
        }
    }
}
