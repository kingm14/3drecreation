/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pkg3drecreation;

import java.awt.*;
import javax.vecmath.Point3d;

/**
 *
 * @author kingm14
 */
public class DepthLine extends DepthObject {
    private DepthPoint start,end;
    
    public DepthLine(DepthPoint startX, DepthPoint startY, double change)
    {
        this.start=startX;
        this.end=startY;
        this.depth=change;
    }

    public DepthLine(DepthPoint cur, DepthPoint next) {
        this.start=cur;
        this.end=next;
        depth= getStart().get3DPoint().distance(getEnd().get3DPoint());
    }

    /**
     * @return the startX
     */
    public DepthPoint getStart() {
        return start;
    }

    /**
     * @param startX the startX to set
     */
    public void setStartX(DepthPoint start) {
        this.start = start;
    }

    /**
     * @return the startY
     */
    public DepthPoint getEnd() {
        return end;
    }

    /**
     * @param startY the startY to set
     */
    public void getEnd(DepthPoint end) {
        this.end = end;
    }
    public boolean intersects(DepthLine other)
    {
        boolean shareVertex=(getStart().get3DPoint().equals(other.getStart().get3DPoint())
                ||getStart().get3DPoint().equals(other.getEnd().get3DPoint())
                ||getEnd().get3DPoint().equals(other.getStart().get3DPoint())
                ||getEnd().get3DPoint().equals(other.getEnd().get3DPoint()));
        return Line3D.doLinesIntersect(getStart().get3DPoint(), getEnd().get3DPoint(), other.getStart().get3DPoint(), other.getEnd().get3DPoint())&&!shareVertex;
    }
    public boolean[] getIntersectingPlanes(DepthLine other)
    {
        boolean shareVertex=(getStart().get3DPoint().equals(other.getStart().get3DPoint())
                ||getStart().get3DPoint().equals(other.getEnd().get3DPoint())
                ||getEnd().get3DPoint().equals(other.getStart().get3DPoint())
                ||getEnd().get3DPoint().equals(other.getEnd().get3DPoint()));
        
        boolean[] ret= Line3D.getIntersectPlanes(getStart().get3DPoint(), getEnd().get3DPoint(), other.getStart().get3DPoint(), other.getEnd().get3DPoint());
        if(shareVertex)
        {
            ret[0]=false;
            ret[1]=false;
            ret[2]=false;
        }
        return ret;
    }
    public DepthPoint sharesVertex(DepthLine other)
    {
         if(getStart().get3DPoint().equals(other.getStart().get3DPoint())||getStart().get3DPoint().equals(other.getEnd().get3DPoint()))
         {
             return getStart();
         }
         else if(getEnd().get3DPoint().equals(other.getStart().get3DPoint())
                ||getEnd().get3DPoint().equals(other.getEnd().get3DPoint()))
         {
             return getEnd();
         }
         return null;
                
                
    }
    public double getLength()
    {
        return getStart().get3DPoint().distance(getEnd().get3DPoint());
    }
    public void paint(Graphics g, double scale)
    {
        
        
        if(maxDepthInited)
        {
            double ratio=depth/maxDepth;
            int val=(int)(255*ratio);
            g.setColor(new Color(val,val,val));
        }
        //System.out.println(start+" "+end);
         g.drawLine((int)(scale*start.getP().getY()), (int)(scale*start.getP().getX()), (int)(scale*end.getP().getY()), (int)(scale*end.getP().getX()));
    }

    
    public String toSaveString() {
       return "dl|"+start.toSaveString()+":"+end.toSaveString();
    }
    
    /**
     * @return the maxChange
     */
    
}
