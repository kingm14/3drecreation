/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pkg3drecreation;

import DelaunayTriangulation.Point_dt;
import java.awt.*;
import java.util.ArrayList;
import javax.vecmath.Point3d;

/**
 *
 * @author kingm14
 */
public class DepthPoint extends DepthObject{
    private Point p;
    private Point colorPoint;
    
    
    private final int radius=5;
    public DepthPoint(Point p, double depth )
    {
        this.p=p;
        this.depth=depth;
    }
    public DepthPoint(Point_dt dtp)
    {
        p=new Point((int)dtp.x(),(int)dtp.y());
        depth=dtp.z();
    }

    @Override
    public void paint(Graphics g, double scale) {
        if(maxDepthInited)
        {
           
            g.setColor(getColor());
        }
        g.fillOval((int)((scale*colorPoint.getX())-radius), (int)((scale*colorPoint.getY())-radius), radius*2, radius*2);
    }

    

    /**
     * @return the p
     */
    public Point getP() {
        return p;
    }

    /**
     * @param p the p to set
     */
    public void setP(Point p) {
        this.p = p;
    }
    public Point3d get3DPoint()
    {
        return new Point3d(p.getX(), p.getY(),this.getDepth());
    }

    /**
     * @return the depth
     */
    public double getDepth() {
        return depth;
    }

    /**
     * @param depth the depth to set
     */
    public void setDepth(double depth) {
        this.depth = depth;
    }

    /**
     * @return the maxDepth
     */
    public double getMaxDepth() {
        return maxDepth;
    }

    /**
     * @param maxDepth the maxDepth to set
     */
    
    public Color getColor()
    {
        double ratio=(depth-minDepth)/(maxDepth-minDepth);
        int hue=(int)(255*ratio);
//        Integer val=new Integer((int)(Math.pow(255,3)*ratio));
//        String s=Integer.toString((int)(Math.pow(255,3)*ratio), 255);
//        ArrayList<Integer> temp=Converter.convertList((int)(Math.pow(255,3)*ratio), 255,3);
//        //System.out.println("r:"+temp.get(0)+" g:"+temp.get(1)+" b:"+temp.get(2));
//       
//        return (new Color(temp.get(0),temp.get(1),temp.get(2)));
        if(hue<0||hue>255)
        {
            System.out.println();
            System.out.println("depth:"+depth+",minDepth: "+minDepth+", maxDepth:"+maxDepth+", hue:"+hue);
            hue=0;
        }
        return new Color(hue,hue,hue);
    }
    public boolean equals(DepthPoint other)
    {
        return (other.getP().equals(p));
    }

    /**
     * @return the colorPoint
     */
    public Point getColorPoint() {
        return colorPoint;
    }

    /**
     * @param colorPoint the colorPoint to set
     */
    public void setColorPoint(Point colorPoint) {
        this.colorPoint = colorPoint;
    }

   
    public String toSaveString() {
        String pointString=(int)p.getX()+","+(int)p.getY();
        String cPointString=(int)colorPoint.getX()+","+(int)colorPoint.getY();
       return "dp|"+pointString+"|"+cPointString+"|"+getDepthSaveString();
    }

    


    /**
     * @return the imgColor
     */
   

   
}
