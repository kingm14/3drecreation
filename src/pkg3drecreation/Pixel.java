/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pkg3drecreation;

import java.awt.Graphics;

/**
 *
 * @author kingm14
 */
public class Pixel implements Comparable{
    RGB color;
    //double NDif,NEDif,EDif,SEDif,SDif,SWDif,WDif,NWDif;
    double[][] difs;
    double ssd;
    int numC=3;
    //int threshold=200;
    /*public Pixel(RGB color, double NDif, double NEDif, double EDif,double SEDif, double SDif, double SWDif, double WDif, double NWDif)
    {
        this.color=color;
        this.NDif=NDif;
        this.NEDif=NEDif;
        this.EDif=EDif;
        this.SEDif=SEDif;
        this.SDif=SDif;
        this.SWDif=SWDif;
        this.WDif=WDif;
        this.NWDif=NWDif;
        
    }*/
    public Pixel(RGB color,double[][] difs )
    {
        this.color=color;
        this.difs=difs;
    }
    public double getSSD()
    {
        int nNZ=0;
        int sum=0;
        for(int i=0; i<difs.length; i++)
        {
            for(int j=0; j<difs[0].length; j++)
            {
                if(difs[i][j]>0)
                {
                    nNZ++;
                }
                sum+=difs[i][j];
            }
        }
        /*int nNZ=0;
        if(NDif>0) nNZ++;
        if(NEDif>0) nNZ++;
        if(NWDif>0) nNZ++;
        if(EDif>0) nNZ++;
        if(WDif>0) nNZ++;
        if(SEDif>0) nNZ++;
        if(SDif>0) nNZ++;
        if(SWDif>0) nNZ++;*/
        
        
            
        return Math.sqrt(sum/nNZ);
    }
    
    public boolean isEdge(int threshold)
    {
        return getSSD()>=threshold;
        //return NDif>threshold||NEDif>threshold||EDif>threshold||SEDif>threshold||SDif>threshold||SWDif>threshold||WDif>threshold||NWDif>threshold;
    }
    public boolean isCorner(int threshold)
    {
        return false;
        /*int dX=0;
        int dY=0;
        int pS=0;
        int nS=0;
        int sumY=0;
        int sumX=0;
        
        for(int x=0; x<difs.length; x++)
        {
        for(int y=0; y<difs[0].length; y++)
        {
            if(x==0||x==difs.length-1)
            {
                //sumX+=Math.pow(color.getIntensity(),2);
            }
            else
                sumX+=Math.pow(difs[x-1][y]-color.getIntensity(),2);
            if(y==0||y==difs.length-1)
            {
                //sumY+=Math.pow(color.getIntensity(),2);
            }
            else
                sumY+=-0.5*Math.pow(difs[x][(difs[0].length/2)+1]-color.getIntensity(),2)+.5*Math.pow(difs[x][(difs[0].length/2)+1]-color.getIntensity(),2);
        }
        
        
            
        
        }
        if(sumX>=threshold&&sumY>=threshold)
            System.out.println(sumX+" "+sumY);
        return (sumX>=threshold&&sumY>=threshold);*/
    }
    
    public Pixel(RGB color, double ssd)
    {
        this.color=color;
        this.ssd=ssd;
    }
    public static String insertSpaces(double d, int numCharacters)
    {
        String s="";
        s+=(int)d;
        while(s.length()<numCharacters)
        {
            s+=" ";
        }
        return s;
    }
    public String getLine()
    {
        
        String ret=new String("----");
        for(int i=0; i<numC*3; i++)
        {
            ret+=("-");
        }
        return ret;
    }
    public String getFirstLine()
    {
        return "|";//+insertSpaces(NWDif,numC)+" "+insertSpaces(NDif,numC)+" "+insertSpaces(NEDif,numC)+"|";
    }
    public String getSecondLine()
    {
        return "|";//+insertSpaces(WDif,numC)+" "+insertSpaces(color.toGray(),numC)+" "+insertSpaces(EDif,numC)+"|";
    }
    public String getThirdLine()
    {
        return "|";//+insertSpaces(SWDif,numC)+" "+insertSpaces(SDif,numC)+" "+insertSpaces(SEDif,numC)+"|";
    }
    public String toString()
    {
        
        StringBuilder ret=new StringBuilder();
        ret.append(getLine()+"\n"+getFirstLine()+"\n"+getSecondLine()+"\n"+getThirdLine()+"\n"+getLine());
        
        return ret.toString();
        /*String s= ""+(int)ssd;
        if(ssd<100)
        {
            s+=" ";
        }
        if(ssd<10)
        {
            s+=" ";
        }
        return s;*/
    }

    @Override
    public int compareTo(Object t) {
        return (int) (getSSD()-((Pixel)t).getSSD());
    }
}
