/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pkg3drecreation;

/**
 *
 * @author kingm14
 */
import java.awt.Point;
import java.util.ArrayList;


public interface ConvexHullAlgorithm 
{
        ArrayList<Point> execute(ArrayList<Point> points);
}