/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pkg3drecreation;

/**
 *
 * @author kingm14
 */
public class Window {
    private RGB[][] pixels;
    private double[][] intensities;
    public Window(RGB[][] pixels)
    {
        this.pixels=pixels;
        intensities=new double[pixels.length][pixels[0].length];
        for(int i=0; i<pixels.length; i++)
        {
            for(int j=0; j<pixels[0].length; j++)
            {
                intensities[i][j]=pixels[i][j].getIntensity();
            }
        }
    }
    public Window(double[][] intensities)
    {
        this.intensities=intensities;
    }
    
    public double getGradientXSum()
    {
        double ret=0;
        for(int r=0; r<intensities.length; r++)
        {
            for(int c=0; c<intensities[0].length; c++)
            {
                double prev=0;
                double next=0;
                double cur=intensities[r][c];
                if(r>0)
                {
                    prev=intensities[r-1][c];
                }
                if(r<intensities.length-1)
                {
                    next=intensities[r+1][c];
                }
                ret+=Math.pow((-0.5)*(cur-prev)+.5*(cur-next),2);
            }
        }
        return ret;
    }
    public double getGradientYSum()
    {
        double ret=0;
        for(int r=0; r<intensities.length; r++)
        {
            for(int c=0; c<intensities[0].length; c++)
            {
                double prev=0;
                double next=0;
                double cur=intensities[r][c];
                if(c>0)
                {
                    prev=intensities[r][c-1];
                }
                if(c<intensities.length-1)
                {
                    next=intensities[r][c+1];
                }
                ret+=Math.pow((-0.5)*(cur-prev)+.5*(cur-next),2);
            }
        }
        return ret;
    }
    public double[][] getGradientXMap()
    {
        double[][] ret=new double[intensities.length][intensities[0].length];
        for(int r=1; r<ret.length-1; r++)
        {
            for(int c=0; c<ret[0].length; c++)
            {
                double prev=0;
                double next=0;
                double cur=intensities[r][c];
                if(r>0)
                {
                    prev=intensities[r-1][c];
                }
                if(r<ret.length-1)
                {
                    next=intensities[r+1][c];
                }
                ret[r][c]=(-0.5)*(cur-prev)+.5*(cur-next);
            }
        }
        return ret;
    }
    public double[][] getGradientYMap()
    {
        double[][] ret=new double[intensities.length][intensities[0].length];
        for(int r=0; r<ret.length; r++)
        {
            for(int c=1; c<ret[0].length-1; c++)
            {
                double prev=0;
                double next=0;
                double cur=intensities[r][c];
                if(c>0)
                {
                    prev=intensities[r][c-1];
                }
                if(c<ret[0].length-1)
                {
                    next=intensities[r][c+1];
                }
                ret[r][c]=(-0.5)*(cur-prev)+.5*(cur-next);
            }
        }
        return ret;
    }
    public double getWindowIntensity()
    {
        double sum=0;
        for(int r=0; r<intensities.length; r++)
        {
            for(int c=0; c<intensities[0].length; c++)
            {
                sum+=intensities[r][c];
            }
        }
        return sum/(intensities.length*intensities[0].length);
        
    }
    /**
     * @return the intensites
     */
    public double[][] getIntensities() {
        return intensities;
    }

    /**
     * @param intensites the intensites to set
     */
    public void setIntensites(double[][] intensites) {
        this.intensities = intensites;
    }
}
