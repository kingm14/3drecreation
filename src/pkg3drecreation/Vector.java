/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pkg3drecreation;

import java.awt.Point;


/**
 *
 * @author kingm14
 */
public class Vector {
    private double magnitude;
    private double direction;
    private double x;
    private double y;
    public Vector(double x, double y, double mag, double dir)
    {
        magnitude=mag;
        direction=dir;
        this.x=x;
        this.y=y;
        
    }
    public static Vector createVector(double x, double y)
    {
        
        double mag=Point.distance(0, 0, x, y);
        double dir=Math.atan(y/x);
        Vector v= new Vector(x,y,mag,dir);
        
        return v;
    }
    public Vector swapAxis()
    {
        return createVector(y,x);
    }
    public static Vector createVectorPolar(double mag, double dir)
    {
        
        double y=Math.sin(dir)*mag;
        double x=Math.cos(dir)*mag;
        Vector v= new Vector(x,y,mag,dir);
        
        return v;
    }

    /**
     * @return the magnitude
     */
    public double getMagnitude() {
        return magnitude;
    }

    /**
     * @param magnitude the magnitude to set
     */
    public void setMagnitude(double magnitude) {
        this.magnitude = magnitude;
    }

    /**
     * @return the direction
     */
    public double getDirection() {
        return direction;
    }

    /**
     * @param direction the direction to set
     */
    public void setDirection(double direction) {
        this.direction = direction;
    }
    public double dot(Vector other)
    {
        double angleDif=getDirection()-other.getDirection();
        double cosTheta=Math.cos(angleDif);
        return magnitude*other.getMagnitude()*cosTheta;
    }
    
    public static void main(String[] args)
    {
        Vector a= Vector.createVector(-6, 8);
        Vector b= Vector.createVector(5, 12);
        
        System.out.print(a.dot(b));
    }

    /**
     * @return the x
     */
    public double getX() {
        return x;
    }

    /**
     * @param x the x to set
     */
    public void setX(double x) {
        this.x = x;
    }

    /**
     * @return the y
     */
    public double getY() {
        return y;
    }

    /**
     * @param y the y to set
     */
    public void setY(double y) {
        this.y = y;
    }
    
}
