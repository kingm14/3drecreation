/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pkg3drecreation;

/**
 *
 * @author kingm14
 */
public class Edge {

    private DepthPoint p1;
    private DepthPoint p2;

    public Edge(DepthPoint p1, DepthPoint p2) {
        
        this.p1 = p1;
        this.p2 = p2;
    }

    /**
     * @return the p1
     */
    public DepthPoint getP1() {
        return p1;
    }

    /**
     * @param p1 the p1 to set
     */
    public void setP1(DepthPoint p1) {
        this.p1 = p1;
    }

    /**
     * @return the p2
     */
    public DepthPoint getP2() {
        return p2;
    }

    /**
     * @param p2 the p2 to set
     */
    public void setP2(DepthPoint p2) {
        this.p2 = p2;
    }
    public boolean equals(Edge e)
    {
        return ((e.getP1().equals(p1)&&e.getP2().equals(p2))
                ||(e.getP1().equals(p2)&&e.getP2().equals(p1)));
    }
}