/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package KD;

import java.util.*;
import java.lang.*;
import java.io.*;
import java.util.StringTokenizer;

class KDNode {

    int axis;
    float[] x;
    int id;
    boolean checked;
    boolean orientation;
    KDNode Parent;
    KDNode Left;
    KDNode Right;

    public KDNode(float[] x0, int axis0) {
        x = new float[2];
        axis = axis0;
        for (int k = 0; k < 2; k++) {
            x[k] = x0[k];
        }
        Left = Right = Parent = null;
        checked = false;
        id = 0;
    }

    public KDNode FindParent(float[] x0) {
        KDNode parent = null;
        KDNode next = this;
        int split;
        while (next != null) {
            split = next.axis;
            parent = next;
            if (x0[split] > next.x[split]) {
                next = next.Right;
            } else {
                next = next.Left;
            }
        }
        return parent;
    }

    public KDNode Insert(float[] p) {
        x = new float[2];
        KDNode parent = FindParent(p);
        if (equal(p, parent.x, 2) == true) {
            return null;
        }
        KDNode newNode = new KDNode(p, parent.axis + 1 < 2 ? parent.axis + 1 : 0);
        newNode.Parent = parent;
        if (p[parent.axis] > parent.x[parent.axis]) {
            parent.Right = newNode;
            newNode.orientation = true;
        } else {
            parent.Left = newNode;
            newNode.orientation = false;
        }
        return newNode;
    }

    boolean equal(float[] x1, float[] x2, int dim) {
        for (int k = 0; k < dim; k++) {
            if (x1[k] != x2[k]) {
                return false;
            }
        }
        return true;
    }

    float distance2(float[] x1, float[] x2, int dim) {
        float S = 0;
        for (int k = 0; k < dim; k++) {
            S += (x1[k] - x2[k]) * (x1[k] - x2[k]);
        }
        return S;
    }
}