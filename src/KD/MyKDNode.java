/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package KD;

import java.awt.Color;
import java.awt.Graphics;
import mpi.cbg.fly.Feature;

/**
 *
 * @author kingm14
 */
public class MyKDNode {

    private MyKDNode parent;
    private float[] descriptor;
    private Feature f;
    private MyKDNode leftTree;
    private MyKDNode rightTree;
    private int discriminator;
    private float medVal;
    private int numChildren;
    private float min;
    private float max;

    public MyKDNode(Feature f, int discriminator) {
        this.f = f;
        this.descriptor = f.descriptor;
        leftTree = null;
        rightTree = null;
        this.discriminator = discriminator;
        numChildren = 0;
        medVal = f.descriptor[discriminator];
        parent = null;

    }

    public MyKDNode(float[] descriptor, int discriminator) {

        this.descriptor = descriptor;
        leftTree = null;
        rightTree = null;
        this.discriminator = discriminator;
        numChildren = 0;
        medVal = descriptor[discriminator];
        parent = null;


    }

    public void init(int discriminator) {
        this.setLeftTree(null);
        this.setRightTree(null);
        this.setDiscriminator(discriminator);
        this.setNumChildren(0);
        this.setMedVal(this.getF().descriptor[discriminator]);


    }

    public void init(int discriminator, float min, float max, MyKDNode parent) {
        discriminator %= descriptor.length;
        this.setLeftTree(null);
        this.setRightTree(null);
        this.setDiscriminator(discriminator);
        this.setNumChildren(0);
        this.setMedVal(descriptor[discriminator]);
        this.setMin(min);
        this.setMax(max);
        this.setParent(parent);

    }

    public void insert(MyKDNode node) {

        if (node == null) {
            return;
        }
        float val = node.getDescriptor()[discriminator];
        numChildren++;
        int balance = getBalance();
        System.out.println(balance);
        if (balance == 0) {
            if (val > getMedVal()) {
                addToRight(node);
            } else {
                addToLeft(node);
            }
        } else if (balance > 0) {
            //more nodes are on left
            if (val < getMedVal()) {
                if (rightTree == null) {
                    addToRight(swap(node));
                } else {
                    float tempVal = rightTree.descriptor[discriminator];
                    if (tempVal > val) {
                        MyKDNode tempNode = swap(rightTree);
                        MyKDNode tempNode2 = node.swap(tempNode);
                        addToRight(tempNode2);
                    } else {
                        addToRight(swap(node));
                    }
                }
            } else {
                addToRight(node);
            }
        } else {
            //more nodes are on right
            if (val > getMedVal()) {
                if (leftTree == null) {
                    addToLeft(swap(node));
                } else {
                    float tempVal = leftTree.descriptor[discriminator];
                    if (tempVal > val) {
                        MyKDNode tempNode = leftTree.swap(this);
                        MyKDNode tempNode2 = node.swap(tempNode);
                        addToLeft(tempNode2);
                    } else {
                        addToLeft(swap(node));
                    }
                }
            } else {
                addToLeft(node);
            }
        }
    }

    public MyKDNode swap(MyKDNode node) {
        System.out.println("SWAP");
        Feature temp = f;

        float[] tempDesc = getDescriptor();
        setF(node.getF());
        setMedVal(node.getDescriptor()[discriminator]);
        setDescriptor(node.getDescriptor());
        node.setF(temp);
        node.setDescriptor(tempDesc);
        resetMinMax();

        return node;
    }

    public void resetMinMax() {

        float newMin = 0;
        float newMax = 500;

        if (getParent() != null) {
            newMin = getParent().getMin();
            newMax = getParent().getMax();
        }

        if (getLeftTree() != null) {
            MyKDNode lNode = getLeftTree();

            lNode.setMin(newMin);
            lNode.setMax(medVal);
            lNode.resetMinMax();

        }
        if (getRightTree() != null) {
            MyKDNode rNode = getRightTree();
            rNode.setMin(medVal);
            rNode.setMax(newMax);
            rNode.resetMinMax();
        }
    }

    private void addToRight(MyKDNode node) {
        if (rightTree == null) {
            if (parent == null) {
                node.init(this.getDiscriminator() + 1, medVal, 500, this);
            } else {
                node.init(this.getDiscriminator() + 1, medVal, parent.getMax(), this);
            }
            rightTree = node;
        } else {
            rightTree.insert(node);
        }
    }

    private void addToLeft(MyKDNode node) {
        if (leftTree == null) {
            if (parent == null) {
                node.init(this.getDiscriminator() + 1, 0, medVal, this);
            } else {
                node.init(this.getDiscriminator() + 1, parent.getMin(), medVal, this);
            }
            //node.init(this.getDiscriminator()+1);
            leftTree = node;

        } else {
            leftTree.insert(node);
        }
    }

    private int getBalance() {
        int leftVal = 0;
        if (leftTree != null) {
            leftVal = 1 + leftTree.getNumChildren();
        }
        int rightVal = 0;
        if (rightTree != null) {
            rightVal = 1 + rightTree.getNumChildren();
        }
        return leftVal - rightVal;
    }

    /**
     * @return the f
     */
    public Feature getF() {
        return f;
    }

    /**
     * @param f the f to set
     */
    public void setF(Feature f) {
        this.f = f;
    }

    /**
     * @return the leftTree
     */
    public MyKDNode getLeftTree() {
        return leftTree;
    }

    /**
     * @param leftTree the leftTree to set
     */
    public void setLeftTree(MyKDNode leftTree) {
        this.leftTree = leftTree;
    }

    /**
     * @return the rightTree
     */
    public MyKDNode getRightTree() {
        return rightTree;
    }

    /**
     * @param rightTree the rightTree to set
     */
    public void setRightTree(MyKDNode rightTree) {
        this.rightTree = rightTree;
    }

    /**
     * @return the discriminator
     */
    public int getDiscriminator() {
        return discriminator;
    }

    /**
     * @param discriminator the discriminator to set
     */
    public void setDiscriminator(int discriminator) {
        this.discriminator = discriminator;
        discriminator = discriminator % (descriptor.length);
    }

    /**
     * @return the medianValue
     */
    /**
     * @return the numChildren
     */
    public int getNumChildren() {
        return numChildren;
    }

    /**
     * @param numChildren the numChildren to set
     */
    public void setNumChildren(int numChildren) {
        this.numChildren = numChildren;
    }

    /**
     * @return the medVal
     */
    public float getMedVal() {
        return medVal;
    }

    /**
     * @param medVal the medVal to set
     */
    public void setMedVal(float medVal) {
        this.medVal = medVal;
    }

    /**
     * @return the descriptor
     */
    public float[] getDescriptor() {
        return descriptor;
    }

    /**
     * @param descriptor the descriptor to set
     */
    public void setDescriptor(float[] descriptor) {
        this.descriptor = descriptor;
    }

    public void draw(Graphics g) {
        int width = (this.getNumChildren() + 1);
        g.setColor(Color.green);
        if (this.getDiscriminator() % 2 == 0) {
            g.fillRect((int) medVal, (int) min - (width / 2), (int) width, (int) (max - min));
        } else {
            g.fillRect((int) min - (width / 2), (int) medVal, (int) (max - min), (int) width);
        }
        g.setColor(Color.black);
        g.fillOval((int) (descriptor[0] - 5), (int) (descriptor[1] - 5), 10, 10);
        if (leftTree != null) {
            leftTree.draw(g);
        }
        if (rightTree != null) {
            rightTree.draw(g);
        }
    }

    /**
     * @return the parent
     */
    public MyKDNode getParent() {
        return parent;
    }

    /**
     * @param parent the parent to set
     */
    public void setParent(MyKDNode parent) {
        this.parent = parent;
    }

    /**
     * @return the min
     */
    public float getMin() {
        return min;
    }

    /**
     * @param min the min to set
     */
    public void setMin(float min) {
        this.min = min;
    }

    /**
     * @return the max
     */
    public float getMax() {
        return max;
    }

    /**
     * @param max the max to set
     */
    public void setMax(float max) {
        this.max = max;
    }
}
